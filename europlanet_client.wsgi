#
import os
import sys

sys.stdout = sys.stderr

os.environ['DJANGO_SETTINGS_MODULE'] = 'app.settings'
os.environ['MPLCONFIGDIR'] = '/tmp'

sys.path.append('/share/web/django-app/europlanet_client')
sys.path.append('/share/web/django-app/europlanet_client/europlanet_client')
sys.path.append('/share/web/django-app/europlanet_client/europlanet_client/app')

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

