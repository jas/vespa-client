from datetime import datetime, date, time, timedelta

JC_DATETIME = datetime(1, 1, 1)
JC_JD = 1721425.5

SECONDS_IN_A_DAY = 60 * 60 * 24
MICROSECONDS_IN_A_SECOND = 1000000

def DateTime2jd (dateTime):
    timedelta_dateTime = dateTime - JC_DATETIME
    timedelta_seconds = timedelta_dateTime.total_seconds()
    timedelta_days = timedelta_seconds / SECONDS_IN_A_DAY
    jd = timedelta_days + JC_JD
    rounded_jd = round(jd,8)
    if timedelta_days < 0 :
        raise ValueError('Time must be after 01-01-01T00:00:00')
    return jd

def jd2DateTime (jd):
    delta_jd = jd - JC_JD
    if delta_jd < 0 :
        raise ValueError("Time must be after 1721423.5 JD")
    timedelta_days = timedelta(days=delta_jd)
    dateTime = JC_DATETIME + timedelta_days
    
    #Round To Millisecond
    seconds_float_part = float(dateTime.microsecond) / MICROSECONDS_IN_A_SECOND
    rounded_seconds_float_part = round(seconds_float_part,2)
    rounded_microseconds_as_float = rounded_seconds_float_part * MICROSECONDS_IN_A_SECOND
    microseconds_rounded = int(rounded_microseconds_as_float)
    if microseconds_rounded == MICROSECONDS_IN_A_SECOND:
        dateTime = dateTime.replace(microsecond=0)
        dateTime = dateTime + timedelta(seconds=1)
    else :
        dateTime = dateTime.replace(microsecond=microseconds_rounded)
    return dateTime

def isDateTimeValid(dateTime):
    timeDelta = dateTime - JC_DATETIME
    if timeDelta.total_seconds() < 0 :
        raise ValueError('Time must be after 01-01-01T00:00:00')
    