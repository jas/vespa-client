
MAXREC = 1000
ALL_REC = 1000000000
class Service():
#    id
#    description
#    url
#    schema
#    title
#    copyright
#    query_url
#    query_metadata
#    query_count

    def  __init__(self, access_url, schema, table=None, id=None, description=None, title=None, shortname = None, creator=None, contributors=None, copyright=None, reference_url=None):
        import query
        self.id = id
        self.description = description
        self.access_url = access_url
        self.schema = schema
        if table==None:
            if not 'epn_core' in self.schema:
                self.table = schema + '.epn_core'
            elif access_url == "http://tapvizier.u-strasbg.fr/TAPVizieR/tap":
                self.table = "\"" + schema + "\""
            else:
                self.table = schema
        else :
            self.table = table
        self.title = title
        if shortname == None:
            self.shortname = schema
        else:
            self.shortname = shortname
        self.creator = creator
        self.contributors = contributors
        self.copyright = copyright
        self.reference_url = reference_url
        
    def setQuery(self, query_conditions, pagination = None, columns=[]):
        import urllib
        # BIG UGLY HACK FOR VIZIER
        if self.access_url == "http://tapvizier.u-strasbg.fr/TAPVizieR/tap":
            query_conditions = query_conditions.replace("lower(", "cds_lower(")
        
        
        
        if columns == []:
            selected_columns = "*"
        else:
            selected_columns = ', '.join("\"" + column + "\"" for column in columns)
        if pagination == None or pagination['page_size'] == "-1" :
            select_clause = 'SELECT ' + selected_columns            
        else:
            select_clause = 'SELECT TOP ' +pagination['page_size'] + ' ' + selected_columns
        
        select_top10_clause = 'SELECT TOP 10 ' + selected_columns
        
        count_clause = 'SELECT COUNT (*)'
            
        from_clause = 'FROM '+ self.table
        
        if query_conditions != "" and query_conditions != None:
            where_clause = 'WHERE ' + query_conditions + ''
        else:
            where_clause = ''

        where_pagination_clause = ''
        if pagination != None and pagination['page_size'] != "-1" :
            where_pagination_clause = 'ORDER BY ' + pagination['sort']['name'] + ' ' + pagination['sort']['dir']
            if pagination['offset'] != "0":
                where_pagination_clause = where_pagination_clause +' OFFSET ' + pagination['offset']
        self.count_query = count_clause + ' ' + from_clause + ' ' + where_clause
        self.select_top10_query = select_top10_clause + ' ' + from_clause + ' ' + where_clause

        if where_clause == '':
            self.select_query = select_clause + ' ' + from_clause + ' ' + where_pagination_clause
        else:
            self.select_query = select_clause + ' ' + from_clause + ' ' + where_clause + ' ' + where_pagination_clause
        
        self.count_parameters = urllib.urlencode({'REQUEST':'doQuery', 'LANG': 'ADQL', 'QUERY': self.count_query})
        self.select_paramaters_encoded = urllib.urlencode({'REQUEST':'doQuery', 'MAXREC' :str(ALL_REC), 'LANG': 'ADQL', 'QUERY': self.select_query})
        self.select_top10_parameters_encoded = urllib.urlencode({'REQUEST':'doQuery', 'MAXREC' :str(ALL_REC), 'LANG': 'ADQL', 'QUERY': self.select_top10_query})

        self.url_base = self.access_url + '/sync?'
        self.empty_query = self.url_base + urllib.urlencode({'REQUEST':'doQuery', 'MAXREC' :str(ALL_REC), 'LANG': 'ADQL'})
        self.adql_query_all = urllib.urlencode({'QUERY': "SELECT * FROM "+self.table})


        self.url_query_count = self.url_base + self.count_parameters
        self.url_query_select = self.url_base + self.select_paramaters_encoded
        self.url_query_select_top10 = self.url_base + self.select_top10_parameters_encoded
        
        where_clause_is_geojsonisable = "WHERE spatial_frame_type='body' AND c1min IS NOT NULL AND c1max IS NOT NULL AND c2min IS NOT NULL AND c2max IS NOT NULL"
        query_is_geojsonisable = 'SELECT distinct target_name' + ' ' + from_clause + ' ' + where_clause_is_geojsonisable
        query_is_geojsonisable = urllib.urlencode({'REQUEST':'doQuery', 'LANG': 'ADQL', 'QUERY': query_is_geojsonisable})
        self.query_is_geojsonisable = self.url_base + query_is_geojsonisable
        
        self.url_metadata = urllib.urlencode({'REQUEST':'doQuery', 'MAXREC' :str(0), 'LANG': 'ADQL', 'QUERY': self.select_query})

        COLUMN_NAMES = ['column_name','unit','ucd','description','datatype']
        self.get_columns_query = "SELECT " + " ,".join(COLUMN_NAMES) + " FROM TAP_SCHEMA.columns WHERE table_name='" + self.table + "'"
        
        self.test_offset_query = "SELECT TOP 1 granule_uid FROM " + self.table + " ORDER BY granule_uid OFFSET 0"
#     def getColumns(self):
#         import tapQuery
#         COLUMN_NAMES = ['column_name','unit','ucd','description','datatype']
#         query = "SELECT " + " ,".join(COLUMN_NAMES) + " FROM tap_schema.columns WHERE table_name='" + self.table + "'"
#         FORMAT = 'json'
#         result = tapQuery.run(self.access_url, query,format = FORMAT)
#         columns = []
#         for row in result:
#             columns.append({'name':row[0],'unit':row[1],'ucd':row[2],'description':row[3],'datatype':row[4]})
#         return columns
#     
#     def hasColumn(columnName):
#         QUERY_BASE = "SELECT COUNT (*) FROM rr.table_column WHERE ivoid='%s' AND name='%s'"
#         FORMAT = 'json'
#         query = QUERY_BASE % (ivoid,columnName)
#         result = tapQuery.run(BASE_URL, query,format = FORMAT)
#         if result[0][0] == 0:
#             QUERY_CHECK_RESOURCE = "SELECT COUNT (*) FROM rr.table_column WHERE ivoid='%s'"
#             query = QUERY_CHECK_RESOURCE % (ivoid)
#             result_check = tapQuery.run(BASE_URL, query,format = FORMAT)
#             if result_check[0][0] == 0:
#                 raise NoColumnInformationException(ivoid)
#             return False
#         else:
#             return True
