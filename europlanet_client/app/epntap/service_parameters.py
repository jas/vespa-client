URL = "https://voparis-svn.obspm.fr/validator/branches/gcz/json/EPN-TAP2.json"
import urllib2
import json
class ServiceParameters:
    def  __init__(self):
        webFile = urllib2.urlopen(URL, timeout=10)     
        json_file = json.load(webFile)
        parameters_dict = json_file["RESOURCE"]["then"]["QUERY_STATUS OK"]["then"]
        self.mandatory_list = []
        self.optional_list = []
        for key, value in sorted(parameters_dict.iteritems()):
            if key.startswith("4.2") or key.startswith("4.3") or key.startswith("4.4"):
                name = value['xpath'].partition("@name=")[2].split("'")[1]
                if value["error type"] == "error":
                    self.mandatory_list.append(name)
                elif value["error type"] == "warning":
                    self.optional_list.append(name)
                    
    def getMandatoryParameters(self):
        return self.mandatory_list
    def getOptionalParameters(self):
        return self.optional_list