import os
import urllib2
import json
import sys
import settings
from service import *
import xmlReader
import tapQuery

#BASE_URL = 'http://voparis-registry.obspm.fr/vo/ivoa/1'
# BASE_URL = 'http://voparis-registry2.obspm.fr/vo/ivoa/1'
# BASE_URL = 'http://voparis-rr.obspm.fr/tap'
# BASE_URL = 'http://reg.g-vo.org/tap'
#BASE_URL = 'http://registry.euro-vo.org/regtap/tap'
BASE_URL = 'http://gavo.aip.de/tap'


TAP_STANDARDID = 'ivo://ivoa.net/std/TAP'

class NoTapCapabilityException(Exception):
    def __init__(self, value):
        self.value = value

def readVirtualRegistryJson():
    fileAdr = os.path.join(settings.DJANGO_PROJECT_PATH, '../media/json/virtualRegistry.json')
    try:
        with open(fileAdr, 'r') as input:
            virtualRegistry = json.load(input)
            input.close()
    except Exception as e:
        sys.stderr.write("Fail loading virtualRegistry.json\n")
    return virtualRegistry
    
def getEpnServices():
    return getVirtualRegistryServices()
    

    
def getVirtualRegistryServices():
    virtualRegistry = readVirtualRegistryJson()
    
    return getServicesInfos(virtualRegistry)

def getServicesInfos(virtualRegistry):
    servicesInfos = []
    for r in virtualRegistry['services']:
        try :

            id = r['identifier']
            title = r['title']
            shortname = r['shortname']
            description = r['description']
            
            referenceurl = None
            if r.has_key('referenceurl'):
                referenceurl = r['referenceurl']

            copyright = None
            if r.has_key('publisher'):
                copyright = r['publisher']

            creator = None
            if r.has_key('creator'):
                creator =', '.join(r['creator'])
                
            contributors = None
            if r.has_key('contributors'):
                contributors = ', '.join(r['contributors'])
                
            table = None
            if r.has_key('table_name'):
                table = r['table_name']
            
            currentService = Service(
                                        r['accessurl'],
                                        r['schema'],
                                        table,                                         
                                        id = r['identifier'],
                                        title = r['title'],
                                        shortname = r['shortname'],
                                        description = r['description'],
                                        reference_url = referenceurl,
                                        copyright = copyright,
                                        creator = creator,
                                        contributors = contributors,
                                        )


#            testResource(currentResource)
            servicesInfos.append(currentService)
        except Exception as e:
            title = ""
            if r['title']:
                title = r['title']
            sys.stderr.write("Error reading service \"%s\"\n" % (title))
        servicesInfos = sorted(servicesInfos, key=lambda servicesInfos: servicesInfos.title)
    return servicesInfos
    
    
def getRegistryEpnServices():
#    url = BASE_URL + '/voresources/search?keywords=datamodel:epncore&max=100'
    url = BASE_URL + '/voresources?q=capabilities.datamodel:"ivo://vopdc.obspm/std/EpnCore/schema-2.0"'
#    url = BASE_URL + '/voresources/search?keywords="ivo://ivoa.net/std/TAP" "epn"&max=100'
    json = readJsonURL(url)
    services = readServicesFields(json)
    return services

def getDefinedServices():
    service_list = [{'access_url':'http://voparis-tap-planeto.obspm.fr/tap','schema_list':[
                                                                                   'apis',
                                                                                   'bdip',
                                                                                   'exoplanet',
                                                                                   'iks',
                                                                                   'planets',
                                                                                   'titan',
                                                                                   'm4ast',
                                                                                   'basecom',
                                                                                   'tnosarecool',
                                                                                   'radiojove',
                                                                                   'vvex'
                                                                                   ]},
                     {'access_url':'http://voparis-tap-helio.obspm.fr/tap', 'schema_list':['hfc1ar','hfc1t3']},
                     {'access_url':'http://epn1.epn-vespa.jacobs-university.de/tap', 'schema_list':['crism','mars_craters','usgs_wms']},
#                      {'access_url':'http://cdpp-epntap.cesr.fr/tap', 'schema_list':['amdadb']},
                     {'access_url':'http://amda-epntap.irap.omp.eu/tap', 'schema_list':['amdadb']},
                      
                     {'access_url':'http://ia2-tap.oats.inaf.it:8080/epntap', 'schema_list':['nasadustcat']},
                     {'access_url':'http://vogate.obs-nancay.fr/tap', 'schema_list':['nda_routine']},
                      {'access_url':'http://www2-graz.fzg.oeaw.ac.at/tap', 'schema_list':['impex_epn20','vexmag_epn20']},
#                    {'access_url':'http://epn-tap.oeaw.ac.at/tap', 'schema_list':['impex_epn20','vexmag_epn20']},
                      
                      
#                      {'access_url':'http://ilithyie.obspm.fr/tap', 'schema_list':['hisaki','iitate']},
                     {'access_url':'http://sery.lmd.jussieu.fr/tap', 'schema_list':['mcd']},
                     {'access_url':'http://vo.projet.latmos.ipsl.fr/tap', 'schema_list':['spicam']},
                     {'access_url':'http://thebe.gp.tohoku.ac.jp/tap', 'schema_list':['hisaki','iitate']},
 
                     ]
    services = []
    for r_vm in service_list:
        access_url = r_vm['access_url']
        for schema in r_vm['schema_list']:
            currentService =  Service(access_url,schema,shortname=schema)
            services.append(currentService)
     
    return services

def getServiceByID(serviceID):
    return getVirtualRegistryServiceByID(serviceID)

def getVirtualRegistryServiceByID(serviceID):
    virtualRegistry = readVirtualRegistryJson()
    servicesInfos = []
    for r in virtualRegistry['services']:    
        if r['identifier'] == serviceID:
            res = {'services':[r]}
            return getServicesInfos(res)[0]
    sys.stderr.write("Unexpected number of service found with id : %s\n" % (serviceID))
    raise Exception
    

def getRegistryServiceByID(serviceID):
    url = BASE_URL + '/voresources/search?keywords=identifier:\"' + serviceID + '\"'
    json = readJsonURL(url)
    services = readServicesFields(json)
    if len(services) != 1:
        sys.stderr.write("Unexpected number of service found with id : %s\n" % (requestId))
        raise Exception
    else :
        return services[0]

def getVirtualRegistryServiceByShortname(shortname):
    virtualRegistry = readVirtualRegistryJson()
    servicesInfos = []
    for r in virtualRegistry['services']:    
        if str(r['shortname']).lower() == str(shortname).lower():
            res = {'services':[r]}
            return getServicesInfos(res)[0]
    return None

def getVirtualRegistryServiceBySchema(schema):
    virtualRegistry = readVirtualRegistryJson()
    servicesInfos = []
    for r in virtualRegistry['services']:    
        if str(r['schema']).lower() == str(schema).lower():
            res = {'services':[r]}
            return getServicesInfos(res)[0]
    return None

def getVirtualRegistryServiceByAccessurlAndSchema(access_url, schema):
    virtualRegistry = readVirtualRegistryJson()
    servicesInfos = []
    for r in virtualRegistry['services']:    
        if r['accessurl'] == access_url and r['schema'] == schema: 
            res = {'services':[r]}
            return getServicesInfos(res)[0]
    return None
    

def readJsonURL(url):
    req = urllib2.Request(url)
#    req.add_header('Accept', 'application/json')
    webFile = urllib2.urlopen(req)
    s = webFile.read()
    webFile.close()
    return json.loads(s)

def testService(Service):
    url_tables = service.access_url + '/tables'
    try:
        webFile = urllib2.urlopen(url_tables)
    except (urllib2.URLError, ValueError):
        raise TableUrlNotFoundException(url_tables)
    
def readServicesFields(json):
    servicesInfos = []
#    for r in json['resources']:
    for r in json['data']:
        try :
            if r['identifier'] != 'ivo://vopdc.obspm/luth/exoplanet/epn':
                capability = None
                for c in r['capabilities']:
                    if TAP_STANDARDID in c['standardid']:
                        capability = c
                if not capability:
                    raise NoTapCapabilityException
#                urlVoresources = BASE_URL + '/voresources.xml?identifier=' + r['identifier']
                urlVoservices = r['self']
                schema, table = xmlReader.readVoServices(urlVoservices)
                referenceurl = None
                if r.has_key('referenceurl'):
                    referenceurl = r['referenceurl']
                if 'creator' in r and 'contributors' in r :               
                    currentService = Service(
                                               capability['accessurl'],
                                               schema,
                                               id = r['identifier'],
                                               description = r['description'],
                                               table = table,
                                               title = r['title'],
                                               shortname = r['shortname'],
                                               creator = r['creator'][0],
                                               contributors = r['contributors'][0],
                                               reference_url = referenceurl
                                           )
                else :
                    currentService = Service(
                                               capability['accessurl'],
                                               schema,
                                               id = r['identifier'],
                                               description = r['description'],
                                               table = table,
                                               title = r['title'],
                                               shortname = r['shortname'],
                                               copyright = r['publisher'],
                                               reference_url = referenceurl)
    #            testResource(currentResource)
                servicesInfos.append(currentService)
        except Exception as e:
            title = ""
            if r['title']:
                title = r['title']
            sys.stderr.write("Error reading service \"%s\"\n" % (title))
        servicesInfos = sorted(servicesInfos, key=lambda servicesInfos: servicesInfos.title)
    return servicesInfos

class NoColumnInformationException(Exception):
    def __init__(self, value):
        self.value = value


def getServiceColumns(ivoid):
    COLUMN_NAMES = ['name','unit','ucd','column_description','datatype']
    QUERY_BASE = "SELECT " + " ,".join(COLUMN_NAMES) + " FROM rr.table_column WHERE ivoid='%s'"
    FORMAT = 'json'
    query = QUERY_BASE % (ivoid)
    result = tapQuery.run(BASE_URL, query,format = FORMAT)
    if len(result) == 0:
        raise NoColumnInformationException(ivoid)
    else:
        columns = []
        for row in result:
            columns.append({'name':row[0],'unit':row[1],'ucd':row[2],'description':row[3],'datatype':row[4]})
        return columns
        
# def getResourcesColumns(resources):
#     import urllib2
#     from astropy.io.votable import parse
#     QUERY_BASE = "SELECT * FROM rr.table_column WHERE rr.table_column.ivoid IN (%s)"
#     id_list = []
#     for resource in resources:
#         if resource.id == None:
#             raise Exception
#         else:
#             id_list.append("'"+resource.id+"'")
#     query = QUERY_BASE % (",".join(id_list))
#     query_encoded= urllib.urlencode({'REQUEST':'doQuery', 'LANG': 'ADQL', 'QUERY': query})
#     webFile = urllib2.urlopen(URL_BASE, query_encoded)
#     votable = parse(webFile)
#     webFile.close()
#     table = votable.get_first_table().to_table()
            
