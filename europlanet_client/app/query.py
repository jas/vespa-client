import TableMetadataReader
import registry
from forms import SENSITIVE_STRING_COMPARE

class ADQLQuery():
#     def  __init__(self, access_url):
#         self.url_base = self.access_url + '/sync?'
    pass
        
class ADQLQueryWhere():
    
    def  __init__(self,conditions):
        self.conditions = ' AND '.join(filter(None, conditions))            
        
    def getQueryConditions(self):
        return self.conditions
    
    def constructMandatoryParametersquery(self, form):
        """Not a view function: it just constructs the query""" 
    # IT has to be done properly !!!
        from forms import MandatoryParametersForm, HASHLISTPARAMETERS
        conditions = []
        if form != None and form.is_valid():
            conditions.append(self.__getCoordinatesQueryPart(form.cleaned_data))
            conditions.append(self.__getSpectralQueryPart(form.cleaned_data))
            for f in form.cleaned_data:
                if ((form.cleaned_data[f] or form.cleaned_data[f] == 0) and not '_op' in f)\
                and not (f in MandatoryParametersForm.SPECIAL_FIELDS):  # only non-empty and non-operator fields participate in the query 
                    if f in ('time_min', 'time_max', 'time_center') :
                        conditions.append(self.__queryTime(f, form))
                    elif form.cleaned_data.has_key(f + '_op') :
                        value = form.cleaned_data[f]
                        if f in MandatoryParametersForm.SPECTRAL_FIELDS:
                            value = self.__get_spectral_value(value, form.cleaned_data['spectral_unit_op'])
                        if f in HASHLISTPARAMETERS:
                            if f in SENSITIVE_STRING_COMPARE:
                                conditions.append(self.__getHashListSensitiveQueryWithOp(f, form.cleaned_data[f + '_op'], value))
                            else:
                                conditions.append(self.__getHashListInsensitiveQueryWithOp(f, form.cleaned_data[f + '_op'], value))
                        else:
                            conditions.append(self.__getNumberQueryWithOp(f, form.cleaned_data[f + '_op'], value))
                    # if there's no operator for the field, test equality, but also process MultipleChoiceField here as they don't have operators
                    elif isinstance(form.cleaned_data[f], list):  # MultipleChoiceField
                        # expression is somewhat complicated, sorry
                        conditions.append('(%s)' % (" OR " . join(map(lambda p: "\"%s\" LIKE '%%%s%%'" % (f, p), form.cleaned_data[f]))))
    #                 elif (form.cleaned_data['resource_type'] == 'dataset') and ((f == 'target_name') or (f == 'measurement_type')):
    #                     query = query + self.__getLikeQuery(f, form.cleaned_data[f])
                    elif isinstance(form.cleaned_data[f], int):
                        conditions.append(self.__getNumberQuery(f, form.cleaned_data[f]))
                    elif f == 'target_name':
                        conditions.append(self.__getTargetNameQuery(form.cleaned_data))
    #                elif f == 'instrument_host_name' or f == 'instrument_name':
                    elif f in HASHLISTPARAMETERS:
                        if f in SENSITIVE_STRING_COMPARE:
                            conditions.append(self.__getHashListSensitiveQuery(f, form.cleaned_data[f]))
                        else:
                            conditions.append(self.__getHashListInsensitiveQuery(f, form.cleaned_data[f]))
                    else:
                        conditions.append(self.__getStringQuery(f, form.cleaned_data[f]))
        return ' AND '.join(filter(None, conditions))

    
    def getAdvancedQuery(self, advanced_parameters, service):
        
        try:
            columns = registry.getServiceColumns(service.id)
        except registry.NoColumnInformationException:
            tableMetadata = TableMetadataReader.TableMetadata(service.access_url, service.table)
            columns = tableMetadata.getColumns()

        conditions = []
        for advanced_parameter in advanced_parameters:
            if advanced_parameter['op'] == 'LIKE':
                conditions.append(self.__getLikeQuery(advanced_parameter['label'], advanced_parameter['value']))
            else:
                fieldType = None
                for column in columns:
                    if column['name'] == advanced_parameter['label']:
                        fieldType = column['datatype']
#                 fieldType = tableMetadata.getFieldType(advanced_parameter['label'])
                if fieldType == "char":
                    conditions.append(self.__getStringQuery(advanced_parameter['label'], advanced_parameter['value']))
                else:
                    conditions.append(self.__getNumberQueryWithOp(advanced_parameter['label'], advanced_parameter['op'], advanced_parameter['value']))
        return ' AND '.join(filter(None, conditions))



    def __getCoordinatesQueryPart(self, form_data):
        data_min_max = self.__getMinMaxCoordinates(form_data)
        data = self.__normalizeValues(form_data['spatial_frame_type'], data_min_max)
        conditions = []
        if form_data['location_interval_op'] == 'inclusion':
            # Deal with crossing meridian 
            if form_data['spatial_frame_type'] == 'body' or form_data['spatial_frame_type'] == 'celestial':
                if data['c1min'] != None and data['c1max'] != None :
                    if data['c1min'] > data['c1max']:
                        conditions.append('((c1min >= ' + str(data['c1min']) + ' AND c1max <= ' + str(data['c1max']) + ') OR (c1min >= '+ str(data['c1min']) + ' AND c1max >= ' + str(data['c1min']) + ') OR (c1min <= ' + str(data['c1max']) + ' AND c1max <= ' + str(data['c1max']) + '))')
                    else:
                        conditions.append('c1min >= ' + str(data['c1min']) + ' AND c1max <= ' + str(data['c1max']) + ' AND c1min <= c1max')
                    data.pop("c1min", None)
                    data.pop("c1max", None)
            # Deal with crossing meridian 
            elif form_data['spatial_frame_type'] == 'cylindrical' or form_data['spatial_frame_type'] == 'spherical':
                if data['c2min'] != None and data['c2max'] != None : 
                    if data['c2min'] > data['c2max']:
                        conditions.append('((c2min >= ' + str(data['c2min']) + ' AND c2max <= ' + str(data['c2max']) + ') OR (c2min >= '+ str(data['c2min']) + ' AND c2max >= ' + str(data['c2min']) + ') OR (c2min <= ' + str(data['c2max']) + ' AND c2max <= ' + str(data['c2max']) + '))')
                    else:
                        conditions.append('c2min >= ' + str(data['c2min']) + ' AND c2max <= ' + str(data['c2max']) + ' AND c2min <= c2max')
                    data.pop("c2min", None)
                    data.pop("c2max", None)
            conditions.append(self.__getSimpleInclusion(data))
        else:
            if form_data['spatial_frame_type'] == 'body' or form_data['spatial_frame_type'] == 'celestial':
                if data['c1min'] != None and data['c1max'] != None :
                    if data['c1min'] > data['c1max']:
                        conditions.append('(c1min <= ' + str(data['c1max']) + ' OR c1max >= ' + str(data['c1min']) + ' OR c1min > c1max)')
                    else:
                        conditions.append('((c1min <= c1max AND c1min <= ' + str(data['c1max']) + ' AND c1max >= ' + str(data['c1min']) + ') OR (c1min > c1max AND (c1min <= ' + str(data['c1max']) + ' OR c1max >= ' + str(data['c1min']) + ')))')
                    data.pop("c1min", None)
                    data.pop("c1max", None)
            elif form_data['spatial_frame_type'] == 'cylindrical' or form_data['spatial_frame_type'] == 'spherical':
                if data['c2min'] != None and data['c2max'] != None :
                    if data['c2min'] > data['c2max']:
                        conditions.append('(c2min <= ' + str(data['c2max']) + ' OR c2max >= ' + str(data['c2min']) + ' OR c2min > c2max)')
                    else:
                        conditions.append('((c2min <= c2max AND c2min <= ' + str(data['c2max']) + ' AND c2max >= ' + str(data['c2min']) + ') OR (c2min > c2max AND (c2min <= ' + str(data['c2max']) + ' OR c2max >= ' + str(data['c2min']) + ')))')
                    data.pop("c2min", None)
                    data.pop("c2max", None)
            conditions.append(self.__getSimpleIntersection(data,('c1','c2','c3')))
        return ' AND '.join(filter(None, conditions))

    def __getSimpleInclusion(self, data):
        conditions = []
        for key, value in data.iteritems():
            if value != None:
                if key.endswith('min'):
                    conditions.append(key + ' >= ' + str(value))
                else:
                    conditions.append(key + ' <= ' + str(value))
        return ' AND '.join(filter(None, conditions))

    def __getMinMaxCoordinates(self, form_data):
        data = {}
        if form_data['location_values_op'] == 'between':
            for c in ('c1','c2','c3'):
                for m in ('min','max'):
                    data [c+m] = form_data [c+m]
        else:
            for c in ('c1','c2','c3'):
                if form_data[c+'delta'] != None:
                    data[c+'min'] = form_data[c+'center'] - form_data[c+'delta']
                    data[c+'max'] = form_data[c+'center'] + form_data[c+'delta']
                else:
                    data[c+'min'] = form_data[c+'center']
                    data[c+'max'] = form_data[c+'center']
        return data

    def __normalizeValues(self, spatial_frame_type, data_min_max):
        data = {}
        if spatial_frame_type == 'body' or spatial_frame_type == 'celestial':
            for m in ('min','max'):
                data['c1'+m] = self.__normalizeValue(data_min_max['c1'+m])
            for c in ('c2','c3'):
                for m in ('min','max'):
                    data [c+m] = data_min_max [c+m]
        elif spatial_frame_type == 'cylindrical' or spatial_frame_type == 'spherical':
            for m in ('min','max'):
                data['c2'+m] = self.__normalizeValue(data_min_max['c1'+m])
            for c in ('c1','c3'):
                for m in ('min','max'):
                    data [c+m] = data_min_max [c+m]
        else:
            for c in ('c1','c2','c3'):
                for m in ('min','max'):
                    data [c+m] = data_min_max [c+m]
        return data

    def __normalizeValue(self, value):
        if value != None:
            if value != 360:
                return value % 360
            else:
                return 360
        else:
            return None

    def __getSimpleIntersection(self, data, fields):
        conditions = []
        for c in fields:
            for m in ('min','max'):
                if data.has_key(c+m):
                    if data[c+m] != None:
                        if m == 'min':
                            conditions.append(c + 'max >= ' + str(data[c+m]))
                        else:
                            conditions.append(c + 'min <= ' + str(data[c+m]))
        return ' AND '.join(filter(None, conditions))

    def __getSpectralQueryPart(self, form_data):
        from forms import MandatoryParametersForm
        FIELDS = ('spectral_resolution_', 'spectral_sampling_step_', 'spectral_range_')
        data = {}
        if form_data['spectral_unit_op'] == 'hertz':
            for f in FIELDS:
                data[f+'min'] = form_data[f+'min']
                data[f+'max'] = form_data[f+'max']
        else:
            for f in FIELDS:
                if f == 'spectral_resolution_':
                    data[f+'min'] = form_data[f+'min']
                    data[f+'max'] = form_data[f+'max']
                else:
                    if form_data[f+'max'] == None:
                        data[f+'min'] = None
                    else:
                        if form_data['spectral_unit_op'] == 'micro_meter':
                            data[f+'min'] = MandatoryParametersForm.COEFF_MICROMETER_TO_HERTZ / form_data[f+'max']
                        else:
                            data[f+'min'] = form_data[f+'max'] * MandatoryParametersForm.COEFF_PERCENTIMETER_TO_MICROMETER
                    if form_data[f+'min'] == None:
                        data[f+'max'] = None
                    else:
                        if form_data['spectral_unit_op'] == 'micro_meter':
                            data[f+'max'] = MandatoryParametersForm.COEFF_MICROMETER_TO_HERTZ / form_data[f+'min']
                        else:
                            data[f+'max'] = form_data[f+'min'] * MandatoryParametersForm.COEFF_PERCENTIMETER_TO_MICROMETER 
                
        if form_data['spectral_interval_op'] == 'inclusion':
            return self.__getSimpleInclusion(data)
        else:
            return self.__getSimpleIntersection(data, FIELDS)

    def __queryTime(self, f, form):
        import datetime
        time = form.cleaned_data[f]
        conditions = []
        if f == 'time_center' :
            if (form.cleaned_data['time_delta_value_op']):
                unit = form.cleaned_data['time_delta_unit_op']
                delta = form.cleaned_data['time_delta_value_op']
                delta_iso = self.__createTimeDelta(delta, unit)
            else :
                delta_iso = datetime.timedelta(days=0)
            time_min = time - delta_iso
            time_max = time + delta_iso
            conditions.append(self.__queryTimeValue(form, 'time_min', time_min))
            conditions.append(self.__queryTimeValue(form, 'time_max', time_max))
        else :
            conditions.append(self.__queryTimeValue(form, f, time))
        return ' AND '.join(filter(None, conditions))
    
    def __createTimeDelta(self, delta, unit):
        import datetime
        return {
            'days': datetime.timedelta(days=delta),
            'hours': datetime.timedelta(hours=delta),
            'minutes': datetime.timedelta(minutes=delta),
            'seconds': datetime.timedelta(seconds=delta),
            'milliseconds': datetime.timedelta(milliseconds=delta),
        }[unit]

    def __queryTimeValue(self, form, f, time):
        from DateConversion import DateTime2jd
        time_jd = DateTime2jd(time)
        precision = "{0:0.8f}"
        time_print = precision.format(time_jd)
        condition = ''
        if (form.cleaned_data['time_search_type_op'] == 'is_included_in'):
            if (f == 'time_min'):
                condition = 'time_min >= %s' % (time_print)
            else :
                condition = 'time_max <= %s' % (time_print)
        else :
            if (f == 'time_min'):
                condition = 'time_max >= %s' % (time_print)
            else :
                condition = 'time_min <= %s' % (time_print)
        return condition

    def __get_spectral_value(self, value, unit):
        from forms import MandatoryParametersForm
        if unit == "hertz":
            return value
        elif unit == "micro_meter":
            return MandatoryParametersForm.COEFF_MICROMETER_TO_HERTZ / value
        else:
            return value * MandatoryParametersForm.COEFF_PERCENTIMETER_TO_MICROMETER

    def __getHashListInsensitiveQueryWithOp(self, name, operator, value):
        if operator == 'LIKE':
            return self.getILikeQuery(name, value)
        else:
            return self.__getHashListInsensitiveQuery(name,value)
        
    def __getHashListSensitiveQueryWithOp(self, name, operator, value):
        if operator == 'LIKE':
            return self.__getLikeQuery(name, value)
        else:
            return self.__getHashListSensitiveQuery(name,value)
    
        
    def getILikeQuery (self, field_name, field_value):
        return 'lower(\"%s\") LIKE lower(\'%%%s%%\')' % (field_name, field_value)
    
    def __getNumberQueryWithOp(self, name, operator, value):
        if operator == u'\u2264':
            operator = '<='
        elif operator == u'\u2265':
             operator = '>='
        return '\"%s\" %s %s' % (name, operator, value)

    def __getNumberQuery(self, name, value):
        return '\"%s\" = %s' % (name, value)

#     def __getTargetNameQuery(self, data):
#         import urllib2
#         import json
        
#         if data['target_name_ssodnet_link']:
#             webFile = urllib2.urlopen(data['target_name_ssodnet_link'])            
#             if webFile.code == 200:
#                 res = json.loads(webFile.read())
#                 names = []
#                 query_list = []
#                 names.append(res["name"])
#                 if res.has_key("aliases"):
#                     for alias in res["aliases"]:
#                         names.append(alias)
#                 for name in names:
#                     query_list.append("1 = ivo_hashlist_has(lower(\"target_name\"),lower(\'"+ name + "\'))")
#                 
#                 condition = '(' + ' OR '.join(query_list) + ')'
#                 return condition
#             else:
#                 return self.__getHashListQuery('target_name', data['target_name'])
#         else:
#             return self.__getHashListQuery('target_name', data['target_name'])

        
        
#         TARGET_CLASS_TO_SSODNET = {
#                                'asteroid' : 'Asteroid',
#                                 'dwarf_planet' : 'Dwarf Planet',
#                                'comet' : 'Comet',
#                                'planet' : 'Planet',
#                                'exoplanet' : 'Exoplanet',
#                                'satellite' : 'Satellite'
#                        }
#         
#         type_list = []
#         if data.has_key('target_class') and data['target_class'] != []:
#             for target_class in data['target_class']:
#                 if (target_class in TARGET_CLASS_TO_SSODNET):
#                     type_list.append(TARGET_CLASS_TO_SSODNET[target_class])
#         type_query = ''
#         if ( len(type_list) != 0) :
#             type_query =' AND (type:'+ ' OR type:'.join(type_list) + ')' 
#         query = 'http://voparis-registry.obspm.fr/ssodnet/1/search?q="'+data['target_name']+'"'+type_query
#         webFile = urllib2.urlopen(query)
#         if webFile.code == 200:
#             res = json.loads(webFile.read())
#             names = []
#             for hit in res['hits']:
#                 names.append(hit['name'])
#                 if hit.has_key('aliases'):
#                     names = names + hit['aliases']
#             query_list = []
#             for name in names:
#     #            query_list.append('lower(target_name)= lower(\'' + name + '\')')
#                 query_list.append("1 = ivo_hashlist_has(lower(\"target_name\"),lower(\'"+ name + "\'))")
#     #            1 = ivo_hashlist_has(lower(\"%s\"),lower(\'%s\'))" % (name, value)
#             condition = '(' + ' OR '.join(query_list) + ')'
#             return condition
#         else:
#             return self.__getHashListQuery('target_name', data['target_name'])

    def __getTargetNameQuery(self, data):
        import urllib2
        import json
        import urllib
        TARGET_CLASS_TO_SSODNET = {
                               'asteroid' : 'Asteroid',
                                'dwarf_planet' : 'Dwarf Planet',
                               'comet' : 'Comet',
                               'planet' : 'Planet',
                               'exoplanet' : 'Exoplanet',
                               'satellite' : 'Satellite'
                       }
         
        type_list = []
        if data.has_key('target_class') and data['target_class'] != []:
            for target_class in data['target_class']:
                if (target_class in TARGET_CLASS_TO_SSODNET):
                    type_list.append(TARGET_CLASS_TO_SSODNET[target_class])
        type_query = ''
        if ( len(type_list) != 0) :
            type_query =' AND (type:'+ ' OR type:'.join(type_list) + ')' 
        query = 'https://api.ssodnet.imcce.fr/quaero/1/sso/search?' + urllib.urlencode({'q':'"'+data['target_name']+'"'+type_query})
        webFile = urllib2.urlopen(query)
        if webFile.code == 200:
            res = json.loads(webFile.read())
            names = []
            if len(res['data']) == 0:
                names.append(data['target_name'])
            else:
                hit=res['data'][0]
                names.append(hit['name'])
                if hit.has_key('aliases'):
                    names = names + hit['aliases']
            query_list = []
            for name in names:
                query_list.append(self.__getHashListInsensitiveQuery("target_name", name))
    #            query_list.append('lower(target_name)= lower(\'' + name + '\')')
#                 query_list.append("1 = ivo_hashlist_has(lower(\"target_name\"),lower(\'"+ name + "\'))")
    #            1 = ivo_hashlist_has(lower(\"%s\"),lower(\'%s\'))" % (name, value)
            condition = '(' + ' OR '.join(query_list) + ')'
            return condition
        else:
            return self.__getHashListQuery('target_name', data['target_name'])
        
    def __getHashListInsensitiveQuery(self, name,value):
        return "1 = ivo_hashlist_has(lower(\"%s\"),lower(\'%s\'))" % (name, value)

    def __getHashListSensitiveQuery(self, name,value):
        return "1 = ivo_hashlist_has(\"%s\",\'%s\')" % (name, value)

    def __getStringQuery(self, name, value):
        return '\"%s\" = \'%s\'' % (name, value)

    def __getLikeQuery (self, name, value):
        return '\"%s\" LIKE \'%%%s%%\'' % (name, value)
    
    def __getTargetNameWilcardQuery (self, name, value):
        return 'lower(\"%s\") LIKE lower(\'%s\')' % (name, value)

    def getLowerCaseQuery(self, name, value):
        return ' lower(\"%s\")= lower(\'%s\')' % (name, value)

class EmptyADQLQueryWhere(ADQLQueryWhere):
    def  __init__(self):
        ADQLQueryWhere.__init__(self,[])

class FormADQLQueryWhere(ADQLQueryWhere):
    def  __init__(self, mandatoryParametersForm = None, advanced_parameters = None, service = None):
        conditions = []
        conditions.append(self.constructMandatoryParametersquery(mandatoryParametersForm))
        if advanced_parameters != None:
            conditions.append(self.getAdvancedQuery(advanced_parameters, service))
        ADQLQueryWhere.__init__(self, conditions)
    

class TextADQLQueryWhere(ADQLQueryWhere):
    def  __init__(self, text_query = None):
        self.conditions = text_query

class UrlADQLQueryWhere(ADQLQueryWhere):
    def  __init__(self, urlParameters):
        self.params = {}
        if urlParameters.has_key("file_name"):
            self.params["file_name"] = urlParameters["file_name"]
            self.conditions = self.getLowerCaseQuery("file_name", urlParameters["file_name"])
        elif urlParameters.has_key("access_url_contains"):
            self.params["access_url_contains"] = urlParameters["access_url_contains"]            
            self.conditions = self.getILikeQuery("access_url", urlParameters["access_url_contains"])

