from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import include, url
from django.views.generic import TemplateView
from django.views.generic import RedirectView
from django.views import static
import app.views as views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    url(r'^planetary/data/$', views.index, {'template_name': 'results.html'}, name='index'),
    url(r'^planetary/data/epn/request/count/$', views.epn_count_request, name='epn_count_request'),
    url(r'^planetary/data/pdap/request/count/$', views.pdap_count_request, name='pdap_count_request'),
    url(r'^planetary/data/compil/votable$', views.compil_votable, name='compil_votable'),
    url(r'^planetary/data/display/$', views.display_service_results, {'template_name': 'display_service_results.html'}, name='display_service'),
    url(r'^planetary/data/display/json/$', views.display_service_results_ajax),
    url(r'^planetary/data/epn/tableResult/$', views.tableResult, name='tableResult'),
    url(r'^planetary/data/epn/help/mandatory$', views.help_mandatory,name='help_mandatory'),
    url(r'^planetary/data/epn/help',TemplateView.as_view(template_name="help.html"), name='help_page'),

    url(r'^getVOTable/$', views.getVOTable, name='getVOTable'),
    
    url(r'^planetary/data/display/downloadselection/$', views.download_selection,name='download_selection'),
    url(r'^planetary/data/geojson/$', views.get_geojson,name='get_geojson'),
    url(r'^planetary/data/epn/access-url/', views.access_url ,name='access_url'),
    
    url(r'^datalink_getParameters/$', views.datalink_getParameters,  name='datalink_getParameters'),

#    url(r'^validation_monitor/$', views.validation_monitor, {'template_name': 'validation_monitor.html'}, name='validation_monitor'),
#    url(r'^validation_monitor/single_test/$', views.validation_monitor_single_test, {'template_name': 'validation_monitor_single_test.html'}, name='validation_monitor_single_test'),
#    url(r'^test/$', views.test,  name='test'),
#    url(r'^stats/$', TemplateView.as_view(template_name="stats.html")),


#
#     url(r'^test/$', 'app.views.test', {'template_name': 'test.html'}, name='test'),
#     url(r'^test_timeline/$', 'app.views.test_timeline', name='test_timeline'),
#     url(r'^tableTimeLineResult/$', 'app.views.tableTimeLineResult', name='tableTimeLineResult'),

#    url(r'^sampvotable', 'samp_votable', name='samp_votable'),
#    url(r'^example/$', 'app.views.queryExample',{'template_name': 'query_result_all.html'}, name='example'),
#    url(r'^test_yaml/$', TemplateView.as_view(template_name="test_yaml.html")),
#    url(r'^easter-egg/$', TemplateView.as_view(template_name="easter_egg.html")),
#    url(r'^allparameters$', 'query_specific_resource', {'template_name': 'query_specific_resource.html'}, name='query_specific_resource'),
    url(r'^media/(?P<path>.*)$', static.serve,
        {'document_root': settings.MEDIA_ROOT}),
                       
    url(r'^robots\.txt$', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    url(r'^$', RedirectView.as_view(url='planetary/data/')),

#    url(r'^$', RedirectView.as_view(url='planetary/data?query=form&services=all')),
    # Examples:
    # url(r'^$', 'app.views.home', name='home'),
    # url(r'^app/', include('app.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
]

urlpatterns += staticfiles_urlpatterns()
