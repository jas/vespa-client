import urllib
#    PDAP_SERVICES = [{'name' = "ESAC",'url'='Paris Observatory'},{'name' ="JAXA", 'url',"Paris Observatory"]
#ESAC = {'name': 'PSA','url':'http://psa.esac.esa.int:8000/aio/jsp/metadata.jsp', 'active': False} 
#ESAC = {'name': 'PSA','url':'none'} 
ESAC = {'name': 'PSA','url':'https://archives.esac.esa.int/psa/pdap/metadata', 'active': True} 

JAXA = {'name': 'DARTS','url':'http://darts.jaxa.jp/planet/pdap/0.3/php/index.php', 'active': True}
PDAP_SERVICES = [ESAC,JAXA]
    
def getPdapQuery(form):
    queryDict = {}
    queryDict['RESOURCE_CLASS'] = 'DATA_SET'
    if form != None and form.is_bound :
        for f in form.cleaned_data:
            if form.cleaned_data[f] :
                if f == 'dataset_id':
                    queryDict['DATASET_ID'] = form.cleaned_data[f]
                if f == 'target_name':
                    queryDict['TARGET_NAME'] = form.cleaned_data[f]
                if f == 'instrument_host_name':
                    queryDict['MISSION_NAME'] = form.cleaned_data[f]
                if f == 'instrument_name':
                    queryDict['INSTRUMENT_NAME'] = form.cleaned_data[f]
        
    first = True
    query = ''
    for key, value in queryDict.items():
        if not first:
            query = query + '&'
        else:
            first = False
        # TODO Mettre en maj
        query = query + key + '=' + value.upper()
    query = query.replace(" ","+")
    return query

def getPdapServices(query):
    pdapServices = []
    for s in PDAP_SERVICES:
        service = {}
        service['name'] = s['name']
        service['active'] = s['active']
        service['url_query'] = s['url'] + '?' + query
        service['url_query_encoded'] = urllib.quote(service['url_query'].encode("utf-8"))
        pdapServices.append(service)
    return pdapServices
        
        
        