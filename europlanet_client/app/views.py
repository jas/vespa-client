# from forms import MandatoryParametersForm, OptionalParametersForm, AdvancedParametersForm, CustomResourceForm, TextForm, HASHLISTPARAMETERS
from forms import MandatoryParametersForm, AdvancedParametersForm, CustomServiceForm, TextForm, HASHLISTPARAMETERS
from django.http import HttpResponse, Http404, HttpResponseNotFound, HttpResponseRedirect, HttpResponseServerError, HttpResponseBadRequest
from django.shortcuts import render_to_response, redirect, render
from django.forms.utils import ErrorList
import urllib, urllib2
import atpy
import StringIO
import base64
import math
import sys
import json
import numpy
from dateutil.parser import parse as dt_parse
from lxml import etree
from DateConversion import DateTime2jd, jd2DateTime
from django.forms.formsets import formset_factory
import settings
import os
from django.core.urlresolvers import reverse
import decimal
from registry import getEpnServices, getServiceByID
from service import *
from pdap import *
import urlparse
import tapQuery
import registry
import query
import TableMetadataReader
import socket
import threading
import xmlReader
MUTEX = threading.Lock()

ADVANCED_PREFIX = 'advanced_'

def getDisplayMandatoryFields(mandatoryParametersForm):
#     help = getHelp()
#    fields = {}
#     for field in mandatoryParametersForm:
#         if help.has_key(field.html_name):
#             field.help_text = help[field.html_name]
#         fields[field.html_name] = field
        
    display_fields = {}
    
    display_fields['main'] = {
                              'target_name' : mandatoryParametersForm["target_name"],
#                              'left_fields' : [mandatoryParametersForm["target_name"], mandatoryParametersForm["dataset_id"]],
                              'checkboxes' : [mandatoryParametersForm["target_class"],mandatoryParametersForm["dataproduct_type"]],
                              'instrument_host_name' : mandatoryParametersForm['instrument_host_name'],
                              'instrument_name' :  
                                    {
                                      'value': mandatoryParametersForm['instrument_name'],
                                      'op' : mandatoryParametersForm['instrument_name_op']
                                    },
  
                              'processing_level' : mandatoryParametersForm["processing_level"]
                              
                              }
    
    display_fields['dataReference'] = {
                                'string_fields': [mandatoryParametersForm["granule_uid"], mandatoryParametersForm["granule_gid"], mandatoryParametersForm['obs_id'],mandatoryParametersForm["measurement_type"]]
        }
    display_fields['location'] = {
                                  'spatial_frame_type' : mandatoryParametersForm['spatial_frame_type'],
                                  'location_interval_op' : mandatoryParametersForm['location_interval_op'],
                                  'location_values_op' : mandatoryParametersForm['location_values_op'],
                                  'fields':[
                                            {
                                            'min': mandatoryParametersForm['c1min'],
                                            'max': mandatoryParametersForm['c1max'],
                                            'center' : mandatoryParametersForm['c1center'],
                                            'delta' : mandatoryParametersForm['c1delta'],
                                            },
                                            {
                                            'min': mandatoryParametersForm['c2min'],
                                            'max': mandatoryParametersForm['c2max'],
                                            'center' : mandatoryParametersForm['c2center'],
                                            'delta' : mandatoryParametersForm['c2delta'],
                                            },
                                            {
                                            'min': mandatoryParametersForm['c3min'],
                                            'max': mandatoryParametersForm['c3max'],
                                            'center' : mandatoryParametersForm['c3center'],
                                            'delta' : mandatoryParametersForm['c3delta'],
                                            },
                                            {
                                            'min': mandatoryParametersForm['c1_resol_min'],
                                            'max': mandatoryParametersForm['c1_resol_max'],
                                            'center' : mandatoryParametersForm['c1_resol_center'],
                                            'delta' : mandatoryParametersForm['c1_resol_delta'],
                                            },
                                            {
                                            'min': mandatoryParametersForm['c2_resol_min'],
                                            'max': mandatoryParametersForm['c2_resol_max'],
                                            'center' : mandatoryParametersForm['c2_resol_center'],
                                            'delta' : mandatoryParametersForm['c2_resol_delta'],
                                            },
                                            {
                                            'min': mandatoryParametersForm['c3_resol_min'],
                                            'max': mandatoryParametersForm['c3_resol_max'],
                                            'center' : mandatoryParametersForm['c3_resol_center'],
                                            'delta' : mandatoryParametersForm['c3_resol_delta'],
                                            },
                                            ]                           
                                  }
    
    display_fields['spectral'] = {
                                  'spectral_interval_op' : mandatoryParametersForm['spectral_interval_op'],
                                  'spectral_unit_op': mandatoryParametersForm['spectral_unit_op'],
                                  'fields_without_unit':[
                                            {
                                            'min': mandatoryParametersForm['spectral_resolution_min'],
                                            'max': mandatoryParametersForm['spectral_resolution_max'],
                                            },
                                            ],
                                  'fields_with_unit':[
                                            {
                                            'min': mandatoryParametersForm['spectral_range_min'],
                                            'max': mandatoryParametersForm['spectral_range_max'],
                                            },
                                            {
                                            'min': mandatoryParametersForm['spectral_sampling_step_min'],
                                            'max': mandatoryParametersForm['spectral_sampling_step_max'],
                                            },
                                            ]
                                  }

    display_fields['time'] = {
                                    'time_search': {
                                        'time_search_type_op' : mandatoryParametersForm['time_search_type_op'],
                                        'time_interval_type_op' : mandatoryParametersForm['time_interval_type_op'],
                                        'time_min' : mandatoryParametersForm['time_min'],
                                        'time_max' : mandatoryParametersForm['time_max'],
                                        'time_center' : mandatoryParametersForm['time_center'],
                                        'time_delta_value_op' : mandatoryParametersForm['time_delta_value_op'],
                                        'time_delta_unit_op' : mandatoryParametersForm['time_delta_unit_op'],        
                                },
    
    
                                  'minmax_fields':[
                                            {
                                            'min': {'value': mandatoryParametersForm['time_exp_min'], 'op': mandatoryParametersForm['time_exp_min_op']},
                                            'max': {'value': mandatoryParametersForm['time_exp_max'], 'op': mandatoryParametersForm['time_exp_max_op']}
                                            },
                                            {
                                            'min': {'value': mandatoryParametersForm['time_sampling_step_min'], 'op': mandatoryParametersForm['time_sampling_step_min_op']},
                                            'max': {'value': mandatoryParametersForm['time_sampling_step_max'], 'op': mandatoryParametersForm['time_sampling_step_max_op']}
                                            },
                                            ]
                                  }
    display_fields['Illumination'] = {
                                  'fields':[
                                            {
                                            'min': {'value': mandatoryParametersForm['phase_min'], 'op': mandatoryParametersForm['phase_min_op']},
                                            'max': {'value': mandatoryParametersForm['phase_max'], 'op': mandatoryParametersForm['phase_max_op']}
                                            },
                                            {
                                            'min': {'value': mandatoryParametersForm['incidence_min'], 'op': mandatoryParametersForm['incidence_min_op']},
                                            'max': {'value': mandatoryParametersForm['incidence_max'], 'op': mandatoryParametersForm['incidence_max_op']}
                                            },
                                            {
                                            'min': {'value': mandatoryParametersForm['emergence_min'], 'op': mandatoryParametersForm['emergence_min_op']},
                                            'max': {'value': mandatoryParametersForm['emergence_max'], 'op': mandatoryParametersForm['emergence_max_op']}
                                            },
                                            ]
                                  }

    display_fields['instrument'] = {
                                  }

    display_fields['optional'] = {
                                  'species' :  mandatoryParametersForm['species'],
                                  'feature_name' :  mandatoryParametersForm['feature_name'],
                                  }
    

    return display_fields


def IsFormBounded(request):
    if request.GET.has_key(MandatoryParametersForm.REFERENCE_FIELD_ID):
        return True
    else:
        return False
    
def index(request, **kwargs):
    import pdsQuery
    
    if not IsFormBounded(request):
        mandatoryParametersForm = MandatoryParametersForm(auto_id='%s', error_class=DivErrorList)
#         optionalParametersForm = OptionalParametersForm(auto_id='%s', error_class=DivErrorList)
        customServiceForm = CustomServiceForm(auto_id='%s', error_class=DivErrorList)
        
        textForm = TextForm(auto_id='%s', error_class=DivErrorList, initial={'text_query': "target_name = 'Saturn' AND target_class LIKE '%planet%'"})

        epnServices = getEpnServices()
        queryWhere = query.EmptyADQLQueryWhere()
        pdapQuery = getPdapQuery(None)
        pdsLink, pdsQuery = pdsQuery.getPdsQuery(None)
        pdapServices = getPdapServices(pdapQuery)
        
    else:
        mandatoryParametersForm = MandatoryParametersForm(request.GET, auto_id='%s', error_class=DivErrorList)
#         optionalParametersForm = OptionalParametersForm(request.GET, auto_id='%s', error_class=DivErrorList)
        customServiceForm = CustomServiceForm(request.GET, auto_id='%s', error_class=DivErrorList)
        textForm = TextForm(request.GET, auto_id='%s', error_class=DivErrorList)
        error = False
        epnServices = []
        if request.GET["services"] == "form_registered_services":
            if mandatoryParametersForm.is_valid():
                optionalParameterList = []
                epnServices = getEpnServices()
            else:
                error = True
                epnServices = []
                pdapQuery = None
                pdapServices = []
    
        elif request.GET["services"] == "form_custom_service":
            if mandatoryParametersForm.is_valid() and customServiceForm.is_valid():  # All validation rules pass
            # If a custom resource is wanted, resources list is constructed with the resource url and schema filled in the form
                
                epnServices = [getCustomService(customServiceForm)]
                pdapServices = []
                pdapQuery = None
            else:
                error = True
                epnServices = []
                pdapQuery = None
                pdapServices = []
        else:
            return HttpResponseServerError()
        if request.GET["query_source"] == "form":
            pdapQuery = getPdapQuery(mandatoryParametersForm)
            pdsLink, pdsQuery = pdsQuery.getPdsQuery(mandatoryParametersForm)
            pdapServices = getPdapServices(pdapQuery)
            if mandatoryParametersForm.is_valid():
                queryWhere = query.FormADQLQueryWhere(mandatoryParametersForm = mandatoryParametersForm)
                textForm = TextForm(auto_id='%s', error_class=DivErrorList, initial={'text_query':queryWhere.getQueryConditions()})
            else:
                error = True
                queryWhere = query.EmptyADQLQueryWhere()
        elif request.GET["query_source"] == "text":
            pdapQuery = None
            pdapServices = []
            pdsLink = None
            pdsQuery = None
            textForm = TextForm(request.GET, auto_id='%s', error_class=DivErrorList)
            if textForm.is_valid() :
                queryWhere = query.TextADQLQueryWhere(text_query = textForm.cleaned_data['text_query'])
            else:
                error = True
                queryWhere = query.EmptyADQLQueryWhere()
        else:
            return HttpResponseServerError()


    for service in epnServices:
        service.setQuery(queryWhere.getQueryConditions())
        

    display_mandatory_fields = getDisplayMandatoryFields(mandatoryParametersForm)        
    display_custom_fields = getDisplayCustomFields(customServiceForm)
    
    display_service_url = reverse('display_service') + '?'
    display_service_url = addFormValuesToUrl(display_service_url, request)
    
    return render(request, kwargs['template_name'], {
         'display_mandatory_fields' : display_mandatory_fields,
         'display_custom_fields' : display_custom_fields,
         'textForm' : textForm,
         'epnServices': epnServices,
         'epnQuery': queryWhere.getQueryConditions(),
         'pdapServices' : pdapServices,
         'pdapQuery': pdapQuery,
         'pdsLink' : pdsLink,
         'pdsQuery': pdsQuery,
         'display_service_url' : display_service_url
    })
     

# def getResourceColumns(resource):
#     import registry
#     columns = None
#     try:
#         if resource.id != None:
#             columns = registry.getResourceColumns(resource.id)
#             return columns
#     except registry.NoColumnInformationException:
#         pass
#     import TableMetadataReader
#     tableMetadata = TableMetadataReader.TableMetadata(resource.access_url, resource.table)
#     columns = tableMetadata.getColumns()
#     return columns

# def getResourceColumns(resource):
#     import tapQuery
#     COLUMN_NAMES = ['column_name','unit','ucd','description','datatype']
#     query = "SELECT " + " ,".join(COLUMN_NAMES) + " FROM tap_schema.columns WHERE table_name='" + resource.table + "'"
#     FORMAT = 'json'
#     result = tapQuery.run(resource.access_url, query,format = FORMAT)
#     if len(result) == 0:
#         raise NoColumnInformationException(ivoid)
#     else:
#         columns = []
#         for row in result:
#             columns.append({'name':row[0],'unit':row[1],'ucd':row[2],'description':row[3],'datatype':row[4]})
#         return columns

def getServiceHeaders(service):
    import xmlReader
    # Some resources don't have their columns declared at the registry level so we have to go to VOSI page to get columns informations
    if service.shortname == "Venus atmospheric profiles":
        tableMetadata = TableMetadataReader.TableMetadata(service.access_url, service.table)
        service_columns = tableMetadata.getColumns()
    else:
        try:
            service_columns = registry.getServiceColumns(service.id)
        except registry.NoColumnInformationException:
            tableMetadata = TableMetadataReader.TableMetadata(service.access_url, service.table)
            service_columns = tableMetadata.getColumns()
#     schema = getXmlSchemaElements(resource.access_url, resource.table)
#     schemaElement = schema[0]
#     columnElements = schemaElement.xpath('../column')

    default_columns = ['granule_uid', 'dataproduct_type', 'target_name', 'time_min', 'time_max', 'access_url' ]
    always_requested_columns = ['granule_uid', 'access_url', 'thumbnail_url','s_region', 'target_name']
    default_sort_name = 'granule_uid'

    headers_default = []
    headers_other = []
    default_sort = 0
    
    has_data_access_url = False
#     for index, columnElement in enumerate(columnElements):
    for index, column in enumerate(service_columns):
        columnName = column['name']
        ucd = column['ucd']
        description = column['description']
        unit = column['unit']
        
#         columnName = columnElement.xpath('name')[0].text
        if columnName in default_columns:
            visible = True
            header_list = headers_default
        else:
            visible = False
            header_list = headers_other
            
        if columnName in always_requested_columns:
            always_requested = True
        else:
            always_requested = False
            
        if columnName == default_sort_name:
            default_sort = index
            
#         unitElements = columnElement.xpath('unit')
#         if len(unitElements) == 1:
#             unit = unitElements[0].text
        
        link = False
        if ucd != None and "meta.ref.url" in ucd:
                link = True
            
#         ucdElements = columnElement.xpath('ucd')
#         if len(ucdElements) == 1:
#             ucd = ucdElements[0].text
#             if "meta.ref.url" in ucd:
#                 link = True
        
#         descriptionElements = columnElement.xpath('description')
#         if len(descriptionElements) == 1:
#             description = descriptionElements[0].text
        
        JDToIsoDate = False
        if columnName=="time_min" or columnName=="time_max" or ( unit == 'd' and  "time.epoch" in ucd and not "stat.error" in ucd):
            JDToIsoDate = True
        
        
        if columnName == "access_url":
            has_data_access_url = True
        header = {'text' : columnName,
                  'visible': visible,
                  'always_requested': always_requested,
                  'unit': unit,
                  'link': link,
                  'JDToIsoDate' : JDToIsoDate,
                  'description' : description
                  }
        if unit != None and unit == 'Hz' and (columnName=="spectral_range_min" or columnName=="spectral_range_max"):
            header['unit_type'] = 'frequency'
            header['unit_value'] = 'Hz'
#         if len(unitElements) == 1:
#             if unitElements[0].text == 'Hz' and (columnName=="spectral_range_min" or columnName=="spectral_range_max"):
#                 header['unit_type'] = 'frequency'
#                 header['unit_value'] = 'Hz'
        
        header_list.append(header)
    
    headers = headers_default + headers_other
    return headers, default_sort, has_data_access_url
        
def display_service_results(request,**kwargs):
    try:
        if not IsFormBounded(request):
            if request.GET.has_key("service_id"):
                service = getServiceByID(request.GET["service_id"])
            else:
                service = Service(request.GET['service_access_url'], request.GET['service_schema'])
            mandatoryParametersForm = MandatoryParametersForm(auto_id='%s', error_class=DivErrorList)
            customServiceForm = CustomServiceForm(auto_id='%s', error_class=DivErrorList)        
            textForm = TextForm(auto_id='%s', error_class=DivErrorList)
            advanced_form_values = getAdvancedFormValues(request.GET)
            advanced_form = getServiceFields(service, mandatoryParametersForm)
    
            queryWhere = query.EmptyADQLQueryWhere()
            
        else:
            mandatoryParametersForm = MandatoryParametersForm(request.GET, auto_id='%s', error_class=DivErrorList)
            customServiceForm = CustomServiceForm(request.GET, auto_id='%s', error_class=DivErrorList)
            if request.GET["services"] == "form_custom_service":
                if mandatoryParametersForm.is_valid() and customServiceForm.is_valid():  # All validation rules pass
                # If a custom resource is wanted, resources list is constructed with the resource url and schema filled in the form
                    service = getCustomService(customServiceForm)
                else:
                    return HttpResponseServerError()
            elif request.GET.has_key("service_id"):
                service = getServiceByID(request.GET["service_id"])
            else:
                service = Service(request.GET['service_access_url'], request.GET['service_schema'])
                
            textForm = TextForm(auto_id='%s', error_class=DivErrorList)
            advanced_form_values = getAdvancedFormValues(request.GET)
            advanced_form = getServiceFields(service, mandatoryParametersForm)
    
            if request.GET['query_source'] == 'form':
                if mandatoryParametersForm.is_valid():
                    queryWhere = query.FormADQLQueryWhere(mandatoryParametersForm = mandatoryParametersForm, advanced_parameters= advanced_form_values, service = service)
                    textForm = TextForm(auto_id='%s', error_class=DivErrorList, initial={'text_query':queryWhere.getQueryConditions()})
                else:
                    return HttpResponseServerError()
            elif request.GET['query_source'] == 'text':
                textForm = TextForm(request.GET, auto_id='%s', error_class=DivErrorList)
                if textForm.is_valid() :
                    queryWhere = query.TextADQLQueryWhere(text_query = textForm.cleaned_data['text_query'])
                else:
                    return HttpResponseServerError()        
            else:
                return HttpResponseServerError()
            
        display_mandatory_fields = getDisplayMandatoryFields(mandatoryParametersForm)
        display_custom_fields = getDisplayCustomFields(customServiceForm)
        service.setQuery(queryWhere.getQueryConditions())
        service_shortname = service.shortname
        if not service_shortname:
            service_shortname = service.schema
        headers, default_sort, has_data_access_url = getServiceHeaders(service)
        
        targets = is_geojsonisable(service)
        back_to_services_results_url = reverse('index') + '?'
        back_to_services_results_url = addFormValuesToUrl(back_to_services_results_url, request)
        datalink = {}
        try:
            datalink = xmlReader.getServiceDatalink(service.url_base, service.url_metadata)
            header = {'text' : 'datalink',
                      'visible': True,
                      'always_requested': False,
                      'unit': None,
                      'link': False,
                      'JDToIsoDate' : False,
                      'description' : 'Datalink'
                      }
            headers.append(header)
        except:
            pass
        paging = "True"
        try:
            table = tapQuery.run(service.access_url, service.test_offset_query,format="json")
        except:
            paging = "False"
        return render(request, kwargs['template_name'], {
                                                         'display_mandatory_fields' : display_mandatory_fields,
                                                         'display_custom_fields' : display_custom_fields,
                                                         'service_id' : service.id,
                                                         'service_schema' : service.schema,
                                                         'service_shortname' : service_shortname,
                                                         'service_reference_url' : service.reference_url,
                                                         'service_access_url' : service.access_url,
                                                         'service_description' : service.description,
                                                         'service_table' : service.table,
                                                         "service_creators" : service.creator,
                                                         "service_contributors" : service.contributors,
                                                         "service_copyright" : service.copyright,
                                                         "service_title" : service.title,
                                                         'query_conditions' : queryWhere.getQueryConditions(),
                                                         'headers' : headers,
                                                         'default_sort' : default_sort,
                                                         'base_url_query_votable' : service.empty_query,
                                                         'adql_query_all' : service.adql_query_all,
                                                         'url_query_select' : service.url_query_select,
                                                         'targets': targets,
                                                         'has_data_access_url' : has_data_access_url,
                                                         'advanced_form_values' : advanced_form_values,
                                                         'advanced_form' : advanced_form,
                                                         'textForm': textForm,
                                                         'back_to_services_results_url' : back_to_services_results_url,
                                                         'datalink' : datalink,
                                                         'paging' : paging
            })
    except TableUrlNotFoundException , url_tables:
        print url_tables


# def getCollectionResources(query):
#     resources = getEpnResources()
#     resourcesWithResult = []
#     for resource in resources:
#         resource.setQuery(query)
#         res = getEpnCountResult(resource.url_query_count)
#         if res > 0:
#             resourcesWithResult.append(resource)
#             print resource.shortname
#     return resourcesWithResult
    
# def getColumnsFromCollection(resources):
#     import registry
#     headers = []
#     has_data_access_url = False
# #     registry.getResourcesColumns(resources)
#     for resource in resources:
#         columns=[]
#         resource_headers, _, resource_has_data_access_url = getResourceHeaders(resource)
#         if not has_data_access_url and resource_has_data_access_url:
#             has_data_access_url = True
#         for resource_header in resource_headers:
#             is_header_already_added = False
#             for header in headers:
#                 if resource_header['text']== header['text']:
#                     is_header_already_added = True
#                     break
#             if not is_header_already_added:
#                 headers.append(resource_header)
#             columns.append(resource_header['text'])
#         resource.setColumns(columns)
#     return headers, has_data_access_url, resources
            
# def getFakeCollectionResources(query_conditions):
# #     BDIP_RESOURCE_ID = "ivo://vopdc.obspm/lesia/bdip/epn"
# #     EXPRES_RESOURCE_ID = "ivo://vopdc.obspm/lesia/maser/expres/epn"
# #     DYNASTVO_RESOURCE_ID = "ivo://vopdc.obspm/imcce/dynastvo/epn"
# #     MPC_RESOURCE_ID = "ivo://org.gavo.dc/mpc/q/epn_core"
#     AMDA_RESOURCE_ID = 'ivo://cdpp/amda'
#     APIS_RESOURCE_ID = 'ivo://vopdc.obspm/lesia/apis/epn'
#     HSTPLANETO_RESOURCE_ID = 'ivo://vopdc.obspm/lesia/hst_planeto/epn'
#     PVOL_RESOURCE_ID = 'ivo://pvol/pvol/q/collection'
#     resources = [getResourceByID(AMDA_RESOURCE_ID),getResourceByID(APIS_RESOURCE_ID),getResourceByID(HSTPLANETO_RESOURCE_ID),getResourceByID(PVOL_RESOURCE_ID)]
#     for resource in resources:
#         resource.setQuery(query_conditions)
#     return resources
# 
# def display_collection_results(request,**kwargs):
#     DEFAULT_SORT = 0
#     query_conditions = request.GET["query_conditions"]
# #     resources = getCollectionResources(query_conditions)
#     resources = getCollectionResources(query_conditions)
#     headers, has_data_access_url, resources = getColumnsFromCollection(resources)
#     resources_params = []
#     for resource in resources:
#         resources_params.append({"access_url":resource.access_url,"schema":resource.schema,"columns":resource.columns})
# #     for header in headers:
# #         print header['text']
# #     print 'toto'
# #     services = []
# #     for key, value in request.GET.iteritems():
# #         if key.startswith("service_id"):
# #             
# #     services = request["GET"][""]
# #     header1 = {'text' : 'test1',
# #               'visible': True,
# #               'always_requested': True,
# #               'unit': None,
# #               'link': False,
# #               'JDToIsoDate' : False,
# #               'description' : 'toto'
# #               }
# #     header2 = {'text' : 'test2',
# #               'visible': True,
# #               'always_requested': True,
# #               'unit': None,
# #               'link': False,
# #               'JDToIsoDate' : False,
# #               'description' : 'toto'
# #               }
# #     headers=[header1,header2]
#     return render(request, kwargs['template_name'], {
# #                                                      'display_mandatory_fields' : display_mandatory_fields,
# #                                                      'display_custom_fields' : display_custom_fields,
# #                                                      'resource_id' : resource.id,
# #                                                      'resource_schema' : resource.schema,
# #                                                      'resource_shortname' : resource_shortname,
# #                                                      'resource_reference_url' : resource.reference_url,
# #                                                      'resource_access_url' : resource.access_url,
# #                                                      'resource_description' : resource.description,
# #                                                      'resource_table' : resource.table,
# #                                                      "resource_creators" : resource.creator,
# #                                                      "resource_contributors" : resource.contributors,
# #                                                      "resource_copyright" : resource.copyright,
# #                                                      "resource_title" : resource.title,
#                                                     'resources_params' : resources_params,
#                                                     'query_conditions' : query_conditions,
#                                                      'headers' : headers,
#                                                     'default_sort' : DEFAULT_SORT,
# #                                                      'base_url_query_votable' : resource.empty_query,
# #                                                      'adql_query_all' : resource.adql_query_all,
# #                                                      'url_query_select' : resource.url_query_select,
# #                                                      'targets': targets,
#                                                      'has_data_access_url' : has_data_access_url,
# #                                                      'advanced_form_values' : advanced_form_values,
# #                                                      'advanced_form' : advanced_form,
# #                                                      'textForm': textForm,
# #                                                      'back_to_services_results_url' : back_to_services_results_url,
# #                                                      'datalink' : datalink
#         })
# def check_service_registration(request, **kwargs):
#     import urllib
#     import urllib2
#     from astropy.io.votable import parse
#     BASE_URL = 'http://voparis-rr.obspm.fr/tap/sync?'
#     QUERY_BASE = "SELECT COUNT (*) FROM rr.table_column WHERE rr.table_column.ivoid = '%s'"
#     resources = getEpnResources()
#     bad_resources = []
#     for resource in resources:
#         query = QUERY_BASE % (resource.id)
#         query_encoded= urllib.urlencode({'REQUEST':'doQuery', 'LANG': 'ADQL', 'QUERY': query})
#         webFile = urllib2.urlopen(BASE_URL, query_encoded)
#         votable = parse(webFile)
#         webFile.close()
#         res = votable.get_first_table().array[0][0]
#         if res==0:
#             bad_resources.append(resource)
#             print resource.shortname + " : " + resource.access_url
#     print "Number of service to register correctly : "+str(len(bad_resources))
    
    
# def display_collection_results_ajax(request, **kwargs):
#     import json
#     args = json.loads(request.POST.get("args"))
#     query = args['query_conditions']
#     resources_params = args['resources']
#     aaData=[]
#     for resource_param in resources_params:
#         resource = Resource(access_url=resource_param['access_url'], schema = resource_param['schema'])
#         resource.setQuery(query)
#         try:
#             webFile = urllib2.urlopen(resource.url_base, resource.select_paramaters_encoded)
#         except:
#             from astropy.io.votable import parse
#             votable = parse(StringIO.StringIO(error.read()))
#             data_error = {'error_msg' : votable.resources[0].infos[0].content}
#             from django.core.serializers.json import DjangoJSONEncoder    
#             jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
#             return HttpResponseBadRequest(jsonDataTable, content_type="application/json")      
#         from astropy.io.votable import parse
#         votable = parse(webFile)
#         webFile.close()
#         table = votable.get_first_table().to_table()
# #         data = table.array
#         for row in table:
#             aaData_row = {}
#             for colname in row.colnames:
#                 aaData_row[colname] = row[colname]
# #             for column in row:
# #                 print "toto"
# #                 aaData_row[column['name']]
# #             for index, column in enumerate(columns):
# #                 if column['name'] in query_column_names:
# #                     val = row[column['name']]
# #                     if val == '--':
# #                         val = None
# #                     else:
# #                         try:                   
# #                             float_val = float(val)
# #                             if math.isnan(float_val):
# #                                 val = None
# #                         except ValueError:
# #                             pass
# #                     if column['name'] == 'granule_uid':
# #                         aaData_row['DT_RowId'] = val 
# #                 else:
# #                     val = None
# #                 aaData_row[str(index)] = val
#             aaData.append(aaData_row)    
#        
#     data = {
#             'data':aaData,
# #             "recordsTotal": nb_rows,
# #             "recordsFiltered": nb_rows,
# #             "draw" : args["draw"],
#             }
#       
# #     import numpy as np
#     from astropy.utils.misc import JsonCustomEncoder
#     jsonDataTable = json.dumps(data, cls=JsonCustomEncoder)
# #     from django.core.serializers.json import DjangoJSONEncoder    
# #     jsonDataTable = json.dumps(data, cls=DjangoJSONEncoder)
#       
#     return HttpResponse(jsonDataTable, content_type="application/json")

    
# def display_collection_results_ajax(request, **kwargs):
#     import json
#     args = json.loads(request.POST.get("args"))
#     paginationStart = args['start']
#     paginationLength = args['length']
#     pagination = {'paginationStart' : paginationStart, 'paginationLength' : paginationLength}
#      
#     query = args['query_conditions']
#     resources_params = args['resources']
#     sort_index = args['order'][0]['column']
#     sort = {}
#     sort['name'] =  args['columns'][sort_index]['name']
#     sort['dir'] = args['order'][0]['dir']
#     pagination= {'sort' : sort, 'page_size' : str(paginationLength), 'offset' : str(paginationStart)}
#      
#     columns = args["columns"]
#     index = 0
#     nb_rows = 0
#     aaData=[]
#     for resource_param in resources_params:
#         query_column_names = []
#         for column in columns:
#             if column['requested'] == True and column['name'] in resource_param["columns"]:
#                 query_column_names.append(column['name'])
#         resource = Resource(access_url=resource_param['access_url'], schema = resource_param['schema'])
#         resource.setQuery(query, pagination, query_column_names)
#         try:
#             webFile = urllib2.urlopen(resource.url_base + resource.count_parameters)
#         except urllib2.HTTPError, error:
#             from astropy.io.votable import parse
#             votable = parse(StringIO.StringIO(error.read()))
#             data_error = {'error_msg' : votable.resources[0].infos[0].content}
#             from django.core.serializers.json import DjangoJSONEncoder    
#             jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
#             return HttpResponseBadRequest(jsonDataTable, content_type="application/json")      
#  
#         try:
#             webFile = urllib2.urlopen(resource.url_base, resource.select_paramaters_encoded)
#         except urllib2.HTTPError, error:
#             resource.setQuery(query, None,(column['name'] for column in columns if column['requested'] == True) )       
#             try:
#                 webFile = urllib2.urlopen(resource.url_base, resource.select_paramaters_encoded)
#             except:
#                 from astropy.io.votable import parse
#                 votable = parse(StringIO.StringIO(error.read()))
#                 data_error = {'error_msg' : votable.resources[0].infos[0].content}
#                 from django.core.serializers.json import DjangoJSONEncoder    
#                 jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
#                 return HttpResponseBadRequest(jsonDataTable, content_type="application/json")      
#  
#  
#  
#          
#         from astropy.io.votable import parse
#         votable = parse(webFile)
#         webFile.close()
#         table = votable.get_first_table().to_table()
# #         data = table.array
#         for row in table:
#             aaData_row = {}
#             for index, column in enumerate(columns):
#                 if column['name'] in query_column_names:
#                     val = row[column['name']]
#                     if val == '--':
#                         val = None
#                     else:
#                         try:                   
#                             float_val = float(val)
#                             if math.isnan(float_val):
#                                 val = None
#                         except ValueError:
#                             pass
#                     if column['name'] == 'granule_uid':
#                         aaData_row['DT_RowId'] = val 
#                 else:
#                     val = None
#                 aaData_row[str(index)] = val
#             aaData.append(aaData_row)    
#       
#     data = {
#             'data':aaData,
#             "recordsTotal": nb_rows,
#             "recordsFiltered": nb_rows,
#             "draw" : args["draw"],
#             }
#      
# #     import numpy as np
#     from astropy.utils.misc import JsonCustomEncoder
#     jsonDataTable = json.dumps(data, cls=JsonCustomEncoder)
# #     from django.core.serializers.json import DjangoJSONEncoder    
# #     jsonDataTable = json.dumps(data, cls=DjangoJSONEncoder)
#      
#     return HttpResponse(jsonDataTable, content_type="application/json")

    
def addFormValuesToUrl(url,request):
    for key, value in request.GET.iteritems():
        if key.startswith('f-') or key == 'query_source' or key == 'services':
            url = url + '&' + key + '=' + value
    return url

def getDisplayCustomFields(customServiceForm):
        
    display_fields = {}
    
    display_fields['url_op'] = customServiceForm ['url_op']
    display_fields['schema_op'] = customServiceForm ['schema_op']
                            
    return display_fields



def getAdvancedFormValues(get):
    advanced_form_values = []
    i = 0
    while(get.has_key(ADVANCED_PREFIX+str(i)+'_label')):
        advanced_form_value = {}
        advanced_form_value['label'] = get[ADVANCED_PREFIX+str(i)+'_label']
        advanced_form_value['op'] = get[ADVANCED_PREFIX+str(i)+'_op']
        advanced_form_value['value'] = get[ADVANCED_PREFIX+str(i)+'_value']
        advanced_form_values.append(advanced_form_value)
        i = i + 1
    return advanced_form_values

def getServiceFields(service, mandatoryParametersForm):
    # Some resources don't have their columns declared at the registry level so we have to go to VOSI page to get columns informations
    try:
        columns = registry.getServiceColumns(service.id)
    except registry.NoColumnInformationException:
        tableMetadata = TableMetadataReader.TableMetadata(service.access_url, service.table)
        columns = tableMetadata.getColumns()
#     schemaElement = getXmlSchemaElements(r.access_url, r.table)[0]
#     columnElements = schemaElement.xpath('../column')
    
    advanced_form = {}
    advanced_form['labels'] = []
#     for columnElement in columnElements:
#         columnNameElement = columnElement.xpath('name')[0]
#         columnName = columnNameElement.text
#         if not mandatoryParametersForm.fields.has_key(columnName):
#             advanced_form['labels'].append(columnName)
#     advanced_form['op'] = ["=",u'\u2264',u'\u2265',"LIKE"]
#     return advanced_form
    for column in columns:
#         columnNameElement = columnElement.xpath('name')[0]
        columnName = column['name']
        if not mandatoryParametersForm.fields.has_key(columnName):
            advanced_form['labels'].append(columnName)
    advanced_form['op'] = ["=",u'\u2264',u'\u2265',"LIKE"]
    return advanced_form

        
def is_geojsonisable(service):
    try:
        webFile = urllib2.urlopen(service.query_is_geojsonisable)    
        t = atpy.Table(webFile, type='vo')
        if len(t) >= 1:
            targets = []
            for line in t.data:
                targets.append(line[0])
            return targets
    except:
        pass
    return None


def display_service_results_ajax(request, **kwargs):
    args = json.loads(request.POST.get("args"))
    paginationStart = args['start']
    paginationLength = args['length']
    pagination = {'paginationStart' : paginationStart, 'paginationLength' : paginationLength}
    
    query = args['query_conditions']
    if args.has_key("service_id"):
        service = getServiceByID(args["service_id"])
    else:
        service = Service(args['service_access_url'], args['service_schema'])
    
#     resource = Resource(args['resource_access_url'], args['resource_schema'])
    sort_index = args['order'][0]['column']
    sort = {}
    sort['name'] =  args['columns'][sort_index]['name']
    sort['dir'] = args['order'][0]['dir']
    pagination= {'sort' : sort, 'page_size' : str(paginationLength), 'offset' : str(paginationStart)}
    
    columns = args["columns"]

    service.setQuery(query, pagination,(column['name'] for column in columns if column['requested'] == True) )
#     try:
#         webFile = urllib2.urlopen(resource.url_base + resource.count_parameters)
#     except urllib2.HTTPError, error:
#         from astropy.io.votable import parse
#         votable = parse(StringIO.StringIO(error.read()))
#         data_error = {'error_msg' : votable.resources[0].infos[0].content}
#         from django.core.serializers.json import DjangoJSONEncoder    
#         jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
#         return HttpResponseBadRequest(jsonDataTable, content_type="application/json")      
    
    try:
        nb_rows = tapQuery.count(service.access_url, service.count_query)
    except urllib2.HTTPError, error:
        jsonError = tapQuery.getJsonError(error)
        return HttpResponseBadRequest(jsonError, content_type="application/json")      
    try:
        table = tapQuery.run(service.access_url, service.select_query,format="json")
    except Exception, error:
        try:
            pagination["offset"] = "0"
            service.setQuery(query, pagination,(column['name'] for column in columns if column['requested'] == True) )
            table = tapQuery.run(service.access_url, service.select_query,format="json")            
        except:
            from astropy.io.votable import parse
            votable = parse(StringIO.StringIO(error.read()))
            data_error = {'error_msg' : votable.resources[0].infos[0].content}
            from django.core.serializers.json import DjangoJSONEncoder    
            jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
            return HttpResponseBadRequest(jsonDataTable, content_type="application/json")      
    
#     nb_rows = result.array[0][0]
# #     t = atpy.Table(webFile, type='vo')
# #     webFile.close()
# #     nb_rows = int(t.data[0][0])
#     try:
#         webFile = urllib2.urlopen(resource.url_base, resource.select_paramaters_encoded)
#     except urllib2.HTTPError, error:
#         resource.setQuery(query, None,(column['name'] for column in columns if column['requested'] == True) )       
#         try:
#             webFile = urllib2.urlopen(resource.url_base, resource.select_paramaters_encoded)
#         except:
#             from astropy.io.votable import parse
#             votable = parse(StringIO.StringIO(error.read()))
#             data_error = {'error_msg' : votable.resources[0].infos[0].content}
#             from django.core.serializers.json import DjangoJSONEncoder    
#             jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
#             return HttpResponseBadRequest(jsonDataTable, content_type="application/json")      
# 
#     COLUMN_NAMES = ['name','unit','ucd','column_description','datatype']
#     QUERY_BASE = "SELECT " + " ,".join(COLUMN_NAMES) + " FROM rr.table_column WHERE ivoid='%s'"
#     FORMAT = 'json'
#     query = QUERY_BASE % (ivoid)
#     aaData = tapQuery.run(BASE_URL, query,format = FORMAT)
    
    
#     
#     t = atpy.Table(webFile, type='vo', pedantic=False, verbose=False)
#     webFile.close()
#
#     table = t.array
#     numpy.ma.filled(table, -1)
#     aaData = table
    aaData=[]
    for row_index in range(0,len(table)) :
        row = table[row_index]
        col_index = 0
        aaData_row = {}
        for index, column in enumerate(columns):
            if column['requested']:
                val = row[col_index]
#                 if val == '--':
#                     val = None
#                 else:
#                     try:                   
#                         float_val = float(val)
#                         if math.isnan(float_val):
#                             val = None
#                     except ValueError:
#                         pass
#                 if column['name'] == 'granule_uid':
#                     aaData_row['DT_RowId'] = val 
                col_index = col_index + 1
            else:
                val = None
            aaData_row[str(index)] = val
        aaData.append(aaData_row)    
     
    data = {
            'data':aaData,
            "recordsTotal": nb_rows,
            "recordsFiltered": nb_rows,
            "draw" : args["draw"],
            }
    
    from astropy.utils.misc import JsonCustomEncoder
    jsonDataTable = json.dumps(data, cls=JsonCustomEncoder)
    
#     from django.core.serializers.json import DjangoJSONEncoder    
# 
#     jsonDataTable = json.dumps(data, cls=DjangoJSONEncoder)
    
    return HttpResponse(jsonDataTable, content_type="application/json")

def get_geojson(request):
    import tools.votable_to_geojson_polygon as votable_to_geojson_polygon
    import registry
    target_name = request.GET['target_name']
    query = request.GET['query_conditions']
    if query != '':
        query = query + ' AND '
    query = query + "lower(target_name)=lower('" + target_name + "')"
    columns = ['c1min','c2min','c1max','c2max','c3min', 'c3max','granule_uid','spatial_frame_type']
    service = registry.getVirtualRegistryServiceByAccessurlAndSchema(request.GET['service_access_url'], request.GET['service_schema'])
    if service == None:
        service = Service(request.GET['service_access_url'], request.GET['service_schema'] )
    if serviceHasOptionField(service, 'access_url'):
        columns.append("access_url")
    service.setQuery(query, columns=columns )
    webFile = urllib2.urlopen(service.url_query_select)
    res = votable_to_geojson_polygon.run(webFile)

    from django.core.serializers.json import DjangoJSONEncoder    
    jsonData = json.dumps(res, cls=DjangoJSONEncoder)

    response = HttpResponse(jsonData, content_type="application/json")    
    return response

def getEpnCountResult(url):
    webFile = urllib2.urlopen(url, timeout=60)
    s = webFile.read()
    webFile.close()
    t = atpy.Table(StringIO.StringIO(s), type='vo', pedantic=False, verbose=False)
    nb_rows = int(t.data[0][0])
    return nb_rows

def epn_count_request(request):
    error = False
    if request.GET.has_key("access_url") and request.GET.has_key("count_request"):
        access_url = request.GET["access_url"]
        count_request = request.GET["count_request"]
        try:
            nb_rows = tapQuery.count(access_url, count_request)
        except Exception, e:
            error = True            
    if error:        
        return HttpResponseServerError()
    else:
        result = {}
        result['nb_rows'] = str(nb_rows)
        if nb_rows > MAXREC:
            result['all_results'] = False
        else:
            result['all_results'] = True
        jsonresult = json.dumps(result)
        response = HttpResponse(jsonresult, content_type="application/json")    
        return response
    
def pdap_count_request(request):
    error = False
    if request.GET.has_key("url"):
        url = request.GET["url"]
        webFile = urllib2.urlopen(url, timeout=60)
        s = webFile.read()
        webFile.close()
        try:
            t = atpy.Table(StringIO.StringIO(s), type='vo', pedantic=False, verbose=False)
            nb_rows = len(t)
        except Exception, e:
            error = True
    if error:        
        return HttpResponseServerError()
    else:
        result = {}
        result['nb_rows'] = nb_rows
        if nb_rows > MAXREC:
            result['all_results'] = False
        else:
            result['all_results'] = True
        jsonresult = json.dumps(result)
        response = HttpResponse(jsonresult, content_type="application/json")    
        return response

def access_url(request):
    from registry import getVirtualRegistryServiceByShortname
    
    if (request.GET.has_key("file_name") and request.GET.has_key("access_url_contains")) or not (request.GET.has_key("file_name") or request.GET.has_key("access_url_contains")):
        return HttpResponseBadRequest()    
    queryWhere = query.UrlADQLQueryWhere(request.GET)
    parameters = queryWhere.params
    if request.GET.has_key('service_shortname'):
        parameters['service_shortname'] = request.GET["service_shortname"]
        epnService = getVirtualRegistryServiceByShortname(request.GET['service_shortname'])
        if epnService == None:
            epnServices = []
        else:
            epnServices = [epnService]
    else:
        epnServices = getEpnServices()
    results = []
    services_OK = []
    services_KO = []
    threads = []
    for service in epnServices:
        thread = threading.Thread(target=service_access_url,args=(service, queryWhere, results, services_OK, services_KO))
        thread.start()
        threads.append(thread)
    
    for thread in threads:
        thread.join()
    services_OK.sort()
    services_KO.sort()
    
    jsonresults = {'parameters': parameters,'results':results,'services':{'services answering':services_OK,'services not answering':services_KO}}
    jsonFile = json.dumps(jsonresults)
    response = HttpResponse(jsonFile, content_type="application/json")
    return response


def service_access_url(service, queryWhere, results, services_OK, services_KO):
    service.setQuery(queryWhere.getQueryConditions(), columns=["access_url"])
    Is_service_OK = True
    try:
        webFile = urllib2.urlopen(service.url_base, service.select_paramaters_encoded,timeout=10)
        t = atpy.Table(webFile, type='vo', pedantic=False, verbose=False)
        webFile.close()
        if len(t) > 0:
            with MUTEX:
                service_result = {"service_shortname":service.shortname, "access_url":[]}
            for row_index in range(0,len(t)):
                row = t.row(row_index,python_types=True)
                service_result["access_url"].append(row[0])
            results.append(service_result)
    except socket.timeout, e:
        Is_service_OK = False
    except:
        pass
    with MUTEX:
        if Is_service_OK:
            services_OK.append(service.shortname)
        else:
            services_KO.append(service.shortname)

# def access_url(request):
    # import socket
    # from registry import getVirtualRegistryResourceByShortname
    #
    # if (request.GET.has_key("file_name") and request.GET.has_key("access_url_contains")) or not (request.GET.has_key("file_name") or request.GET.has_key("access_url_contains")):
        # return HttpResponseBadRequest()    
    # queryWhere = query.UrlADQLQueryWhere(request.GET)
    # parameters = queryWhere.params
    # if request.GET.has_key('service_shortname'):
        # parameters['service_shortname'] = request.GET["service_shortname"]
        # epnResource = getVirtualRegistryResourceByShortname(request.GET['service_shortname'])
        # if epnResource == None:
            # epnResources = []
        # else:
            # epnResources = [epnResource]
    # else:
        # epnResources = getEpnResources()
    # results = []
    # services_OK = []
    # services_KO = []
    # for resource in epnResources:
        # resource.setQuery(queryWhere.getQueryConditions(), columns=["access_url"])
        # Is_service_OK = True
        # try:
            # webFile = urllib2.urlopen(resource.url_base, resource.select_paramaters_encoded,timeout=10)
            # t = atpy.Table(webFile, type='vo', pedantic=False, verbose=False)
            # webFile.close()
            # if len(t) > 0:
                # service_result = {"resource_shortname":resource.shortname, "access_url":[]}
                # for row_index in range(0,len(t)):
                    # row = t.row(row_index,python_types=True)
                    # service_result["access_url"].append(row[0])
                # results.append(service_result)
        # except socket.timeout, e:
            # Is_service_OK = False
        # except:
            # pass
        # if Is_service_OK:
            # services_OK.append(resource.shortname)
        # else:
            # services_KO.append(resource.shortname)
    # services_OK.sort()
    # services_KO.sort()
    #
    # jsonresults = {'parameters': parameters,'results':results,'services':{'services answering':services_OK,'services not answering':services_KO}}
    # jsonFile = json.dumps(jsonresults)
    # response = HttpResponse(jsonFile, content_type="application/json")
    # response['Content-Disposition'] = 'attachment; filename=vespa_filegrab.json'
    #
    #
    #
    # return response

def getDefaultCoordinatesQuery(data):
    query = ''
    for c in MandatoryParametersForm.COORDINATE_VALUE_FIELDS:
        if data[c] != None:
            if c.endswith('min'):
                query = query + (' AND ' + c + ' >= ' + str(data[c]))
            else:
                query = query + (' AND ' + c + ' <= ' + str(data[c]))
    return query
                
def getAllTargetNames(target_name):
    url = 'http://voparis-registry.obspm.fr/ssodnet/1/search?q=' + target_name
    import urllib, json
    response = urllib.urlopen(url);
    data = json.loads(response.read())
    allTargetNames = []
    return [target_name]

def help_mandatory(request):
    help = loadJsonHelpFile()
    help = help["help_text"]
    help_json = json.dumps(help)
    return HttpResponse(help_json, content_type="application/json")    

def loadJsonHelpFile():
    helpJson = None
    fileAdr = os.path.join(settings.DJANGO_PROJECT_PATH, '../media/json/help.json')
    try:
        with open(fileAdr, 'r') as input:
            helpJson = json.load(input)
            input.close()
    except Exception as e:
        sys.stderr.write("Fail loading jsonFile\n")
    return helpJson

def getformsetHelp(formset):
    help = {}
    helpJson = loadJsonHelpFile()
    for form in formset:
        try :
            fieldName = form.cleaned_data['name']
            help[fieldName] = getStringHelp(helpJson, fieldName)
        except Exception :
#            print "exception with %s" % form.cleaned_data['name']
            pass
    return help
        
        
def getFormHelp(form, helpJson):
    help = {}
    for field in form.fields:
        try :
            help[field] = getStringHelp(helpJson, field)
        except Exception :
            pass
    return help

def getStringHelp(helpJson, fieldName):
    if helpJson['help_text'][fieldName]:
        return helpJson['help_text'][fieldName]
    else :
#        print fieldName
        raise Exception
    
def tableResult(request):
    jsonDataTable = {}
    url = request.GET["query_url"]
    service_type = request.GET["service_type"]
    
    webFile = urllib2.urlopen(url)
    s = webFile.read()
    webFile.close()
    t = atpy.Table(StringIO.StringIO(s), type='vo', pedantic=False, verbose=False)
    
    # first column = id, invisible for users
    aoColumns = []
    if service_type == "epn":
        data = getEpnData(t)
    else:
        data = getPdapData(t)
    jsonDataTable = {}
    jsonDataTable = json.dumps(data)
    return HttpResponse(jsonDataTable, content_type="application/json")    
            
def getPdapData(t):
    aoColumns = []
    for columnKey in t.columns.keys:
        dict = {}
        dict["sTitle"] = columnKey
        if not (("DATA_SET_ID" in columnKey) or ("MISSION_NAME" in columnKey) or ("START_TIME" in columnKey) or ("STOP_TIME" in columnKey)):
            dict["bVisible"] = False
        aoColumns.append(dict)
    aaData = []
    for row in t.data:
        try:
            line = []
            # 1st column value : samp_data_type
            for element in row:
                try:                   
                    float_element = float(element)
                    if math.isnan(float_element):
                        element = ''
                    else:
                        element = str(element)
                except ValueError:
                    pass
                # Add clickable link for data
                line.append(element)
            aaData.append(line)
        except Exception, e:
#            print e
            raise e

    data = {
            "aoColumns" : aoColumns,
            "aaData" : aaData
            }

    return data    
    
    
    

def getEpnData(t):
    aoColumns = []
        
    # 1st column : samp_data_type
    dict = {}
#     dict["sTitle"] = "samp_data_type"
#     dict["sClass"] = "samp_data_type"
#     dict["bVisible"] = False
#     aoColumns.append(dict)
    url_indice = -1
    time_min_indice = -1
    time_max_indice = -1
    dataproduct_type_indice = -1
    for indice,columnKey in enumerate(t.columns.keys):
        dict = {}
        i = t.columns.keys.index(columnKey)
        dict["sTitle"] = columnKey
        if not columnKey in ["dataproduct_type", "target_name", "time_min", "time_max", "access_url"]:
            dict["bVisible"] = False
        if t.columns.values[i].dtype.char != 'S' and 'a':
                dict["sType"] = "numeric"
        if columnKey == 'index':
            dict["sClass"] = "id"
        else:
            if columnKey == "preview_url":
                dict["sClass"] = "preview"
            else:
                if columnKey == "access_url":
                    dict["sClass"] = "url"
                    url_indice = indice
                else:
                    if columnKey == "time_min":
                        time_min_indice = indice
                        dict["sType"] = "date"
                    else:
                        if columnKey == "time_max":
                            time_max_indice = indice
                            dict["sType"] = "date"
                        else : 
                            if columnKey == "dataproduct_type":
                                dict["sClass"] = "dataproduct_type"
                                dataproduct_type_indice = indice
                            
                            
                            
                            
                            
        if t.columns.values[i].unit:
            
            dict["sTitle"] = dict["sTitle"] + " (" + str(t.columns.values[i].unit) + ")"
#                        else:
#                             if columnKey == "access_estsize":
#                                 dict["sTitle"] = columnKey + " (kB)"
#            def urlRender(data, type, row):
#                return "<a href=\"" + data + "\">" + data + "</a>"
#            dict["mRender"]= urlRender
#        if columnKey == "access_format":
#            dict["sClass"] = "format"
#        if columnKey == "dataproduct_type":
#            dict["sClass"] = "type"
        
        aoColumns.append(dict)
        
    aaData = []
    id = 0
    for indice, row in enumerate(t.data):
        try:
            line = []
            # 1st column value : samp_data_type
#            samp_data_type = getEpnSampDataType(t,indice)
#            line.append(samp_data_type)
            indice = 0
            for element in row:
                try:
#                     ('im', 'image'), ('sp', 'spectrum'), ('ds', 'dynamic_spectrum'),
#                                                 ('sc', 'spectral_cube'), ('pr', 'profile'), ('vo', 'volume'),
#                                                 ('mo', 'movie'), ('cu', 'cube'), ('ts', 'time_series'), ('ca', 'catalog'),
#                                                 ('sv', 'spatial_vector')),
                    if indice == dataproduct_type_indice:
                        expandDataProductName ={
                                    'im': 'image',
                                    'sp': 'spectrum',
                                    'ds': 'dynamic_spectrum',
                                    'sc': 'spectral_cube',
                                    'pr': 'profile',
                                    'vo': 'volume',
                                    'mo': 'movie',
                                    'cu': 'cube',
                                    'ts': 'time_series',
                                    'ca': 'catalog',
                                    'sv': 'spatial_vector',
                                    }
                        if element in expandDataProductName:
                            element = expandDataProductName[element]
                        else:
                            element
#                         {'ca': 'catalog',
# #                          'option2': function2,
# #                          'option3': function3,
# #                          'option4': function4,
#                          }.get(element, '')()
                    float_element = float(element)
                    if math.isnan(float_element):
                        element = ''
                    else :
                        if indice == time_min_indice or indice == time_max_indice:
                            # conversion from julian date to iso date
                            try:
                                dateTime = jd2DateTime(float_element)
                                if dateTime.microsecond == 0:
                                    element = dateTime.isoformat()
                                else:
                                    element = dateTime.isoformat() [:-3]
                            except ValueError:
                                element = str(float_element)
                        else:
                            element = str(float_element)
                except ValueError:
                    pass
                # Add clickable link for data
                if indice == url_indice :
                    if element == '':
                        element = '--'
                    else:
                        element = "<a href=\"" + element + "\">" + os.path.basename(element) + "</a>"
                line.append(element)
                indice = indice + 1
            aaData.append(line)
        except Exception, e:
#            print e
            raise e

    data = {
            "aoColumns" : aoColumns,
            "aaData" : aaData
            }

    return data

def getVOTable(request):
    if request.GET.has_key("query_url"):
        url = request.GET["query_url"]
        webFile = urllib2.urlopen(url)
        votable = webFile.read()
        webFile.close()
        return HttpResponse(votable, content_type='application/xml')
        
def download_selection(request):
    """
    Deals with
    /catalog/csv
    Returns CSV-formatted catalog in HttpResponse. 
    Few dirty hacks here to rename column names and use different fields unlike in HTML output; it may be reasonable just create special list 
    with metadata settings Planet.CSV_DISPLAY
    second the new name column was done CSV_DISPLAY to introduce error min and error_max. There is still some cleaning to do. It use the work done for votable
    """
    
    files_info = []
    for data in request.POST:
        if data != 'csrfmiddlewaretoken':
#             access_url_messy = request.POST[data]
#             access_url = access_url_messy.split('\"')[1]
#             name = access_url.split('/')[-1]
#             file_info = {'name': name, 'access_url' : access_url}            
#             files_info.append(file_info)
            access_url = request.POST[data]
            name = access_url.split('/')[-1]
            file_info = {'name': name, 'access_url' : access_url}            
            files_info.append(file_info)
    zip_file_name = 'data.zip'
    f = StringIO.StringIO()
    import zipfile
    with zipfile.ZipFile(f, 'a') as myzip:
        for file_info in files_info:
            webFile = urllib2.urlopen(file_info['access_url'])
            file_data = webFile.read()
            myzip.writestr(file_info['name'], file_data)
            webFile.close()

    response = HttpResponse(f.getvalue(), content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename='+zip_file_name
    return response
    
    return response
        

class DivErrorList(ErrorList):
    def __unicode__(self):
        return self.as_divs()
    def as_divs(self):
        if not self: return u''
        return u'%s' % '<br/>'.join([u'%s' % e for e in self])
    
    

def serviceHasOptionField(currentService, fieldName):
    import registry
    try:
        if currentService.id != None:
            return registry.isColumnInService(currentService.id, fieldName)
    except registry.NoColumnInformationException:
        pass
    tableMetadata = TableMetadataReader.TableMetadata(currentService.access_url, currentService.table)
    return tableMetadata.IsColumnNameInColumns(fieldName)

def getXmlSchemaElements(url, table):    
    url_tables = url + '/tables'
    try:
        webFile = urllib2.urlopen(url_tables)
    except (urllib2.URLError, ValueError):
        raise TableUrlNotFoundException(url_tables)
    s = webFile.read()
    webFile.close()
    root = etree.fromstring(s)
    # TODO A faire proprement
    schemaElements = root.xpath('//schema/table/name[text()="%s"]' % (table))
    if schemaElements ==[]:
        schemaElements = root.xpath('//schema/table/name[text()="epn_core"]')
    if schemaElements ==[]:    
        raise SchemaTableNotFoundException(url_tables, table)
    return schemaElements

    




def getCustomService (customServiceForm):
    import registry
    url = customServiceForm.cleaned_data['url_op']
    schema = customServiceForm.cleaned_data['schema_op']
    if url:
        service = Service(access_url=url, schema=schema)
    else:
        service = registry.getVirtualRegistryServiceBySchema(schema)
        if service == None:
            service = Service(access_url=url, schema=schema)
    return service

class TableUrlNotFoundException(Exception):
    def __init__(self, value):
        self.value = value
    
class SchemaTableNotFoundException(Exception):
    def __init__(self, tableUrl, SchemaName):
        self.tableUrl = tableUrl
        self.SchemaName = SchemaName

def datalink_getParameters(request, **kwargs):
    import xmlReader
    DATALINK_URL = 'datalink_url'
    url = request.GET[DATALINK_URL] + '?'
    params = []
    for key, value in request.GET._iteritems():
        if key != DATALINK_URL:
            params.append(key+ '=' + value)
    url = url + '&'.join(params)
    
    webFile = urllib2.urlopen(url)

    from astropy.io.votable import parse
    votable = parse(webFile)
    webFile.close()
    table = votable.get_first_table()
    data = table.array
    datalinks = []
    for row in table.array:
        datalinks.append({'description':row['description'],'access_url':row['access_url'],'service_def':row['service_def']})
        
    datalinkParameters = xmlReader.getDatalinkParameters(url, '&'.join(params),datalinks)
    json_res = json.dumps(datalinkParameters)
    
    return HttpResponse(json_res, content_type="application/json")

def compil_votable(request, **kwargs):

    from astropy.io.votable import parse, from_table, writeto
    from astropy.io.votable.tree import VOTableFile, Service, Table, Info
    from astropy.table import vstack
    import numpy as np
    import numpy.ma as ma
#     votable_compil = VOTableFile()
#     resource = Resource()
#     votable.resources.append(resource)
# 
#     # ... with one table
#     table = Table(votable)
# 
#     resource.tables.append(table)    
    params = request.GET
        
#     params={"tap_url_0":"http://voparis-tap-planeto.obspm.fr/__system__/tap/run/tap/sync?LANG=ADQL&QUERY=SELECT+TOP+10+%2A+FROM+bdip.epn_core+WHERE+%281+%3D+ivo_hashlist_has%28lower%28%22target_name%22%29%2Clower%28%27Saturn%27%29%29+OR+1+%3D+ivo_hashlist_has%28lower%28%22target_name%22%29%2Clower%28%276%27%29%29%29&REQUEST=doQuery&MAXREC=1000000000","tap_url_1":"http://voparis-tap-planeto.obspm.fr/__system__/tap/run/tap/sync?LANG=ADQL&QUERY=SELECT+TOP+10+%2A+FROM+apis.epn_core+WHERE+%281+%3D+ivo_hashlist_has%28lower%28%22target_name%22%29%2Clower%28%27Callisto%27%29%29+OR+1+%3D+ivo_hashlist_has%28lower%28%22target_name%22%29%2Clower%28%27504%27%29%29+OR+1+%3D+ivo_hashlist_has%28lower%28%22target_name%22%29%2Clower%28%27J-4%27%29%29+OR+1+%3D+ivo_hashlist_has%28lower%28%22target_name%22%29%2Clower%28%27J-IV%27%29%29%29&REQUEST=doQuery&MAXREC=1000000000"}
    query = None
    if params.has_key('query'):
        query = params['query']
    table_list = []
    for key, value in params.iteritems():
        if key.startswith('ivoid_') and value != "ivo://cds.vizier/b/planets":
            # Create a new VOTable file...
            service = registry.getServiceByID(value)
            service.setQuery(query)
            try:
                webFile = urllib2.urlopen(service.url_query_select_top10)
                votable = parse(webFile)
    #             votable.get_first_table().array.mask = ma.nomask
    #             print toto
                table = votable.get_first_table().to_table()
                service_title_column = table.field("service_title")
                table.remove_column("service_title")
                table.add_column(service_title_column,0)
                table.remove_column('s_region')
                table_list.append(table)
            except:
                pass
#             for element in table.array['s_region']:
#                 element = ''
#             toto = table.array['s_region']
            
            
                
#             for resource in votable.resources:
#                 for info in resource.infos:
#                     info.ID = None
#                 for table in votable.iter_tables():
#                     table.ID = None
#                     for field in table.fields:
#                         field.ID = None
#                 if len(table.groups) > 0 :
#                     del table.groups[:]
#                     votable_compil.resources.append(resource)
    # [30:34]
#     toto = table_list[30]
    table_compil = from_table(vstack(table_list, join_type='inner')).get_first_table()
#     table = from_table(vstack(table_list)).get_first_table()
#     
#     
#     from astropy.table import Table, vstack    
#     
#     obs1  = Table([[1, 2, 3], [0.1, 0.2, 0.3]], names=('a', 'b'))
# 
#     obs2 = Table([[1, 2, 3], [0.1, 0.2, 0.3]], names=('a', 'c'))
#     
#     print(vstack([obs1, obs2], join_type='inner'))
#     
#     
#     
#     
#     
#     
#     
#     
#     
#     
#     
#     webFile_0 = urllib2.urlopen(params['tap_url_0'])
#     votable_0 = parse(webFile_0)
#     table_0 = votable_0.get_first_table().to_table()
#     
#     webFile_1 = urllib2.urlopen(params['tap_url_1'])
#     votable_1 = parse(webFile_1)
#     table_1 = votable_1.get_first_table().to_table()
#     
#     
# 
#     
#     res = vstack([table_0, table_1])
    table_compil.description = "Compilation of the first 10 results of each EPN-TAP service, from the VESPA portal (notice that more results may be present and not included here)"
    if query is None:
        query_condition_text = ""
    else:
        query_condition_text = query
    table_compil.infos.append(Info(name="query_conditions",value=query_condition_text))
    votable_compil = VOTableFile()
    service = Service()
#     resource.description = "Compilation of the first 10 results of each EPN-TAP service, from the VESPA portal (notice that more results may be present and not included here)"

    votable_compil.services.append(service)

    service.tables.append(table_compil)    
#     info = {name:}
# INFO name="query" value="SELECT abs_cs.epn_core.granule_uid, abs_cs.epn_core.granule_gid, abs_cs.epn_core.obs_id, abs_cs.epn_core.dataproduct_type, abs_cs.epn_core.measurement_type, abs_cs.epn_core.processing_level, abs_cs.epn_core.target_name, abs_cs.epn_core.target_class, abs_cs.epn_core.time_min, abs_cs.epn_core.time_max, abs_cs.epn_core.time_sampling_step_min, abs_cs.epn_core.time_sampling_step_max, abs_cs.epn_core.time_exp_min, abs_cs.epn_core.time_exp_max, abs_cs.epn_core.spectral_range_min, abs_cs.epn_core.spectral_range_max, abs_cs.epn_core.spectral_sampling_step_min, abs_cs.epn_core.spectral_sampling_step_max, abs_cs.epn_core.spectral_resolution_min, abs_cs.epn_core.spectral_resolution_max, abs_cs.epn_core.c1min, abs_cs.epn_core.c1max, abs_cs.epn_core.c2min, abs_cs.epn_core.c2max, abs_cs.epn_core.c3min, abs_cs.epn_core.c3max, abs_cs.epn_core.c1_resol_min, abs_cs.epn_core.c1_resol_max, abs_cs.epn_core.c2_resol_min, abs_cs.epn_core.c2_resol_max, abs_cs.epn_core.c3_resol_min, abs_cs.epn_core.c3_resol_max, abs_cs.epn_core.spatial_frame_type, abs_cs.epn_core.incidence_min, abs_cs.epn_core.incidence_max, abs_cs.epn_core.emergence_min, abs_cs.epn_core.emergence_max, abs_cs.epn_core.phase_min, abs_cs.epn_core.phase_max, abs_cs.epn_core.instrument_host_name, abs_cs.epn_core.instrument_name, abs_cs.epn_core.service_title, abs_cs.epn_core.release_date, abs_cs.epn_core.creation_date, abs_cs.epn_core.modification_date, abs_cs.epn_core.access_format, abs_cs.epn_core.access_url, abs_cs.epn_core.access_estsize, abs_cs.epn_core.species, abs_cs.epn_core.formula, abs_cs.epn_core.cas_number, abs_cs.epn_core.comments, abs_cs.epn_core.molecule_group, abs_cs.epn_core.bib_references, abs_cs.epn_core.s_region, abs_cs.epn_core.thumbnail_url FROM abs_cs.epn_core LIMIT 20000000"></INFO>
    votable_compil.set_all_tables_format('binary')

    output = StringIO.StringIO()
    votable_compil.to_xml(output)
    
    filename="votable.xml"

    
#     response = HttpResponse(content_type="text/xml;content=x-votable")
#     response["Content-Disposition"] = \
#         'attachment; filename="{}"'.format(filename)
# 
#     astropy.io.votable.writeto(votable, response)
#     return response
#     
    response = HttpResponse(content_type="text/xml;content=x-votable")
    response["Content-Disposition"] = \
        'attachment; filename="{}"'.format(filename)

    writeto(votable_compil, response)
    return response


# def test(request, **kwargs):
#     from epntap.service_parameters import ServiceParameters
#     test = ServiceParameters()