from django import forms
from django.forms.widgets import RadioSelect, CheckboxSelectMultiple, SelectMultiple
from dateutil.parser import parse as dt_parse
from datetime import datetime, date, time, timedelta
from DateConversion import jd2DateTime, isDateTimeValid
import sys

op_sup = ('>=', u'\u2265')
    
op_inf = ('<=', u"\u2264")

op_eq = ('=', '=')

op_like = ('LIKE', 'LIKE')

op_like_contains = ('LIKE', 'like')

op_choices_min = (op_sup, op_inf)
    
op_choices_max = (op_inf, op_sup)


class AsciiCharField(forms.CharField):
    
    def validate(self, value):
        super(AsciiCharField, self).validate(value)
        try:
            value.decode('ascii')
        except :
            raise forms.ValidationError('Use only ASCII characters')

    def to_python(self, value):
        if value:
            value = value.strip()
            return value.replace("'", "''")
        else:
            return value

class AsciiLowerCharField(AsciiCharField):
    def to_python(self, value):
        value = super(AsciiLowerCharField, self).to_python(value)
        if isinstance(value, basestring):
            return value.lower()
        return value

class CustomServiceForm(forms.Form):
        
    prefix = 'f'
    url_op = AsciiCharField(label= "Service Url",max_length=400, required=False, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))
    schema_op = AsciiCharField(label="Schema Name",max_length=50, required=True, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))

class MandatoryParametersForm(forms.Form):
    prefix = 'f'

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('label_suffix', '')
        super(MandatoryParametersForm, self).__init__(*args, **kwargs)

    
    error_css_class = 'error'
    
    REFERENCE_FIELD_ID = "f-target_name"
    target_name = AsciiCharField(label='Target Name', max_length=100, required=False, help_text="IAU name of target ", widget=forms.TextInput(attrs={'class':'resetable'}))
#     target_class = forms.MultipleChoiceField(required=False, label = "Target Class", widget=forms.CheckboxInput(),
#                                              choices=(('asteroid', 'Asteroid'), ('comet', 'Comet'), ('dwarf_planet', 'Dwarf Planet'),
#                                                       ('exoplanet', 'Exoplanet'), ('interplanetary_medium', 'Interplanetary Medium'),
#                                                       ('planet', 'Planet'), ('ring', 'Ring'), ('sample', 'Sample'),
#                                                       ('satellite', 'Satellite'), ('sky', 'Sky'), ('spacecraft', 'Spacecraft'),
#                                                       ('spacejunk', 'Spacejunk'), ('star', 'Star'),('calibration', 'Calibration'))
#                                              )
    target_class = forms.MultipleChoiceField(required=False, label = "Target Class",
                                             choices=(('asteroid', 'Asteroid'),('calibration', 'Calibration'), ('comet', 'Comet'), ('dwarf_planet', 'Dwarf Planet'),
                                                      ('exoplanet', 'Exoplanet'), ('interplanetary_medium', 'Intplanet. Medium'),
                                                      ('planet', 'Planet'), ('ring', 'Ring'), ('sample', 'Sample'),
                                                      ('satellite', 'Satellite'), ('sky', 'Sky'), ('spacecraft', 'Spacecraft'),
                                                      ('spacejunk', 'Spacejunk'), ('star', 'Star'))
                                             )

#     resource_type = forms.ChoiceField(required=False, label='Resource Type', choices=(('granule', 'Granule'), ('dataset', 'Dataset')), initial='granule', widget=forms.Select(attrs={'class' : 'simpleInput'}))

#    index = forms.FloatField(required=False,widget=forms.HiddenInput())
    granule_uid = AsciiCharField(label='Granule UID', max_length=100, required=False, widget=forms.TextInput(attrs={'class':'resetable'}))
    
    granule_gid = AsciiCharField(label='Granule GID', max_length=100, required=False, widget=forms.TextInput(attrs={'class':'resetable'}))

    obs_id = AsciiCharField(label='Obs ID', max_length=100, required=False, widget=forms.TextInput(attrs={'class':'resetable'}))

    dataproduct_type = forms.MultipleChoiceField(label='Dataproduct Type',required=False,
                                       choices=(('ca', 'Catalogue'), ('ci','Catalogue Item'),
                                                ('cu', 'Cube'), ('ds', 'Dynamic Spectrum'),('ev','Event'),('im', 'Image'),
                                                ('ma', 'Map'),('mo', 'Movie'),('pf', 'Photometric Function'),
                                                ('pr', 'Profile'), ('sc', 'Spectral Cube'), ('sp', 'Spectrum'), ('sv', 'Spatial Vector'),
                                                ('ts', 'Time Series'), ('vo', 'Volume')
                                                  ),
                                        widget=SelectMultiple(attrs={'size': 3, 'class' : 'simpleInput'})
                                       )

    measurement_type = AsciiLowerCharField(label='Measurement Type', max_length=100, required=False, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))
    
    processing_level = forms.IntegerField(label='Processing level', required=False, widget=forms.NumberInput(attrs={'class' : 'resetable'}))
#     target_class = forms.MultipleChoiceField(required=False,
#                                              choices=(('asteroid', 'asteroid'), ('comet', 'comet'), ('dwarf_planet', 'dwarf_planet'),
#                                                       ('exoplanet', 'exoplanet'), ('interplanetary_medium', 'interplanetary_medium'),
#                                                       ('planet', 'planet'), ('ring', 'ring'), ('sample', 'sample'),
#                                                       ('satellite', 'satellite'), ('sky', 'sky'), ('spacecraft', 'spacecraft'),
#                                                       ('spacejunk', 'spacejunk'), ('star', 'star'),),
#                                               widget=SelectMultiple(attrs={'size': 6, 'class' : 'simpleInput'})
#                                              )
 
 
     
#     time_search_type_op = forms.ChoiceField(label='Time selection', choices=(('is_included_in', 'Data range is included in'),
#                                                      ('includes', 'Data range includes')), initial='Data range is included in', required=False)
    time_search_type_op = forms.ChoiceField(label='Time selection', choices=(('is_included_in', 'Data range is included in the range'),
                                                     ('includes', 'Data range intersects the range')), initial='Data range is included in the range', required=False)
#     time_interval_type_op = forms.ChoiceField(label='', choices=(('between', 'The range between'), ('around', 'The range around')), initial='the range between', required=False)
    time_interval_type_op = forms.ChoiceField(label='', choices=(('between', 'Defined by min/max values'), ('around', 'Defined by center/width values')), initial='Defined by min/max values', required=False)
    time_min = forms.CharField(label='Time Min', max_length=23, required=False, widget=forms.TextInput(attrs={'class' : 'resetable'}))
    time_max = forms.CharField(label='Time Max', max_length=23, required=False, widget=forms.TextInput(attrs={'class' : 'resetable'}))
    time_center = forms.CharField(label='Time', max_length=23, required=False, widget=forms.TextInput(attrs={'class' : 'resetable'}))
    time_delta_value_op = forms.FloatField(label='Delta', required=False, widget=forms.NumberInput(attrs={'class' : 'resetable'}))
    time_delta_unit_op = forms.ChoiceField(choices=(('days', 'days'), ('hours', 'hours'),
                                                    ('minutes', 'minutes'), ('seconds', 'seconds'),
                                                    ('milliseconds', 'milliseconds')),
                                                    initial='days', required=False)

    spatial_frame_type = forms.ChoiceField(label="Spatial Frame Type", required=False,
                                           choices=(('', 'All'),('body', 'Body'), ('cartesian', 'Cartesian'),
                                                    ('celestial', 'Celestial'),
                                                    ('cylindrical', 'Cylindrical'),
                                                    ('spherical', 'Spherical')),
                                            initial='all',
                                            widget=forms.Select(attrs={'class' : 'simpleInput'})
                                           )

#     location_interval_op = forms.ChoiceField(label ='', choices=(('intersection', 'Data range intersects'),('inclusion', 'Data range is included in')), initial='Data range intersects', required=False)
    location_interval_op = forms.ChoiceField(label ='', choices=(('intersection', 'Data range intersects the range'),('inclusion', 'Data range is included in the range')), initial='Data range intersects the range', required=False)    
#     location_values_op = forms.ChoiceField(label='', choices=(('between', 'The range between'), ('around', 'The range around')), initial='the range between', required=False)
    location_values_op = forms.ChoiceField(label='', choices=(('between', 'defined by min/max values'), ('around', 'defined by center/width values')), initial='defined by min/max values', required=False)
    
    c1min = forms.FloatField(label='c1 Min', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c1max = forms.FloatField(label='c1 Max', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c1center = forms.FloatField(label='c1',required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c1delta = forms.FloatField(label='delta', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))

    c2min = forms.FloatField(label='c2 Min', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c2max = forms.FloatField(label='c2 Max', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c2center = forms.FloatField(label='c2',required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c2delta = forms.FloatField(label='delta', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))

    c3min = forms.FloatField(label='c3 Min', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c3max = forms.FloatField(label='c3 Max', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c3center = forms.FloatField(label='c3',required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c3delta = forms.FloatField(label='delta', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
 
    c1_resol_min = forms.FloatField(label="c1 Resolution Min",required=False, widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c1_resol_max = forms.FloatField(label="c1 Resolution Max",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c1_resol_center = forms.FloatField(label='c1 Resolution',required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c1_resol_delta = forms.FloatField(label='delta', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))

    c2_resol_min = forms.FloatField(label="c2 Resolution Min",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c2_resol_max = forms.FloatField(label="c2 Resolution Max",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c2_resol_center = forms.FloatField(label='c2 Resolution',required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c2_resol_delta = forms.FloatField(label='delta', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    
    c3_resol_min = forms.FloatField(label="c3 Resolution Min",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c3_resol_max = forms.FloatField(label="c3 Resolution Max",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c3_resol_center = forms.FloatField(label='c3 Resolution',required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    c3_resol_delta = forms.FloatField(label='delta', required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp c_value resetable'}))
    
    spectral_interval_op = forms.ChoiceField(label ='Data range', choices=(
                                                                           ('intersection', 'Data range intersects'),
                                                                           ('inclusion', 'Data range is included in'),
                                                                           ('includes', 'Data range includes')),
                                             initial='Data range intersects', required=False)
    spectral_unit_op = forms.ChoiceField(required=False,label='Unit', choices=(('hertz', 'Hz'),('micro_meter', u'\u00B5'+'m'),('per_centimeter', 'cm'+u'\u207B\u00B9')), widget=forms.Select(attrs={'class' : 'operator resetable'}))
    spectral_resolution_min = forms.FloatField(required=False, label="Spectral Resolution Min", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    spectral_resolution_max = forms.FloatField(required=False, label="Spectral Resolution Max", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    spectral_sampling_step_min = forms.FloatField(required=False, label="Spectral Sampling Step Min", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    spectral_sampling_step_max = forms.FloatField(required=False, label="Spectral Sampling Step Max", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    spectral_range_min = forms.FloatField(required=False, label="Spectral Range Min", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    spectral_range_max = forms.FloatField(required=False, label="Spectral Range Max", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))

    time_exp_min = forms.FloatField(required=False, label="Exposure Time Min", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    time_exp_min_op = forms.ChoiceField(required=False,choices=op_choices_min, widget=forms.Select(attrs={'class' : 'operator'}))
    time_exp_max = forms.FloatField(required=False, label="Exposure Time Max", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    time_exp_max_op = forms.ChoiceField(required=False,choices=op_choices_max, widget=forms.Select(attrs={'class' : 'operator'}))
    time_sampling_step_min = forms.FloatField(required=False, label="Time Sampling Step Min", widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    time_sampling_step_min_op = forms.ChoiceField(required=False,choices=op_choices_min, widget=forms.Select(attrs={'class' : 'operator'}))
    time_sampling_step_max = forms.FloatField(required=False, label="Time Sampling Step Max",widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    time_sampling_step_max_op = forms.ChoiceField(required=False, choices=op_choices_max, widget=forms.Select(attrs={'class' : 'operator'}))
    phase_min = forms.FloatField(label="Phase Min",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    phase_min_op = forms.ChoiceField(required=False,choices=op_choices_min, widget=forms.Select(attrs={'class' : 'operator'}))
    phase_max = forms.FloatField(label="Phase Max",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    phase_max_op = forms.ChoiceField(required=False,choices=op_choices_max, widget=forms.Select(attrs={'class' : 'operator'}))    
    incidence_min = forms.FloatField(label="Incidence Min",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    incidence_min_op = forms.ChoiceField(required=False,choices=op_choices_min, widget=forms.Select(attrs={'class' : 'operator'}))
    incidence_max = forms.FloatField(label="Incidence Max",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    incidence_max_op = forms.ChoiceField(required=False,choices=op_choices_max, widget=forms.Select(attrs={'class' : 'operator'}))
    emergence_min = forms.FloatField(label="Emergence Min",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    emergence_min_op = forms.ChoiceField(required=False,choices=op_choices_min, widget=forms.Select(attrs={'class' : 'operator'}))
    emergence_max = forms.FloatField(label="Emergence Max",required=False,widget=forms.TextInput(attrs={'class' : 'inputWithOp resetable'}))
    emergence_max_op = forms.ChoiceField(required=False,choices=op_choices_max, widget=forms.Select(attrs={'class' : 'operator'}))
     
    instrument_host_name = AsciiCharField(label="Instrument Host Name",max_length=100, required=False, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))
#     instrument_host_name_op = forms.ChoiceField(required=False,choices=(op_eq, op_like_contains),  widget=forms.Select(attrs={'class' : 'operator'}))
    
    instrument_name = AsciiCharField(label="Instrument Name",max_length=100, required=False, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))
    instrument_name_op = forms.ChoiceField(required=False,choices=(op_eq, op_like_contains),  widget=forms.Select(attrs={'class' : 'operator'}))

    species = AsciiCharField(max_length=100, required=False, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))
    feature_name = AsciiLowerCharField(label="Feature Name",max_length=100, required=False, widget=forms.TextInput(attrs={'class' : 'simpleInput resetable'}))

     
    def validateTime(self,time):
        try:
            time_float = float(time)
            if time_float > sys.maxint:
                raise forms.ValidationError("Time numeric value must be < %d" % sys.maxint)
            try:
                time_iso = jd2DateTime(time_float)
            except ValueError, msg:
                raise forms.ValidationError(msg)
            
        except ValueError:
            try:
                time_iso = dt_parse(time)
            except ValueError:
                raise forms.ValidationError("Invalid date format")
        try :
            isDateTimeValid(time_iso)
        except ValueError, msg : 
            raise forms.ValidationError(msg)
        return time_iso
    
    def clean_time_min(self):
        if self.cleaned_data['time_interval_type_op'] == 'between' :
            if self.cleaned_data['time_min']:
                time_min = self.cleaned_data['time_min']
                return self.validateTime(time_min)
        return None
        
    def clean_time_max(self):
        if self.cleaned_data['time_interval_type_op'] == 'between' :
            if self.cleaned_data['time_max']:
                time_max = self.cleaned_data['time_max']
                return self.validateTime(time_max)
        return None
             
    def clean_time_center(self):
        if self.cleaned_data['time_interval_type_op'] == 'around' :
            if self.cleaned_data['time_center']:
                time_center = self.cleaned_data['time_center']
                return self.validateTime(time_center)
        return None
    
    COORDINATE_VALUE_FIELDS = (
    'c1min',
    'c1max',
    'c2min',
    'c2max',
    'c3min',
    'c3max'
    )
    
    COORDINATE_FIELDS = COORDINATE_VALUE_FIELDS +(
    'location_interval_op',
    'location_values_op',
    'c1center',
    'c1delta',
    'c2center',
    'c2delta',
    'c3center',
    'c3delta')
    
    SPECTRAL_FIELDS = (
    'spectral_resolution_min',
    'spectral_resolution_max',
    'spectral_sampling_step_min',
    'spectral_sampling_step_max',
    'spectral_range_min',
    'spectral_range_max'
                       )
    COEFF_MICROMETER_TO_HERTZ = 2.99792458E14
    
    COEFF_PERCENTIMETER_TO_MICROMETER = COEFF_MICROMETER_TO_HERTZ/10000

    SPECIAL_FIELDS = COORDINATE_FIELDS + SPECTRAL_FIELDS
        
class AdvancedParametersForm(forms.Form):        
    prefix = 'f'
    op_choices = (op_eq, op_sup, op_inf, op_like)
    o = forms.ChoiceField(required=False,choices=op_choices, widget=forms.Select(attrs={'class' : 'allparameters_operator ym-fbox-select'}),label='')
    v = AsciiCharField(max_length=100, required=False,widget=forms.TextInput(attrs={'class' : 'allparameters_input'}),label='')
    l = AsciiCharField(max_length=100, required=False,widget=forms.HiddenInput())
    n = AsciiCharField(max_length=100, required=False,widget=forms.HiddenInput())


class TextForm(forms.Form):
    prefix = 'f'
    text_query = forms.CharField(required=False,widget=forms.Textarea(attrs={"cols":None,'class':'resetable'}), label='')
#     text_query = forms.CharField(required=False,widget=forms.Textarea(), label='')
SENSITIVE_STRING_COMPARE = (
    'species'
    )

HASHLISTPARAMETERS = (
                      'alt_target_name',
                      'target_class',
                      'instrument_host_name',
                      'instrument_name',
                      'measurement_type',
                      'bib_reference',
                      'processing_level',
                      'species'
                      )