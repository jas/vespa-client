# MAPPING_KEYWORDS_PDS2EPNTAP:
# {   'instrument_name' : 
#     
#     
#     }
# MAPPING_TARGETCLASS_EPNTAP2PDS = {
#     'asteroid' : 'ASTEROID OR TRANS-NEPTUNIAN OBJ',
#     'comet' : 'COMET',
#     'dwarf_planet': 'ASTEROID',
#     'exoplanet' : 'PLANETARY_SYSTEM OR PLANETARY SYSTEM',
#     'interplanetary_medium' : 'DUST',
#     'planet' : 'planet',
#     'ring' : 'ring',
#     'sample' : 'TERRESTRIAL SAMPLE OR METEORITE',
#     'satellite' : 'SATELLITE',
#     'sky' : 'GALAXY OR GLOBULAR CLUSTER OR DUST OR NEBULA OR OPEN CLUSTER OR PLANETARY NEBULA OR REFERENCE OR STAR OR STAR CLUSTER',
#     'star' : if form.cleaned_data[].has_key('target_name') and form.cleaned_data['target_name'].lowercase()=='sun' : 'SUN' else: 'STAR OR STAR CLUSTER',
#     'calibration' : 'CALIBRATION'
#     
#     }
# target_class (asteroid -> asteroid or trans_neptunian_obj, comet = comet or  dwarf planet -> asteroid, exoplanet -> planetary system, interplanetary_medium -> dust, Planet -> Planet, ring -> ring,
# Sample -> terrostrial_sample or meteorite, Satellite -> Satellite, Sky -> Star, sky : galaxy, globular cluster, dust, nebula, open                     cluster, planetary nebulla, refernece ? , star, star cluster, spacecraft -> None, Spacejunk -> None, si star et sun dans target-name -> Sun, sinon star OR star_cluster,                     Calibration -> Calibration



def getTargetClassEPNTAP2PDS (target_class_list,cleaned_data):
    conditions = []
    for target_class in target_class_list:
        if target_class == 'asteroid':
            conditions.append('target-type:ASTEROID OR target-type:TRANS-NEPTUNIAN OBJ')
        elif target_class == 'comet':
            conditions.append('target-type:COMET')
        elif target_class == 'dwarf_planet':
            conditions.append('target-type:ASTEROID')
        elif target_class == 'exoplanet':
            conditions.append('target-type:PLANETARY_SYSTEM OR target-type:PLANETARY SYSTEM')
        elif target_class == 'interplanetary_medium':
            conditions.append('target-type:DUST')
        elif target_class == 'planet':
            conditions.append('target-type:PLANET')
        elif target_class == 'ring':
            conditions.append('target-type:RING')
        elif target_class == 'sample':
            conditions.append('target-type:TERRESTRIAL SAMPLE OR target-type:METEORITE')
        elif target_class == 'satellite':
            conditions.append('target-type:SATELLITE')
        elif target_class == 'sky':
            conditions.append('target-type:GALAXY OR target-type:GLOBULAR CLUSTER OR target-type:DUST OR target-type:NEBULA OR target-type:OPEN CLUSTER OR target-type:PLANETARY NEBULA OR target-type:REFERENCE OR target-type:STAR OR target-type:STAR CLUSTER')
        elif target_class == 'star':
            if cleaned_data.has_key('target_name') and cleaned_data['target_name'].lower()=='sun':
                conditions.append('target-type:SUN')
            else:
                conditions.append('target-type:STAR OR target-type:STAR CLUSTER')
        elif target_class == 'target-type:calibration':
            conditions.append('target-type:CALIBRATION')
    return "(" + " OR ".join(conditions) + ")"
    
def createTimeDelta(delta, unit):
    import datetime
    return {
        'days': datetime.timedelta(days=delta),
        'hours': datetime.timedelta(hours=delta),
        'minutes': datetime.timedelta(minutes=delta),
        'seconds': datetime.timedelta(seconds=delta),
        'milliseconds': datetime.timedelta(milliseconds=delta),
    }[unit]

    
def getTime(cleaned_data):
    import datetime
    timeQuery = None
    time_min = None
    time_max = None
    if cleaned_data['time_interval_type_op'] == 'around':
        if cleaned_data['time_center']:
            time = cleaned_data['time_center']
            if (cleaned_data['time_delta_value_op']):
                unit = cleaned_data['time_delta_unit_op']
                delta = cleaned_data['time_delta_value_op']
                delta_iso = createTimeDelta(delta, unit)
            else :
                delta_iso = datetime.timedelta(days=0)
            time_min = time - delta_iso
            time_max = time + delta_iso
    else:
        if cleaned_data["time_min"]:
            time_min = cleaned_data["time_min"]
        if cleaned_data["time_max"]:
            time_max = cleaned_data['time_max']
    if time_min != None or time_max != None:
        if time_max == None:
            timeQuery = 'stop-time:['+time_min.isoformat()+'Z'+' TO '+ datetime.datetime.max.isoformat()+'Z'+']'
        elif time_min == None:
            timeQuery = 'start-time:['+datetime.datetime.min.isoformat()+'Z'+' TO '+time_max.isoformat() +'Z'+']'
        else:
            timeQuery = '(start-time:['+datetime.datetime.min.isoformat()+'Z'+' TO '+time_max.isoformat() +'Z'+'] AND stop-time:['+time_min.isoformat()+'Z'+' TO '+ datetime.datetime.max.isoformat()+'Z'+'])'
#             timeQuery = '(start-time:'+time_min.isoformat()+'Z'+' AND stop-time:'+time_max.isoformat()+'Z'+')'
#             if time_min != None and time_max != None:
#                 timeQuery = "(start-time:["+time_min.isoformat()+" to "+time_max.isoformat()+"] AND stop-time:["+time_min.isoformat()+" to "+time_max.isoformat()+"])"
    return timeQuery
            
            
#             if cleaned_data["time_min"] or cleaned_data["time_max"]:
#         conditions.append("start-time:["+time_min.isoformat()+" to "+time_max.isoformat()+"] AND stop-time:["+time_min.isoformat()+" to "+time_max.isoformat()+"]")
#     else :
#         if (f == 'time_min'):
#             time_min = time
#             if cleaned_data["time_max"]:
#                 time_max = time_max
#             else:
                
            
            
            
            
#             if ((not cleaned_data.has_key("time_max") or (cleaned_data.has_key("time_max") and not cleaned_data["time_max"]))):
#                 conditions.append("stop-time:["+time_min.isoformat()+" to "+datetime.now().isoformat()+"]")
#         else:
#             time_max = cleaned_data["time_max"]
#             conditions.append("start-time:["+time_min.isoformat()+" to "+time_max.isoformat()+"] AND stop-time:["+time_min.isoformat()+" to "+time_max.isoformat()+"]")
#         if f == 'time_max' and (not cleaned_data.has_key("time_min") or (cleaned_data.has_key("time_min") and not cleaned_data["time_min"])):
#             pass

    

def getPdsQuery(form):
    import urllib
    BASE_URL = "https://pds.nasa.gov/services/search/search?"
    queryDict = {}
    conditions = []
    query = None
    if form != None and form.is_bound :
        for f in form.cleaned_data:
            if form.cleaned_data[f] :
                if f == 'instrument_name':
                    conditions.append('(instrument:'+form.cleaned_data[f]+' OR observing-system:'+form.cleaned_data[f]+')')
                elif f == 'instrument_host_name':
                    conditions.append('(instrument-host:'+form.cleaned_data[f]+' OR investigation:'+form.cleaned_data[f]+')')
                elif f == 'target_name':
                    conditions.append('target:'+form.cleaned_data[f])
#                 elif f == 'target_class':
#                     conditions.append('instrument_type:'+ form.cleaned_data[f])
                elif f == 'file_name':
                     conditions.append('title:'+ form.cleaned_data[f])
                elif f == 'target_class':
                    conditions.append(getTargetClassEPNTAP2PDS(form.cleaned_data[f], form.cleaned_data))
#                 elif f == 'time_min':
#                     conditions.append('start-time:' + form.cleaned_data[f])
#                 elif f == 'time_max':
#                     conditions.append('stop-time:' + form.cleaned_data[f])
        timeQuery = getTime(form.cleaned_data)
        if timeQuery != None:
            conditions.append(getTime(form.cleaned_data))
#                 elif f in ('time_min', 'time_max', 'time_center') :
#                     conditions.append(getTime(f,form.cleaned_data))
        query = " AND ".join(conditions)
        url = BASE_URL + urllib.urlencode({"q":query})
    else:
        url = BASE_URL
    return url, query
