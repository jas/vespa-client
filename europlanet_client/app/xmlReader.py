import urllib2
from lxml import etree     

def readVoServices(urlVoservices):
    webFile = urllib2.urlopen(urlVoservices)
    s = webFile.read()
    webFile.close()
    root = etree.fromstring(s)
    
    schemaElements = root.xpath('//tableset/schema/name')
    if len(schemaElements) == 1:
        schema = schemaElements[0].text
    else :
        raise Exception
    tableElements = root.xpath('//tableset/schema/table/name')
    if len(tableElements) == 1:
        table = tableElements[0].text
    else :
        raise Exception
    return schema, table

def getServiceDatalink(url_base, url_metadata):
    inputParams = {}
    standardID = None
    accessURL = None
    try:
        webFile = urllib2.urlopen(url_base, url_metadata)
        s = webFile.read()
        webFile.close()
        root = etree.fromstring(s)
#        test = root.xpath('xmlns')
#         ns = root.attrib['None']
        namespaces = {}
        for key, value in root.nsmap.iteritems():
            if key == None:
                namespaces['ns'] = value
        datalinkServiceElements = root.xpath('//ns:VOTABLE/ns:RESOURCE[@type="meta"][@utype="adhoc:service"]',   namespaces=namespaces)
        if len(datalinkServiceElements) != 1:
            raise Exception
        datalinkService = datalinkServiceElements[0]
        inputParamsElements = datalinkService.xpath('//ns:GROUP[@name="inputParams"]/ns:PARAM',   namespaces=namespaces)
        for inputParam in inputParamsElements:
            inputParams[inputParam.attrib['name']] = inputParam.attrib['ref']
        standardIDElements = datalinkService.xpath('//ns:PARAM[@name="standardID"]',   namespaces=namespaces)
        if len(standardIDElements) != 1 :
            raise Exception
        standardID = standardIDElements[0].attrib['value']
        accessURLElements = datalinkService.xpath('//ns:PARAM[@name="accessURL"]',   namespaces=namespaces)
        if len(accessURLElements) != 1:
            raise Exception
        accessURL = accessURLElements[0].attrib['value']
            
#        schemaElements = root.xpath('//ns:VOTABLE/ns:RESOURCE/type[text()="meta"]utype[text()="adhoc:service"]',   namespaces=namespaces)
        # TODDO : Change this hack when going to astropyv3 (need python 3)
#         name[text()="epn_core"]name="standardID"
    except urllib2.HTTPError, error:
         sys.stderr.write("Error reading VOTable : " + url_base + url_metadata)
         raise Exception
    return { 
        'inputParams' : inputParams,
        'standardID' : standardID,
        'accessURL' : accessURL
        }
def getDatalinkParameters(url_base, url_metadata, datalinks):
    
    standardID = None
    accessURL = None
    try:
        webFile = urllib2.urlopen(url_base, url_metadata)
        s = webFile.read()
        webFile.close()
        root = etree.fromstring(s)
#        test = root.xpath('xmlns')
#         ns = root.attrib['None']
        namespaces = {}
        for key, value in root.nsmap.iteritems():
            if key == None:
                namespaces['ns'] = value
                
        for datalink in datalinks:
            if datalink['service_def'] != "":
                
                datalinkServiceElements = root.xpath('//ns:VOTABLE/ns:RESOURCE[@type="meta"][@utype="adhoc:service"]',   namespaces=namespaces)
                if len(datalinkServiceElements) != 1:
                    raise Exception
                datalinkService = datalinkServiceElements[0]
                inputParamsElements = datalinkService.xpath('//ns:GROUP[@name="inputParams"]/ns:PARAM',   namespaces=namespaces)
                inputParams = []
                for inputParam in inputParamsElements:
                    inputParam_description = inputParam.xpath('ns:DESCRIPTION', namespaces=namespaces)
                    if len(inputParam_description) > 1:
                        raise Exception
                    elif len(inputParam_description) == 1:
                        description = inputParam_description[0].text
                    else:
                        description = None
                    inputParams.append({'name':inputParam.attrib['name'],'value':inputParam.attrib['value'], 'description':description})
                datalink['inputParams'] = inputParams
#                 standardIDElements = datalinkResource.xpath('//ns:PARAM[@name="standardID"]',   namespaces=namespaces)
#                 if len(standardIDElements) != 1 :
#                     raise Exception
#                 standardID = standardIDElements[0].attrib['value']
                accessURLElements = datalinkService.xpath('//ns:PARAM[@name="accessURL"]',   namespaces=namespaces)
                if len(accessURLElements) != 1:
                    raise Exception
                datalink['access_url'] = accessURLElements[0].attrib['value']
            
#        schemaElements = root.xpath('//ns:VOTABLE/ns:RESOURCE/type[text()="meta"]utype[text()="adhoc:service"]',   namespaces=namespaces)
        # TODDO : Change this hack when going to astropyv3 (need python 3)
#         name[text()="epn_core"]name="standardID"
    except urllib2.HTTPError, error:
         sys.stderr.write("Error reading VOTable : " + url_base + url_metadata)
         raise Exception
    return datalinks
    
    
    