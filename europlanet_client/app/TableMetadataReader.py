import urllib2
from lxml import etree


class TableMetadata():
    def  __init__(self, url, table):        
        url_tables = url + '/tables'
        try:
            webFile = urllib2.urlopen(url_tables, timeout=10)
        except (urllib2.URLError, ValueError):
            raise TableUrlNotFoundException(url_tables)
        s = webFile.read()
        webFile.close()
        root = etree.fromstring(s)
        # TODO A faire proprement
        tableElements = root.xpath('//schema/table/name[text()="%s"]' % (table))
        if tableElements == []:
            tableElements = root.xpath('//schema/table/name[text()="epn_core"]')
        if tableElements == []:    
            raise SchemaTableNotFoundException(url_tables, table)
        self.schemaElement = tableElements[0].getparent()
        self.tableElements = tableElements
#        print self.schemaElement.getparent()
        
    def json(self):
        res = {}
        res['columns']=[]
        for schmemaSubElement in self.schemaElement:
            if len(schmemaSubElement):
                column = {}
                for columnElement in schmemaSubElement:
                    if len(columnElement.attrib):
                        column[columnElement.tag] = {}
                        for key, value in columnElement.attrib.iteritems():
                            column[columnElement.tag][key] = value
                        column[columnElement.tag]['text'] = columnElement.text
                    else:
                        column[columnElement.tag]= columnElement.text
                res['columns'].append(column)
#                     for columnSubElement in columnElement:
#                         if len(columnSubElement.attrib):
#                             column[columnSubElement.tag] = {}
#                             print 'toto'
                                
#                        column[columnSubElement]
            else:
#             if element.tag == 'column':
#                 columnElement = element.getchildren()
#                 
#         for attrib in self.schemaElement.findall():
                res [schmemaSubElement.tag] = schmemaSubElement.text
 #               print element.tag 
        return res
#         schema_name = self.schemaElement.xpath('name')[0].text
#         print schema_name
    def getColumnNameElements(self,columnName):
        columnElements = self.schemaElement.xpath('./column/name[text()="%s"]' % (columnName))
        return columnElements

    def getFieldType(self,columnName):
        columnNameElement = self.getColumnNameElements(columnName)
        if len(columnNameElement) == 0:
            raise Exception
        fieldTypeElement = columnNameElement[0].xpath('../dataType')[0]
        return fieldTypeElement.text
    
    def IsColumnNameInColumns(self,columnName):
        columnElements = self.schemaElement.xpath('./column/name[text()="%s"]' % (columnName))
        if len(columnElements) != 0:
            return True
        else :
            return False
        
    def getColumns(self):
        COLUMNS = ['name','unit','ucd','description','datatype']
        columnElements = self.schemaElement.xpath('./column')
        columns = []
        for columnElement in columnElements:
            name = columnElement.xpath('name')[0].text
            unit = None
            unitElements = columnElement.xpath('unit')
            if len(unitElements) == 1:
                unit = unitElements[0].text
            ucd = None
            ucdElements = columnElement.xpath('ucd')
            if len(ucdElements) == 1:
                ucd = ucdElements[0].text
            description = None
            descriptionElements = columnElement.xpath('description')
            if len(descriptionElements) == 1:
                description = descriptionElements[0].text
            datatype = columnElement.xpath('dataType')[0].text
            columns.append({'name':name,'unit':unit,'ucd':ucd,'description':description,'datatype':datatype})
        return columns
            
class TableUrlNotFoundException(Exception):
    def __init__(self, value):
        self.value = value
    
class SchemaTableNotFoundException(Exception):
    def __init__(self, tableUrl, SchemaName):
        self.tableUrl = tableUrl
        self.SchemaName = SchemaName
