import StringIO
import json
import urllib, urllib2
from astropy.io.votable import parse
from lxml import etree
import time
ALL_REC = 1000000000
TIMEOUT = 60

def run(tap_url, query, format='votable'):
    import urllib, urllib2
    sync_tap_url = tap_url + "/sync?"
    query_encoded= urllib.urlencode({'REQUEST':'doQuery', 'LANG': 'ADQL', 'QUERY': query,'FORMAT':format, 'MAXREC' :str(ALL_REC)})
    webFile = urllib2.urlopen(sync_tap_url, query_encoded, timeout=60)
    if format == "votable":
        result = getVOTable(webFile)
    elif format == "json":
        result = getJson(webFile)
    else:
        webFile.close()
        raise Exception("Undefined format")
    webFile.close()
    return result
#     table = parse_single_table(webFile)
#     webFile.close()
#     return table.array

def getJsonError(error):
    votable = parse(StringIO.StringIO(error.read()))
    data_error = {'error_msg' : votable.resources[0].infos[0].content}
    from django.core.serializers.json import DjangoJSONEncoder    
    jsonDataTable = json.dumps(data_error, cls=DjangoJSONEncoder)
    return jsonDataTable
    

def getJson(webFile):
    import json
    result = json.loads(webFile.read())
    return result['data']
    
def getVOTable(webfile):
    votable = parse(webfile)
    table = votable.get_first_table()
    return table
    
def count(tap_url,query):
    table = run(tap_url, query)
    return table.array[0][0]

# def getColumns(tap_url,table):
    # COLUMN_NAMES = ['name','unit','ucd','column_description','datatype']
    # query = "SELECT " + " ,".join(COLUMN_NAMES) + " FROM TAP_SCHEMA.columns WHERE table_name='" + table + "'"
    # res= run(tap_url, query, format='json')
    # return res


def get_job_phase(job_url):
    webFile = urllib2.urlopen(job_url, timeout=TIMEOUT)
    s = webFile.read()
    webFile.close()
    root = etree.fromstring(s)
    namespaces = {}
    for key, value in root.nsmap.iteritems():
        namespaces[key] = value
    NS = {"uws":"http://www.ivoa.net/xml/UWS/v1.0"}
    return root.xpath('//uws:job/uws:phase', namespaces=NS)[0].text
    

def delete_job(job_url):
    delete_job_param = "ACTION=DELETE"
    webFile = urllib2.urlopen(job_url, delete_job_param, timeout=TIMEOUT)
    webFile.close()        
    

# def run(tap_url, query, format='votable'):
    # MAXREC = 1000
    # ASYNC_URL_SUFFIX = "async"
    # PARAMETERS_SUFFIX = "parameters"
    # PHASE_SUFFIX = "phase"
    #
    #
    # create_job_url = tap_url + "/" + ASYNC_URL_SUFFIX
    # service_params = urllib.urlencode({'REQUEST':'doQuery', 'LANG': 'ADQL', 'QUERY': query, 'format':format, 'PHASE':"RUN"})
    # webFile = urllib2.urlopen(create_job_url, service_params, timeout=TIMEOUT)
    # job_url = webFile.url
    #
    # if get_job_phase(job_url) == "PENDING":
        # run_url = job_url + '/phase'
        # service_params = 'PHASE=RUN'
        # urllib2.urlopen(run_url, service_params, timeout=TIMEOUT)
        #
    # i = 0
    # success = False
    # while (i < 300):
        # phase = get_job_phase(job_url)
        # if phase == "COMPLETED":
            # success = True
            # break
        # elif phase != "EXECUTING" and phase != "QUEUED":
            # success = False
            # break
        # i = i + 1
        # time.sleep(1)
    # if not success :
        # delete_job(job_url)
        # raise Exception("JOB NOT COMPLETED")
        #
    # result_url = job_url + "/results/result"
    # webFile = urllib2.urlopen(result_url, timeout=TIMEOUT)
    #
    # if format == "votable":
        # result = getVOTable(webFile)
    # elif format == "json":
        # result = getJson(webFile)
    # else:
        # webFile.close()
        # delete_job(job_url)
        # raise Exception("Undefined format")    
    # delete_job(job_url)
    # return result
    #
