#!/usr/bin/env Python
# -*- coding: iso-8859-15 -*-

# import datetime
# import sys
# import os
# import os.path
# import json
from geojson import Feature, Point, FeatureCollection, Polygon
# from astropy.io.votable import parse_single_table
#from astropy.io.vo.table import parse_single_table
import atpy
# import pandas as pd
import numpy as np
import StringIO
# import cgi
# import numpy.ma as ma

def run(votable_file):

    my_point = []
    toto = []
    ind = 0
    val = {}
    
#     fichier_json = "vvex_poly.json"
#     file = open(fichier_json, 'w')
    
    # fuction to limit value of latitude and longitude
    # in case 9999 is used as error value
    def limit_latitude(latitude):
        if latitude < -90.0:
            latitude = -90.0
        if latitude > 90.0:
            latitude = 90.0
        return latitude
    
    def limit_longitude(longitude):
        if longitude < -180.0:
            longitude = -180.0
        if longitude > 360.0:
            longitude = 360.0
        return longitude
    
    # LECTURE VOTABLE
    table = atpy.Table(votable_file, type='vo')
    # my_fields=table.get_fields()
    data = table.data
#     meta = table.fields
#     # print data
#     print data.size
#     # print 'titi', ma.compressed(data[5])
#     # entete pour la construction du tableau de sortie
    ind = len(table.columns)
    #    print colname
    #    print colname, unit, description, type
    # http://pydoc.net/Python/ATpy/0.9.7/atpy.votable/
    
    #nulle=np.zeros((data.size,ind))
    nulle = np.zeros((ind))
    
    # moyen pas clean d extraire les colonnes NULL de la votable
    for i in range(data.size):
        for j in range(ind):
    #        if pd.isnull(str(data[i][j])):
            if str(data[i][j]) == '--':
                nulle[j] += 1
               # print 'toto',i,j,data[i][j]
               # continue

#     column_map = {}
#     for indice,columnKey in enumerate(table.columns.keys):
#         column_map[columnKey] = indice
#     
    for i in range(data.size):
        is_alt_min = False
        is_alt_max = False
        if i == i:
            val = {}
            for j in range(ind):
                if nulle[j] == 0:

                    field_name = table.data[i].dtype.names[j]
                    # je fais une inversion c1 et c2 a cause d'une erreur de titan
                    if field_name.lower() == "c1min":
                        longitude_min = round(data[i][j], 4)
    #                    print 'longitude',longitude
                    elif field_name.lower() == "c2min":
                        latitude_min = round(data[i][j], 4)
                    elif field_name.lower() == "c1max":
                        longitude_max = round(data[i][j], 4)
                    elif field_name.lower() == "c2max":
                        latitude_max = round(data[i][j], 4)
                    elif field_name.lower() == "c3min":
                        is_alt_min = True
                        altitude_min = round(data[i][j], 4) * 1000
                    elif field_name.lower() == "c3max":
                        is_alt_max = True
                        altitude_max = round(data[i][j], 4) * 1000
                        
                    elif field_name.lower() == "granule_uid":
                        index = str(data[i][j])
    #                    print index
                    elif field_name.lower() == "access_url":
                       # print data[i][j]
                        accessurl = data[i][j]
                    elif field_name.lower() == "spatial_frame_type":
                       # print data[i][j]
                        spacial_frametype = data[i][j]
    
    
                    val[str(field_name)] = str(data[i][j])
            val["identifier"] = str(index)
    #                    val += '"'+table.fields[j].ID+ '"' +':'+'"'+str(data[i][j])+'"'+', '
    #                val += table.fields[j].ID +':'+'"'+str(data[i][j])+'"'+', '
    #                val += table.fields[j].ID +':'+str(data[i][j])+', '
    #               print "toto", table.fields[j].ID.lower()
    #               poly = Polygon( ((0, 0), (0, 1), (1, 1), (0, 0)) )
    #
            # As body in spatial frame type we start conversion
            if 'body' in spacial_frametype:
                if is_alt_min and is_alt_max :
                    mid_altitude = (altitude_max + altitude_min)/2
                else:
                    mid_altitude = 0
                if latitude_min == latitude_max and longitude_min == longitude_max:
                    my_point = Point((longitude_min, latitude_min, mid_altitude))
                    my_feature = Feature(id=index, geometry=my_point, properties=val)
    #        val=val[:-2]
    #        val=str(val)
    
                else:
#                     print 'polygone', accessurl, index, longitude_min, latitude_min, longitude_max, latitude_max
                    my_polygon = Polygon([[(longitude_min, latitude_min, mid_altitude), (longitude_max, latitude_min, mid_altitude), (longitude_max, latitude_max, mid_altitude), (longitude_min, latitude_max, mid_altitude), (longitude_min, latitude_min, mid_altitude)]])
                    my_feature = Feature(id=index, geometry=my_polygon, properties=val)
                if longitude_min >= -180.0 and longitude_min <= 360.0 and longitude_max >= -180.0 and longitude_max <= 360.0 and latitude_min >= -90.0 and latitude_min <= 90.0 and latitude_max >= -90.0 and latitude_max <= 90.0 and spacial_frametype == 'body':
                    toto.append(my_feature)
    
    
    
    #    print 'toto', toto
    s = FeatureCollection(toto)
    return s






