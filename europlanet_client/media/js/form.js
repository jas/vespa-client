	function query_source(source){
    		$("input[name=query_source]").attr('value',source);
    		if (source=="form"){
    			$("#query_source_text").hide();
    			$("#query_source_form").show();
    			$("#nav_query_source_form").addClass("btn-primary");    			
    			$("#nav_query_source_form").removeClass("btn-light");
    			$("#nav_query_source_text").addClass("btn-light");
    			$("#nav_query_source_text").removeClass("btn-primary");
    			
    		}
    		else{
    			$("#query_source_form").hide();
    			$("#query_source_text").show();
    			$("#nav_query_source_text").addClass("btn-primary");    			
    			$("#nav_query_source_text").removeClass("btn-light");
    			$("#nav_query_source_form").addClass("btn-light");
    			$("#nav_query_source_form").removeClass("btn-primary");

    		}
	}
    	


	function services(services_str){
    	
    	if (services_str == "all"){
    		$("input[name=services]").attr('value','form_registered_services');
    		$("#main_form").attr('action',$("div[data-type=main_url]").attr("data-value"));
    		$("input").attr('action',$("div[data-type=main_url]").attr("data-value"));
    		$('#main_form').submit();
    		
    	}
    	if (services_str == "custom"){
    		$("#modalUrl").attr('value',$("#f-url_op").attr("value"));
    		$("#modalSchema").attr('value',$("#f-schema_op").attr("value"));
    		$('#customServiceModal').modal('show');
    		
    	}
    }


(function($) {"use strict";
    var c1 = {
        "" : "C1",
        "celestial" : "Right Ascension",
        "body" : "Longitude",
        "cartesian" : "X",
        "cylindrical" : "R",
        "spherical" : "R"
    }, c2 = {
        "" : "C2",
        "celestial" : "Declination",
        "body" : "Latitude",
        "cartesian" : "Y",
        "cylindrical" : "&theta;",
        "spherical" : "&theta;"
    }, c3 = {
        "" : "C3",
        "celestial" : "Distance",
        "body" : "Z",
        "cartesian" : "Z",
        "cylindrical" : "Z",
        "spherical" : "&phi;"
    }, initialSelectedValue = {
        "service_type" : "dataset"
    }, initial_spatial_frame_type = "all", initial_service_type = "dataset";

    function setCLabelsDisplay(object, spatial_frame_type) {
        var id = String($(object).attr("id")), cIndice, borne, text;

        if (id.indexOf("c1") !== -1) {
            cIndice = c1;
        } else {
            if (id.indexOf("c2") !== -1) {
                cIndice = c2;
            } else {
                cIndice = c3;
            }
        }
        text = cIndice[spatial_frame_type];
        if (id.indexOf("resol") !== -1) {
            text = text + " " + "Resolution";
        }
        
        if (id.indexOf("min") !== -1) {
            text = text + " Min";
        } else {
            if (id.indexOf("max") !== -1) {
                text = text + " Max";
            }
            else{
                if (id.indexOf("delta") !== -1) {
                    text = text + " Delta";
                }
            }
        }
        $(object).parents("div.form-group").children("label").find("abbr").html(text);
    }

    function set_spatial_frame_coordinates() {
        var spatial_frame_type = $('#f-spatial_frame_type').val();
        $('.c_value').each(function() {
            setCLabelsDisplay(this, spatial_frame_type);
        });
    }

    function setRazError(obj) {
        $(obj).css('border-color', '#DDDDDD');
    }

    function setError(obj) {
        $(obj).css('border-color', 'red');
    }

    function setInputErrorStyle(obj, error) {
        obj = $(obj).parent().find('input');
        if (error) {
            setError(obj);
        } else {
            setRazError(obj);
        }
    }

    function setTimeInterval(){
    	if ($('#f-time_interval_type_op').val() === "around") {
    		$('.TimeSearchBetween').hide();
    		$('.TimeSearchAround').show();
      }
    	else {
    		$('.TimeSearchBetween').show();
        $('.TimeSearchAround').hide();
      }
    }

    function setLocation(){
      if ($('#f-location_values_op').val() === "around") {
          $('.locationBetween').hide();
          $('.locationAround').show();
      } else {
          $('.locationBetween').show();
          $('.locationAround').hide();
      }        	
  }

    
    function activateRemoveAdvancedFieldButton(obj){
        var parent = $(obj).parents('.advanced_panel');
        var children = parent.children('.advanced_group');
        if ($(obj).parents('.advanced_panel').children('.advanced_group').length <= 2){
            $(obj).parents('div:first-child').find('button.remove_advanced_field').prop('disabled', true);
        }
        $(obj).parents('.advanced_group').remove();
    }
    function resetAdvancedFields(){
    	var advanced_fields = $("form#main_form div.advanced_group");
    	for (var i = 0; i < advanced_fields.length; ++i) {
    		var advanced_field = advanced_fields[i];
    		if (i > 0){
    			activateRemoveAdvancedFieldButton($(advanced_field).find(".remove_advanced_field"));
    		}
    	}
    }
    
    function resetMainForm(form) {
        // clearing inputs
    	$("form#main_form input.resetable").val('');
    	
    	$("form#main_form input[type='radio']").attr('value','');
    	$("form#main_form input[type='checkbox']").prop('checked',false);
    	$("form#main_form select").prop('selectedIndex', 0);
    	$("form#main_form textarea").val('');
		$("#modalUrl").val("");
		$("#modalSchema").val("");
		setTimeInterval();
		setLocation();
		set_spatial_frame_coordinates();
    	resetAdvancedFields();
    	
    }

    function validateCustomModal(){
    	
		$("#f-url_op").attr('value',$("#modalUrl").val());
		$("#f-schema_op").attr('value',$("#modalSchema").val());
    	$("#customServiceModal").modal('hide');
		$("input[name=services]").attr('value','form_custom_service');
		$("#main_form").attr('action',$("div[data-type=main_url]").attr("data-value"));
		$("input").attr('action',$("div[data-type=main_url]").attr("data-value"));
    	$('#main_form').submit();
    }

    $(document).ready(function() {
        
    	// To remove
    	$("div.form_left").addClass("col-md-4 col-md-offset-1");
        $("div.form_right").addClass("col-md-4 col-md-offset-2");
        $("div.minform_left").addClass("col-md-6");
        $("div.minform_right").addClass("col-md-6");
        
        // Add boostrap form-control class to every input and select except reset button
        $("form#main_form input:not(.main_form_reset, :radio, :checkbox), form#main_form select").addClass("form-control");

//        if ($("div.checkboxes input").prop('checked')){
//        	
//        }
        
        $('[data-toggle="tooltip"]').tooltip();
        
//        $( "form#main_form input" ).change(function() {
//        	$('#main_form').submit();
//        });
        
        $('.TimeSearchAround').hide();
        $('.locationAround').hide();
        
        if ($('input:checked[type=radio][name=services_selected][value=all]').prop('checked')){
            $("#services_fieldset").prop("disabled", true);
        }
        
//        $('input[type=radio][name=services_selected]').change(function() {
//            if (this.value == 'all') {
//                $("#services_fieldset").prop("disabled", true);
//            }
//            else {
//                $("#services_fieldset").prop("disabled", false);
//            }
//        });
//        
        
        // Add calendar to some time fields
        $("form#main_form input#f-time_min, form#main_form input#f-time_max, form#main_form input#f-time_center").addClass("datetimepickerclass");
        $(".datetimepickerclass").datetimepicker({
            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm:ss.l',
            separator : 'T',
            changeMonth : true,
            changeYear : true,
            showSecond : true,
            hourText : 'h',
            minuteText : 'm',
            secondText : 's',
            showMillisec : true,
            millisecText : 'ms',
            yearRange : "-40:+0",
            showOn : "button",
            buttonImage : "/media/images/calendar.gif",
            buttonImageOnly : true,
            constrainInput : false,
            buttonText : ""
        });
        
        // Move calendar image to the right of the label
        $("form#main_form input#f-time_min, form#main_form input#f-time_max, form#main_form input#f-time_center").parent().each(function(){
            var datetimepickerimage = $(this).find("img");
            var label = $(this).find("label");
            datetimepickerimage.insertAfter(label)
        });
        
        
                
//      set_spatial_frame_coordinates();
        $('#f-time_interval_type_op').change(function() {
        	setTimeInterval();
        });
        setTimeInterval();
        $('#f-location_values_op').change(function() {
        	setLocation();
        });
        setLocation();
        
        
        set_spatial_frame_coordinates();
        
        $('#f-spatial_frame_type').change(function() {
            set_spatial_frame_coordinates();
        });
        
        if ($('.advanced_group').length == 1){
            $('button.remove_advanced_field').prop('disabled', true);
        }
        
        
        $(".remove_advanced_field").click(function(){
                activateRemoveAdvancedFieldButton(this);
        });
        $(".add_advanced_field").click(function() {
            $('button.remove_advanced_field').prop('disabled', false);
            var prev_field = $(this).parents('.input-group').prev();
            var clone = prev_field.clone();
            clone.find('button.remove_advanced_field').prop('disabled', false);
            clone.find('input').val('');
            clone.insertAfter(prev_field);
            clone.find(".remove_advanced_field").click(function(){
                activateRemoveAdvancedFieldButton(this);
            });
        });
        
        $('.panel-collapse').has('.has-error').collapse('show');
        	
        $(".open_collapse_onload").collapse('show');
        
        if ($("input[name=services]").attr("value") != "form_custom_service"){
        	$(".custom_service_fields").hide();
        }
        else{
        	$(".open_collapse_onload").collapse();
        }
        
        if ($("input[name=query_source]").attr("value") == "form"){
        	$("#query_source_text").hide();
        }
        else{
        	$("#query_source_form").hide();
        }
        
        $("#form_column").show();

        
        $(".main_form_submit").click(function(event) {
        	event.preventDefault();
            var cpt = 0;
            $('.advanced_group').each(function(){
                if ($(this).find('input').val() != ''){
                    $(this).find('.advanced_label').attr('name','advanced_'+cpt.toString()+'_label');
                    $(this).find('.advanced_op').attr('name','advanced_'+cpt.toString()+'_op');
                    $(this).find('.advanced_value').attr('name','advanced_'+cpt.toString()+'_value');
                    cpt = cpt + 1;
                }
            });
            $('#main_form').submit();
        });
        
//        $("#customServiceModalForm").submit(function() {
//        	alert("toto");
//        });
        
        $(".modal-body").keypress(function(e) {
            if(e.which == 13) {
            	validateCustomModal();
            }
        });
        $("#submitCustomServiceModal").click(function() {
        	validateCustomModal();
        });

        $.ajax({
            url : $('div[data-type=help_mandatory_url]').attr('data-value'),
            async : true,
            type : 'GET',
            dataType : 'json',
            success : function(xhr, status) {
                for (var field_name in xhr){
                    $("#f-"+field_name+"_help").attr('title',xhr[field_name]);
                }
            },
        });
        
        
        
 	   	var TARGET_CLASS_TO_SSODNET = {
			   'asteroid' : 'Asteroid',
			   'dwarf_planet' : '\"Dwarf Planet\"',
			   'comet' : 'Comet',
			   'planet' : 'Planet',
			   'exoplanet' : 'Exoplanet',
			   'satellite' : 'Satellite'
 	   	};
        
        $(function() {
            $('#f-target_name').autocomplete({
               source: function(request, response) {
            	   var type_list = [];
            	   $("input[name=f-target_class]:checked").each(function(){
            		   var target_class = $(this).attr('value');
            		   if (target_class in TARGET_CLASS_TO_SSODNET){
            			   type_list.push(TARGET_CLASS_TO_SSODNET[target_class]);
            		   }
            	   });
            	   var type_query = "";
            	   if ( (type_list.length == 1)){
                	   type_query = "&type=" + type_list[0]
            	   }
                      $.ajax({
//                    	  http://vo.imcce.fr/webservices/ssodnet/resolver
                         url: "https://api.ssodnet.imcce.fr/quaero/1/sso/instant-search",
//                    	 url: "http://vo.imcce.fr/webservices/ssodnet/resolver",
                         type: "GET",
                         data: 'q=' + request.term + type_query + '&from=Vespa',
//                         data: "mime=json&(name=\"" + request.term + "*\"",
                         dataType: "json",
                         success: function(data, status, xhr) {
                        	 var toto = 
                        		 response($.map(data.data, function(target) {
                            	var label = '';
                            	var aliases = [];
                            	if ((type_list.length <= 1) || (type_list.length > 1 && type_list.indexOf(target.type) > -1)) {
                            		if (target.hasOwnProperty('aliases')) {
										label = target.name + " (" + target.type + ", Aliases:" + target.aliases + ")";
										aliases = target.aliases;
									}
                            		else {
										label = target.name + " (" + target.type + ")";
									}
									return {
										label : label,
										value : target.name,
										type: target.type,
										link : target.links.self
									}
								}
                            }));
                         }
                      });
               },
               minLength: 2
            });
         });
        $("form#main_form input.main_form_reset").click(function(){
        	resetMainForm();
        });
        
        
        
    });
})(jQuery);
