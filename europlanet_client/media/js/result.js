    function advanced_form_link_click(service_infos){
        var form = $("#query_advanced_url_form");
//      form.attr("method","POST");
//      form.attr("action",$("#query_advanced_url").attr('value'));
        if(service_infos.length == 1){
            var input = document.createElement("input");
            input.name = "service_id";
            input.value = service_infos[0];
            form.append(input);
        }
        else{
            var input1,input2;
            input1 = document.createElement("input");
            input1.name = "service_url";
            input1.value = service_infos[0];
            input2 = document.createElement("input");
            input2.name = "service_schema";
            input2.value = service_infos[1];            
            form.append(input1);
            form.append(input2);
        }
        form.submit();
    }


(function($) {"use strict";
    $(document).ready(function() {
        $('#form_service_type').change(function() {
            $('#form_switch_rtype').submit();
        });
        $('.advanced_form_link').click(function() {
            var advanced_form_url = $(this).parent().children("div.advanced_form_action").attr('value');
            var form = $(this).parents().eq(3).children('form#form_switch_rtype');
            form.attr("action", advanced_form_url);
            form.submit();
            return false;
        });
    });
})(jQuery);
