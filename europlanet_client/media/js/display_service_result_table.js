$(function() {

	$(document)
			.ready(
					function() {
						var org_buildButton = $.fn.DataTable.Buttons.prototype._buildButton;
						$.fn.DataTable.Buttons.prototype._buildButton = function(
								config, collectionButton) {
							var button = org_buildButton.apply(this, arguments);
							$(document).one(
									'init.dt',
									function(e, settings, json) {
										if (config.container
												&& $(config.container).length) {
											$(button.inserter[0]).detach()
													.appendTo(config.container)
										}
									})
							return button;
						}
						function Large(obj, preview_src, event) {
							var imgbox = document.getElementById("imgbox");
							imgbox.style.visibility = 'visible';
							var img = document.createElement("img");
							img.src = preview_src;
							img.style.maxWidth = '200px';
							img.style.maxHeight = '200px';
							imgbox.innerHTML = '';
							imgbox.appendChild(img);
							var x = event.pageX, y = event.pageY;
							imgbox.style.right = x;
							imgbox.style.bottom = y;
						}

						function Out() {
							document.getElementById("imgbox").style.visibility = 'hidden';
						}
						
						function send_datalink_ajax(url, params){
							
							$.ajax({
								url : url,
								async : true,
								type : 'GET',
								data : params,
								dataType : 'json',
								success : function(xhr, status) {
									$("#datalinkChoice").empty();
									$('.inputParams').remove();
										for (var i=0;i<xhr.length;i++){
											var datalinkService = xhr[i];
											$("#datalinkChoice").append("<option value='"+datalinkService['access_url']+"'>"+datalinkService['description']+"</option>");
											if ('inputParams' in datalinkService){
												var inputParamsId = "inputParams_"+i;
												$("#datalinkModalBody").append("<div class='inputParams' id='"+inputParamsId+"'</div>");
												for (var j=0;j<datalinkService['inputParams'].length;j++){
													var param = datalinkService['inputParams'][j];
													var modalParameter = "<div class='form-group'><label for='"+param['name']+"' class='col-form-label'>"+param['name']+"</label><input type='text' class='form-control' id='"+param['name']+"' value='"+param['value']+"'><small class='form-text text-muted'>"+param['description']+"</small></div>"
													$('#'+inputParamsId).append(modalParameter);
													if (i!=0){
														$('#inputParams_'+i).hide();
													}
												}
											}
										
									}
									$('#datalinkChoiceModal').modal('show');
									
								}
							});
						}
						
						function getDom(){
							if ($("#paging").attr('value') == "True"){
								return "l<'#colvis_buttons'><'#selection_buttons'>rtip"
							}
							else{
								return "l<'#colvis_buttons'><'#selection_buttons'>rti"
							}
						}

						$("#datalinkChoice").change(function(){
							var index= $("#datalinkChoice option:selected" ).index();
							$('.inputParams').hide();
							$('#inputParams_'+index).show();
						});
						
						$("#submitdatalinkModal").click(function(){
							var index= $("#datalinkChoice option:selected" ).index();
							var url = $("#datalinkChoice option:selected").attr('value');
							if ($('#inputParams_'+index).length > 0){
								var inputParams = $('#inputParams_'+index+' input');
								for(var i=0;i<inputParams.length;i++){
									if (i==0){
										url = url+"?";
									}
									else{
										url = url+"&"
									}
									url = url + inputParams[i]['id']+"="+inputParams[i]['value'];
								}
							}
							window.open(url,'_blank');
						});
						
						$.fn.dataTable.ext.buttons.columnVisibility.action = function(
								e, dt, button, conf) {
							var col = dt.columns(conf.columns);
							var curr = col.visible();

							col
									.visible(conf.visibility !== undefined ? conf.visibility
											: !(curr.length ? curr[0] : false));
							datatable.ajax.reload(null, false);
						};

						$.fn.dataTable.ext.buttons.colvisGroup.action = function(
								e, dt, button, conf) {
							dt.columns(conf.show).visible(true);
							dt.columns(conf.hide).visible(false);
							if (conf.hide.length == 0) {
								datatable.ajax.reload(null, false);
							}
						};

						function isColumnNeeded(column_name, columns) {
							switch (column_name) {
							case "spectral_range_min":
								if (selected_units["frequency"] == "micro_meter"
										|| columns[index_spectral_sampling_step_max].bVisible
										|| columns[index_spectral_resolution_max].bVisible) {
									return true;
								}
								return false;
							case "spectral_range_max":
								if (selected_units["frequency"] == "micro_meter"
										|| columns[index_spectral_sampling_step_min].bVisible
										|| columns[index_spectral_resolution_min].bVisible) {
									return true;
								}
								return false;
							case "spectral_sampling_step_min":
								if (selected_units["frequency"] == "micro_meter"
										|| columns[index_spectral_sampling_step_max].bVisible) {
									return true;
								}
								return false;
							case "spectral_sampling_step_max":
								if (selected_units["frequency"] == "micro_meter"
										|| columns[index_spectral_sampling_step_min].bVisible) {
									return true;
								}
								return false;
							default:
								return false;
							}
						}

						var defaultSortIndex = $("#service_results").attr(
								"default_sort");

						var COEFF_MICROMETER_TO_HERTZ = 2.99792458e14;
						
						var COEFF_PERCENTIMETER_TO_MICROMETER = COEFF_MICROMETER_TO_HERTZ/10000;

						var selected = [];
						var units = {
							"frequency" : {
								"default" : {
									'value' : "hertz",
									'display' : 'Hz'
								},
								"alt1" : {
									'value' : 'micro_meter',
									'display' : '&micro;m'
								},
								"alt2" : {
									'value' : 'per_centimeter',
									'display' : 'cm<sup>-1</sup>'
								}
							}
						};
						var selected_units = {
							"frequency" : "hertz"
						};
						var reverted_units = [ "micro_meter" ];
						var index_spectral_range_min = $(
								"table#service_results thead tr th.spectral_range_min")
								.index();
						var index_spectral_range_max = $(
								"table#service_results thead tr th.spectral_range_max")
								.index();
						var index_spectral_sampling_step_min = $(
								"table#service_results thead tr th.spectral_sampling_step_min")
								.index();
						var index_spectral_sampling_step_max = $(
								"table#service_results thead tr th.spectral_sampling_step_max")
								.index();

						var frequency_columns = [];
						$(
								"table#service_results thead tr th a[unit_type=frequency]")
								.each(
										function() {
											frequency_columns.push($(this)
													.parent().attr('name'));
										});

						var url_columns = [];
						$('table#service_results thead tr th.link').each(
								function() {
									url_columns.push($(this).attr('name'));
								});

						var current_url = null;

						var JDToIsoDate = [];
						$('table#service_results thead tr th.JDToIsoDate')
								.each(function() {
									JDToIsoDate.push($(this).attr('name'));
								});
						var datalink_parameters_values_indice = {};
						$('.datalink_inputParams').each(function() {
							var parameter_name = $(this).attr('data-type');
							var parameter_value = $(this).attr('data-value');
							var indice = $("table#service_results thead tr th[name="+parameter_value+"]").index();
							datalink_parameters_values_indice[parameter_name] = indice;
						});
						// var GranuleGID_list = [];
						// var GranuleGID_buttons =
						// $('.granule_gid').each(function(){
						//      
						// )};
						// [
						// GranuleGID_buttons
						// {
						// text : 'toto',
						// action: function ( e, dt, node, config ) {
						// this.active(true);
						// },
						// },
						//     
						// {
						// text : 'titi'
						// }
						// ],

						var datatable = $('#service_results')
								.DataTable(
										{
											"processing" : true,
											"serverSide" : true,
											"order" : [ [ defaultSortIndex,
													"desc" ] ],
//													dom : "<'#colvis_buttons'><'#selection_buttons'>rtipl",
											dom : getDom(),
											pagingType : "inputBootstrap",
											renderer : 'bootstrap',
											"aLengthMenu" : [
													[ 10, 25, 50, 100, 1000, 10000 ],
													[ 10, 25, 50, 100, "1 000", "10 000" ] ],
											"pageLength": 25,
											"columns" : function() {
												var columns = [];
												$(
														'table#service_results thead tr th')
														.each(
																function() {
																	var visible = false;
																	if ($(this)
																			.attr(
																					'visible') == "True") {
																		visible = true;
																	}
																	var always_requested = false;
																	if ($(this)
																			.attr(
																					'always_requested') == "True") {
																		always_requested = true;
																	}
																	var units = null;
																	if ($(this)
																			.hasClass(
																					"multi_unit")) {
																		var unit_type = $(
																				this)
																				.find(
																						'a')
																				.attr(
																						'unit_type');
																		var unit_value = $(
																				this)
																				.find(
																						'a')
																				.attr(
																						'unit_value');
																		units = {
																			'unit_type' : unit_type,
																			'unit_value' : unit_value
																		};
																	}
																	columns
																			.push({
																				'name' : $(
																						this)
																						.attr(
																								'name'),
																				'visible' : visible,
																				'always_requested' : always_requested,
																				'units' : units
																			});
																});
												return columns
											}(),
											"ajax" : {
												"url" : "json/",
												"type" : "POST",
												error : function(xhr, error,
														thrown) {
													if (xhr.status != 0){
														alert('Internal Server Error');
													}
												},
												data : function(d, t) {
													if ($("#service_id").length){
														d.service_id = $("#service_id").attr('value');
													}
													else{
														d.service_schema = $("#service_schema").attr('value');
														d.service_access_url = $("#service_access_url").attr('value');
													}
													d.query_conditions = $(
															"#query_conditions")
															.attr('value');
													for (var i = 0; i < d.columns.length; i++) {
														d.columns[i].name = t.aoColumns[i].name;
														if ((t.aoColumns[i].bVisible
																|| t.aoColumns[i].always_requested) && (t.aoColumns[i].name != "datalink")) {
															d.columns[i].requested = true;
														} else {
															d.columns[i].requested = false;
														}
														// If sorted by column
														// where unit chosen is
														// inversely related
														// with default unit :
														// invert sort value
														// (asc,desc)
														if (i == d.order[0].column
																&& d.columns[i].units != null
																&& d.columns[i].units.unit_value in reverted_units) {
															if (d.order[0].dir == 'asc') {
																d.order[0].dir = 'desc';
															} else {
																d.order[0].dir = 'asc';
															}
														}
													}
													return {
														"args" : JSON
																.stringify(d),
														csrfmiddlewaretoken : document
																.getElementsByName('csrfmiddlewaretoken')[0].value
													};
												},
											// "dataSrc": function ( json ) {
											// current_url = json['url'];
											// return json.data;
											// }
											},

											buttons : [

													{
														extend : 'colvis',
														container : '#colvis_buttons'
													},
													{
														extend : 'colvisGroup',
														text : 'Show all',
														show : ':hidden',
														container : '#colvis_buttons'
													},
													{
														extend : 'colvisGroup',
														text : 'Hide all',
														hide : ':visible',
														container : '#colvis_buttons'
													},
													// {
													// extend: 'colvis',
													// text: 'Granule_gid',
													// buttons:
													// GranuleGID_buttons,
													// container :
													// '#colvis_buttons'
													// },
													{
														text : 'Select All in current page',
														action : function(e,
																dt, node,
																config) {
															var rows = datatable
																	.rows();
															rows.select();
														},
														container : '#selection_buttons'

													},
													{
														text : 'Reset Selection',
														action : function(e,
																dt, node,
																config) {
															var rows = datatable
																	.rows({
																		selected : true
																	});
															rows.deselect();
														},
														container : '#selection_buttons'

													} ],
											select : {
												style : 'multi',
												rows : "rows selected"
											},
											"columnDefs" : [
													{
														"render" : function(
																data, type, row) {
															if (data != null
																	&& data != "") {
																// var
																// text_array =
																// data.split("/");
																var text = data
																		.substring(
																				0,
																				20);
																;
																return '<a href="'
																		+ data
																		+ '" target=_blank>'
																		+ text
																		+ '...</a>';
															} else {
																return null;
															}
														},
														"targets" : url_columns,
													},
													{
														"render" : function(
																data, type, row) {
															if (data != null) {
																var JD_POSIX_TIMESTAMP_ORIGIN = 2440587.5;
																var jd_from_posix_timestamp_origin = data
																		- JD_POSIX_TIMESTAMP_ORIGIN;
																var MILLI_SECONDS_IN_A_DAY = 1000 * 60 * 60 * 24;
																var time_date = new Date();

																time_date
																		.setTime(jd_from_posix_timestamp_origin
																				* MILLI_SECONDS_IN_A_DAY);
																time_str = time_date
																		.toISOString();
																return time_str
																		.substr(
																				0,
																				time_str.length - 1);
															}
															return null;
														},
														"targets" : JDToIsoDate
													},
													{
														"render" : function(
																data, type, row) {
															if (data != null) {
																var datasplit = data
																		.split("#");
																var res;
																var data_display = "";
																for (var i = 0; i < datasplit.length; i++) {
																	switch (datasplit[i]) {
																	case "im":
																		res = "image";
																		break;
																	case "sp":
																		res = "spectrum";
																		break;
																	case "ds":
																		res = "dynamic_spectrum";
																		break;
																	case "sc":
																		res = "spectral_cube";
																		break;
																	case "pr":
																		res = "profile";
																		break;
																	case "vo":
																		res = "volume";
																		break;
																	case "mo":
																		res = "movie";
																		break;
																	case "cu":
																		res = "cube";
																		break;
																	case "ts":
																		res = "time_series";
																		break;
																	case "ca":
																		res = "catalogue";
																		break;
																	case "ma":
																		res = 'map';
																		break;
																	case "ci":
																		res = 'catalogue_item';
																		break;
																	case "ev":
																		res = 'Event';
																		break;
																		
																	default:
																		res = datasplit[i];
																	}

																	if (i != datasplit.length - 1) {
																		res = res
																				+ "#";
																	}
																	data_display = data_display
																			+ res;
																}
																return data_display;
															} else {
																return null;
															}
														},
														"targets" : [ "dataproduct_type" ]
													},
													{
														"render" : function(
																data, type, row) {
															if (data != null) {
																if (selected_units['frequency'] == 'micro_meter') {
																	return COEFF_MICROMETER_TO_HERTZ / data;
																} else {
																	if (selected_units['frequency'] == 'per_centimeter') {
																		return data / COEFF_PERCENTIMETER_TO_MICROMETER;
																	}
																	else{
																		return data;
																	}
																}
															} else {
																return null;
															}
														},
														"targets" : [
																"spectral_range_min",
																"spectral_range_max" ]
													},
													{
														"render" : function(
																data, type, row) {
//															datalink_parameters_values_indice[parameter_name] = indice;
															var params_dict = {};
															for (var parameter_name in datalink_parameters_values_indice){
																params_dict[parameter_name] = row[datalink_parameters_values_indice[parameter_name]];
															}
															var parameters_div = "<div style='display:none'>";
															for (var param_name in params_dict){
																parameters_div = parameters_div + "<div class='granule_datalink' data-name=" + param_name + " data-value=" + params_dict[param_name] + "></div>"
															}
//															var parameters_div = "";
															parameters_div = parameters_div + "</div>"
//															return "<button type='button' class='btn btn-default center-block' aria-label='Left Align' title='Download Votable'><span class='glyphicon glyphicon-download' aria-hidden='true'></span></button>";
															return "<button type='button' class='btn btn-success center-block send_datalink_button'>SEND" +  parameters_div + "</button>";
//															return "<span class='truc'>TOTO</span>";
//															return "<button type=\"button\" class=\"btn btn-default service_results_button download_results_button\" aria-label=\"Left Align\" title=\"Download Votable\" onclick=\"window.open('{{r.url_query_select}}')\"><span class=\"glyphicon glyphicon-download\" aria-hidden=\"true\"></span></button>";

														},
														"targets" : ["datalink"]
//														"targets" : ["datalink_url"]

													},
											// {
											// "render": function ( data, type,
											// row ) {
											// if (data != null){
											// if (selected_units['frequency']
											// == 'micro_meter'){
											// var spectral_sampling_step_min =
											// row[index_spectral_sampling_step_min],
											// spectral_range_min =
											// row[index_spectral_range_min];
											// return
											// COEFF_MICROMETER_TO_HERTZ*(spectral_sampling_step_min+data)/2/Math.pow(spectral_range_min,2);
											// }
											// else{
											// return data;
											// }
											// }
											// else{
											// return null;
											// }
											// },
											// "targets":
											// index_spectral_sampling_step_max
											// },
											// 299792458000000 / sp_max AS
											// spectral_range_min,
											// 299792458000000 / sp_min AS
											// spectral_range_max,
											// 299792458000000 * (sp_step_min +
											// sp_step_max) / 2 / (sp_max ^ 2)
											// AS spectral_sampling_step_min,
											// 299792458000000 * (sp_step_min +
											// sp_step_max) / 2 / (sp_min ^ 2)
											// AS spectral_sampling_step_max,
											// 299792458000000 * (sp_res_min +
											// sp_res_max) / 2 / (sp_max ^ 2) AS
											// spectral_resolution_min,
											// 299792458000000 * (sp_res_min +
											// sp_res_max) / 2 / (sp_min ^ 2) AS
											// spectral_resolution_max
											//                                

											],
											stateSave : false,
										});

						datatable.on('draw.dt', function() {
							datatable.$('tr').hover(
									function(event) {
										var column_index = datatable.column(
												'thumbnail_url:name').index();
										preview_src = datatable.row(this)
												.data()[column_index];
										if (preview_src !== null) {
											Large(this, preview_src, event);
										}
									}, function() {
										Out();
									});
							click_link_change_unit();
						});
						
						datatable.on('select', function(e, dt, type, indexes) {
							$('button.selection_button')
									.prop('disabled', false);
							$(".action_on_selection").removeClass('disabled');
						});
						datatable.on('deselect',
								function(e, dt, type, indexes) {
									if (dt.rows({
										selected : true
									}).indexes().length == 0) {
										$('button.selection_button').prop(
												'disabled', true);
										$(".action_on_selection").addClass(
												'disabled');

									}
								});
						datatable.on( 'click', 'button.send_datalink_button', function(e, dt, type, indexes) {
							var url = $("#datalink_url").attr('data-value');
							var params = {};
							$(".granule_datalink",this).each(function(){
								params[$(this).attr('data-name')] = $(this).attr('data-value');
							})
							params ['datalink_url'] = $("#datalink_accessURL").attr('data-value');
							send_datalink_ajax(url, params);
					    } );

						function download(rows) {
							var form = $("#download_form");
							var access_url_column_index = datatable.column(
									'access_url:name').index();
							var nbData = 0;
							for (i = 0; i < rows.length; i = i + 1) {
								var access_url = datatable.row(rows[i]).data()[access_url_column_index];
								if (access_url != '') {
									var field = $('<input></input>');
									field.attr("type", "hidden");
									field.attr("class", "download_access_url");
									field.attr("name", "access_url_" + i);
									field.attr("value", access_url);
									nbData = nbData + 1;
									form.append(field);
								}
							}
							if (nbData == 0) {
								alert("No Data to download");
							} else {
								form.submit();
							}
							$(".download_access_url").remove();

						}

						$(".dropdown-menu *").click(
								function() {
									var id_dropdownMenu = $(this).closest(
											"ul.dropdown-menu").attr(
											"aria-labelledby");
									$("button#" + id_dropdownMenu).dropdown(
											'toggle');
								});

						$('#download_data_selection')
								.click(
										function() {
											var rows = datatable.rows({
												selected : true
											}).indexes();
											if (rows.length > 100) {
												alert('You can not download more than 100 results');
											} else {
												download(rows);
											}
											return false;

										});

						$('#download_data_all')
								.click(
										function() {
											var rows = datatable.rows()
													.indexes();
											if (rows.length > 100) {
												alert('You can not download more than 100 results');
												return false;
											} else {
												download(rows);
												return false;
											}
										});

						function send(rows, mType) {
							var access_url_column_index = datatable.column(
									'access_url:name').index();
							var dataList = []
							for (i = 0; i < rows.length; i = i + 1) {
								var access_url = datatable.row(rows[i]).data()[access_url_column_index];
								var data = {};
								if (access_url != '') {
									data['access_url'] = access_url;
									data['type'] = mType;
									dataList.push(data);
								}
							}
							if (dataList.length == 0) {
								alert("No Data to send");
							} else {
								samp_europlanetclient.samp_data(dataList);
								$(this).val('default');
							}

						}

						$('.send_data_selection')
								.click(
										function() {
											var mType = $(this).attr('mtype');
											var rows = datatable.rows({
												selected : true
											}).indexes();
											if (rows.length > 100) {
												alert('You can not send more than 100 results');
											} else {
												send(rows, mType);
											}
											return false;
										});

						if ($("#targets").attr('value') == "") {
							$(".li_send_geojson").hide();
						}

						$('.send_data_all')
								.click(
										function() {
											var mType = $(this).attr('mtype');
											var rows = datatable.rows()
													.indexes();
											if (rows.length > 100) {
												alert('You can not send more than 100 results');
											} else {
												send(rows, mType);
											}
											return false;
										});

						function getSelectionWhereClause(rows) {
							var id_column_index = datatable.column(
									'granule_uid:name').index();
							var selectionWhereClause = "(+";
							for (i = 0; i < rows.length; i = i + 1) {
								if (i !== 0) {
									selectionWhereClause += "+OR+";
								}
								var id = datatable.row(rows[i]).data()[id_column_index];
								selectionWhereClause += "granule_uid+=+\'" + encodeURIComponent(id)
										+ "\'";
							}
							selectionWhereClause += "+)";
							return selectionWhereClause;
						}

						$("#download_metadata_selection")
								.click(
										function() {
											var rows = datatable.rows({
												selected : true
											}).indexes();
											if (rows.length > 100) {
												alert('You can not download more than 100 results');
												return false;
											} else {
												var selectionWhereClause = getSelectionWhereClause(rows);
												var votable_url = $(
														"#base_url_query_votable")
														.attr('value')
														+ "&"
														+ $("#adql_query_all")
																.attr('value')
														+ "+WHERE+"
														+ selectionWhereClause;
												window.location.href = votable_url;
												return false;
											}
										});

						$("#download_metadata_all").click(
								function() {
									var votable_url = $("#url_query_select")
											.attr('value');
									window.location.href = votable_url;
									return false;
								});

						$("#send_metadata_table_selection")
								.click(
										function() {
											var rows = datatable.rows({
												selected : true
											}).indexes();
											var selectionWhereClause = getSelectionWhereClause(rows);
											var votable_url = $(
													"#base_url_query_votable")
													.attr('value')
													+ "&"
													+ $("#adql_query_all")
															.attr('value')
													+ "+WHERE+"
													+ selectionWhereClause;
											var schema = $('#service_schema')
													.attr('value');
											samp_europlanetclient.samp_votable(
													votable_url, schema);
											return false;
										});

						$("#send_metadata_geojson_selection")
								.click(
										function() {
											var rows = datatable.rows({
												selected : true
											}).indexes();
											var index_sregion_column_ = datatable
													.column('.s_region')
													.index();
											var target_name = $(
													"#target_select").val();
											var service_schema = $(
													"#service_schema").attr(
													'value');
											var service_access_url = $(
													"#service_access_url")
													.attr('value');
											var query_conditions = $(
													"#query_conditions").attr(
													'value');
											var base_url_get_geojson = $(
													"#base_url_get_geojson")
													.attr('value');
											var rows = datatable.rows({
												selected : true
											}).indexes();
											var selectionWhereClause = getSelectionWhereClause(rows);
											var url_get_geojson = base_url_get_geojson
													+ "?service_schema="
													+ service_schema
													+ "&service_access_url="
													+ service_access_url
													+ "&target_name="
													+ target_name
													+ "&query_conditions="
													+ selectionWhereClause;
											var catalog_name = $(
													'#service_schema').attr(
													'value');
											samp_europlanetclient.samp_geojson(
													target_name, catalog_name,
													url_get_geojson);
											return false;
										});

						$("#send_metadata_sregion_selection")
								.click(
										function() {
											var rows = datatable.rows({
												selected : true
											}).data();
											var index_sregion_column_ = datatable
													.column('.s_region')
													.index();
											var s_region_list = "";
											for (var i = 0; i < rows.length; i++) {
												stc_region = new tapHandleSregion.STCRegion(
														rows[i][index_sregion_column_]);
												s_region_list = s_region_list
														+ stc_region
																.getAladinScript();
											}
											samp_europlanetclient
													.samp_sregion([ s_region_list ]);
											return false;
										});

						$("#send_metadata_sregion_allInCurrentPage")
								.click(
										function() {
											var rows = datatable.rows().data();
											var index_sregion_column_ = datatable
													.column('.s_region')
													.index();
											var s_region_list = "";
											for (var i = 0; i < rows.length; i++) {
												stc_region = new tapHandleSregion.STCRegion(
														rows[i][index_sregion_column_]);
												// s_region_list = s_region_list
												// + stc_region.stcString + ";";
												// toto =
												// stc_region.getAladinScript();
												//          
												// s_region_list.push(stc_region.stcString+
												// ";");
												// s_region_list.push(stc_region.getAladinScript());
												s_region_list = s_region_list
														+ stc_region
																.getAladinScript();
											}
											samp_europlanetclient
													.samp_sregion([ s_region_list ]);
											return false;
										});

						$("#send_metadata_table_all").click(
								function() {
									var votable_url = $("#url_query_select")
											.attr('value');
									// var votable_url = current_url;
									var schema = $('#service_schema').attr(
											'value');
									samp_europlanetclient.samp_votable(
											votable_url, schema);
									return false;
								});

						$("#send_geojson_all")
								.click(
										function() {
											var service_schema = $(
													"#service_schema").attr(
													'value');
											var service_access_url = $(
													"#service_access_url")
													.attr('value');
											var query_conditions = $(
													"#query_conditions").attr(
													'value');
											var target_name = $(
													"#target_select").val();
											var base_url_get_geojson = $(
													"#base_url_get_geojson")
													.attr('value');
											var url_get_geojson = base_url_get_geojson
													+ "?service_schema="
													+ service_schema
													+ "&service_access_url="
													+ service_access_url
													+ "&target_name="
													+ target_name
													+ "&query_conditions="
													+ query_conditions;
											var catalog_name = $(
													'#service_schema').attr(
													'value');
											samp_europlanetclient.samp_geojson(
													target_name, catalog_name,
													url_get_geojson);
											return false;
										});
						$("#datalink_access").click(
								function() {

									var votable_url = $("#url_query_select")
											.attr('value');
									// var votable_url = current_url;
									var schema = $('#service_schema').attr(
											'value');
									samp_europlanetclient.samp_votable(
											votable_url, schema);
									return false;
								});

						// Ugly trick to display unit link in column header but
						// NOT in column visibility choices
						var muti_unit_columns = datatable
								.columns(".multi_unit")
								.every(
										function() {
											var header = this.header();
											link_html = $(header).find('a');
											var unit_type = link_html
													.attr('unit_type');
											var unit_text = "("
													+ units[unit_type]['default']['display']
													+ ")";
											link_html
													.attr(
															'unit_value',
															units[unit_type]['default']['value']);
											link_html.html(unit_text);
											$(header).html(
													$(header).html().replace(
															unit_text, ""));
										});
						// var header= this.header();
						// link_html = $(header).find('a');
						//  
						// var unit_type = link_html.attr('unit_type');
						// link_html.attr('unit_value',units[unit_type]['default']['value']);
						// var unit_text =
						// "("+units[unit_type]['default']['display']+")";
						// $(header).html($(header).html().replace(unit_text,""));
						// // text =
						// link_html.html().replace("("+unit_type+")","");
						//  
						// link_html.html("TOTO");
						var click_link_change_unit = function click_link_change_unit(
								event) {
							$(".link_change_unit").unbind("click");
							$(".link_change_unit")
									.click(
											function(event) {
												var unit_type = $(this).attr(
														'unit_type');
												var unit_value = $(this).attr(
														'unit_value');
												var new_unit_value = units[unit_type]['default']['value'];
												var new_unit_display = units[unit_type]['default']['display'];
												if (unit_value == units[unit_type]['default']['value']) {
													new_unit_value = units[unit_type]['alt1']['value'];
													new_unit_display = units[unit_type]['alt1']['display'];
												}
												else{
													if (unit_value == units[unit_type]['alt1']['value']) {
														new_unit_value = units[unit_type]['alt2']['value'];
														new_unit_display = units[unit_type]['alt2']['display'];
													}
												}
												selected_units[unit_type] = new_unit_value;
												$(".link_change_unit").attr(
														'unit_value',
														new_unit_value);
												$(".link_change_unit").html(
														"(" + new_unit_display
																+ ")");
												event.stopPropagation();
												datatable.draw(false);
												return false;
											});
						};

					});
});
