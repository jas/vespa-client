// TODO Change the color display with proper class and css

(function($) {
	"use strict";
	
	$(document).ready(function() {
		
		$(".res-votable").click(function(event) {
			var tableUrl = $(this).parents(".service").find(".select_query").attr('data-value');
			var table_id = $(this).parents(".service").find(".table_id").attr('data-value');
			samp_europlanetclient.samp_votable(tableUrl, table_id);
			return false;
		});
		
		function InsertServiceSortedByName(service_element, shortname, service_type, res ){
			var services = $(".service[data-service-type="+service_type+"][data-res="+res+"]");
			for (var i=0;i < services.length; i++){
				var service = services[i];
				if (shortname < service.getAttribute("data-shortname")){
					service_element.insertBefore(services[i]);
					return;
				}
			}
			service_element.insertAfter($(".service[data-service-type="+service_type+"][data-res="+res+"]").last());
			return;
		}
		
		$('.service').each(function() {
			var bgcolor = '', bgimage = '', e = $(this), s = $(this).find('.s'), count_request = $(this).find(".count_query").attr('data-value'),
			access_url = $(this).find(".access_url").attr('data-value'),
			count_request_url = $(this).find(".count_request_url").attr('data-value'), results, results_display,
			res_buttons = $(this).find(".service_buttons"),
			display_results_button = $(this).find(".display_results_button"),
			samp_results_button = $(this).find(".samp_results_button"),
			download_results_button = $(this).find(".download_results_button"),
			advanced_form_results_button = $(this).find(".advanced_form_results_button"),
			service_type= $(this).attr("data-service-type"),
			shortname = $(this).attr("data-shortname"),
			active = $(this).attr("data-active");
			if (active!="False"){
			
			$.ajax({
				url : count_request_url,
				async : true,
				type : 'GET',
				data : 'access_url=' + encodeURIComponent(access_url) + "&count_request=" + encodeURIComponent(count_request),
				dataType : 'json',
				beforeSend : function(xhr) {
					e.css('backgroundColor', 'white');
					s.css('backgroundImage', 'url("/media/images/loading.gif?g")');
				},
				success : function(xhr, status) {
					results = e.find(".results");
					if (xhr.nb_rows !== "0") {
						bgcolor = '#a8eea4';
						if (xhr.nb_rows == 1){
							results_display = 1 + " result";
						}
						else{
							results_display = xhr.nb_rows + " results";
						}
						results.html(results_display);
						display_results_button.show();
						samp_results_button.show();
						download_results_button.show();

						e.attr("data-res","results");
						// If only one service, do nothing
						if ($(".service[data-service-type="+service_type+"]").length != 1){
							var service_element_detached = e.detach();
							if ($(".service[data-service-type="+service_type+"][data-res=results]").length > 0){
								InsertServiceSortedByName(service_element_detached, shortname, service_type, "results")
							}
							else{
								service_element_detached.insertBefore($(".service[data-service-type="+service_type+"]").first());
							}
						}
					} else {
						results_display = "0 result";
						download_results_button.show();
						results.html(results_display);
						bgcolor = 'lightgrey';
						e.attr("data-res","no_result");
						if ($(".service[data-service-type="+service_type+"]").length != 1){
							var service_element_detached = e.detach();
							if ($(".service[data-service-type="+service_type+"][data-res=no_result]").length > 0){
								InsertServiceSortedByName(service_element_detached, shortname, service_type, "no_result");
							}
							else{
								if ($(".service[data-service-type="+service_type+"][data-res=results]").length != 0){
									service_element_detached.insertAfter($(".service[data-service-type="+service_type+"][data-res=results]").last());
								}
								else{
									service_element_detached.insertBefore($(".service[data-service-type="+service_type+"][data-res!=result]").first());
								}
							}
						}
					}
					results.attr('data-value',xhr.nb_rows);
					advanced_form_results_button.show();
					res_buttons.show();
					e.css('backgroundColor', bgcolor);
					s.css('backgroundImage', "");
				},
				error : function(xhr, status, exception) {
					results = e.find(".results");
					results_display = "Error";
					results.html(results_display);
					download_results_button.show();
					res_buttons.show();
					e.css('backgroundColor', '#eea8a4');
					s.css('backgroundImage', "");
					e.attr("data-res","error");
					if ($(".service[data-service-type="+service_type+"]").length != 1){
						var service_element_detached = e.detach();
						if ($(".service[data-service-type="+service_type+"][data-res=error]").length > 0){
							InsertServiceSortedByName(service_element_detached, shortname, service_type, "error");
						}
						else{
							if ($(".service[data-service-type="+service_type+"][data-res=wait]").length != 0){
								service_element_detached.insertBefore($(".service[data-service-type="+service_type+"][data-res=wait]").first());
							}
							else{
								service_element_detached.insertAfter($(".service[data-service-type="+service_type+"][data-res!=error][res!=wait]").last());
							}
						}
					}
				}
			});
			}
			else{
				results = e.find(".results");
				e.css('backgroundColor', 'white');
				results_display = "Temporarily deactivated";
				results.html(results_display);
				download_results_button.show();
				res_buttons.show();
			}
		});

		function getEPNTAPServicesWithResultsUrls(){
//			return $(".block_result li.resource[data-res=results][data-resource-type=epn-tap]").attr('data-resource-id');
			return $(".block_result li.service[data-res=results][data-service-type=epn-tap]").map(function() {
				return $( this ).attr('data-service-id');
			}).get();
		}
		
		function getQuery(){
			return $("form#main_form input[name=query_conditions]").attr("value");
		}
		
		function getCompilationResultsUrl(){
			var COMPIL_VOTABLE_PARAMETER_PREFIX = "ivoid_";
//			var query = $("form#main_form input[name=query_conditions]").attr("value");
//			var form = $("form#main_form");
//			var query_input = $("form#main_form input[name=query_conditions]");
			var query = $("#epn_query").attr("data-value");
			var ivoidList = getEPNTAPServicesWithResultsUrls();
			var urlParameters = [];
			if (query != ""){
				urlParameters.push("query="+encodeURIComponent(query));
			}
			for(var i = 0; i < ivoidList.length; i++){
				urlParameters.push(COMPIL_VOTABLE_PARAMETER_PREFIX+i+"="+encodeURIComponent(ivoidList[i]));
			}
			var urlBase = $("#compil_votable_form").attr('action');
			var url = urlBase + "?" + urlParameters.join("&");
			return url;
		}
		
		$("#download_results_compil").click(function(){
			window.location.href = getCompilationResultsUrl();
			return false;
		});
		
		$("#samp_results_compil").click(function(){
			var url = getCompilationResultsUrl();
			samp_europlanetclient.samp_votable(url, "EPN-TAP compilation results");
			return false;
		});
//		function getEPNTAPServicesWithResultsUrls(){
//			return $(".block_result li.resource[data-res=results][data-resource-type=epn-tap] .url_query_select_top10").map(function() {
//		    return $( this ).attr("data-value");
//		  }).get();
//					

			//			).attr("data-value");
//			var urlList=[];
//			for (var resourceWithResults in resourcesWithResults){
//				urlList.push(resourceWithResults.find(".url_query_select_top10").attr("data-value"));
//			}
//			return urlList;
//			
//		}

		
//		
//		var COMPIL_VOTABLE_PARAMETER_PREFIX = "tap_url_";
//
//		$("#download_results_compil").click(function(){
//			var ivoid_list= getEPNTAPServicesWithResultsUrls();
//			var form = $("#compil_votable_form");
//			for (var i = 0; i < ivoid_list.length; i++){
//				var field = $('<input></input>');
//				field.attr("type", "hidden");
//				field.attr("name", COMPIL_VOTABLE_PARAMETER_PREFIX + i);
//				field.attr("value", ivoid_list[i]);
//				form.append(field);
//				
//			}
//			form.submit();
//		});
//		
//		$("#samp_results_compil").click(function(){
//			var urlParameters = [];
//			var urlList= getEPNTAPServicesWithResultsUrls();
//			for (var i = 0; i < urlList.length; i++){
//				urlParameters.push(COMPIL_VOTABLE_PARAMETER_PREFIX+i+"="+encodeURIComponent(urlList[i]));
////				urlParameters.push(COMPIL_VOTABLE_PARAMETER_PREFIX+i+"="+urlList[i]);
//			}
//			var urlBase = $("#compil_votable_form").attr('action');
//				
//			var url = urlBase + "?" + urlParameters.join("&");
//			
//			samp_europlanetclient.samp_votable(url, "toto");
//			
////			var tap_url = "tap_url_0=http%3A%2F%2Fpsws-epntap.irap.omp.eu%2F__system__%2Ftap%2Frun%2Ftap%2Fsync%3FLANG%3DADQL%26QUERY%3DSELECT%2BTOP%2B10%2B%252A%2BFROM%2Bamdadb.epn_core%2BWHERE%2B%25281%2B%253D%2Bivo_hashlist_has%2528lower%2528%2522target_name%2522%2529%252Clower%2528%2527Saturn%2527%2529%2529%2BOR%2B1%2B%253D%2Bivo_hashlist_has%2528lower%2528%2522target_name%2522%2529%252Clower%2528%25276%2527%2529%2529%2529%26REQUEST%3DdoQuery%26MAXREC%3D1000000000";
////			url = urlBase + "?" + tap_url;
////			samp_europlanetclient.samp_votable(url, "toto");
//			
//		});
		
		
	});
})(jQuery);
