/**
 * Definition of namespace 'voparis'
 */

var vopdc = {
    author : "J. Normand"
};

vopdc.converter = {
    /**
     * Converts sexagesimal value to decimal.
     * @param {String} value The sexagesimal string to convert
     * @returns {Float} Decimal value
     */

    toDecimal : function(value) {
        var s = value.split(/:/);
        var res = Math.abs(parseFloat(s[0])) + parseFloat(s[1]) / 60.0 + parseFloat(s[2]) / 3600.0;
        if (s[0].match(/^-/)) {
            res = -res;
        }
        return res.toFixed(6);
        // formatting (6 trailing decimals -> 0.01" precision)
    },

    /**
     * Converts ISO date value to julian day.
     * @param {String} value The ISO string to convert
     * @returns {Float} JD
     */

    toJulianDay : function(iso) {
        var b = 0;
        var c = 0;

        var s = iso.split(/-/);
        var s1 = s[2].split(/T/);
        var s2 = s1[1].split(/:/);

        var _y = parseInt(s[0]);
        var _m = parseInt(s[1]);
        var _d = parseInt(s1[0]);
        var _h = parseInt(s2[0]);
        var _mi = parseInt(s2[1]);
        var _s = parseFloat(s2[2]);

        var y = _y;
        var m = _m;
        var d = _d;

        var h = _h + _mi / 60.0 + _s / 3600.0;

        if (_m <= 2) {
            y = _y - 1;
            m = _m + 12;
        }

        if (!(_y <= 1582 && _m <= 10 && _d < 15)) {
            a = Math.floor(y / 100.0);
            b = Math.floor(a / 4.0)
            b = 2 - a + b;
        }

        if (_y < 0) {
            c = 0.75;
        }

        jj1 = Math.floor(365.25 * y - c);
        jj2 = Math.floor(30.6001 * (m + 1));

        return jj1 + jj2 + d + +h / 24.0 + 1720994.5 + b;
    }
};

/**
 * Definition of sub namespace 'votable'. Extends voparis.
 */

vopdc.votable = (function($) {
    return {
        getQueryStatus : function(xml) {
            var node = $(xml).find('INFO[name="QUERY_STATUS"]');
            if (node.length != 0) {
                return node.attr('value');
            }

            return 'undefined';
        },

        isError : function(xml) {
            return (this.getQueryStatus(xml) == 'ERROR');
        },

        getCopyright : function(xml) {
            return $(xml).find('INFO[name|="copyright"]').text();
        },

        getRowCount : function(xml) {
            var n = 0;
            var nrows = $(xml).find('TABLE').attr('nrows');

            if (nrows != undefined) {
                n = nrows;
            } else if ($(xml).find('TR').length) {
                n = $(xml).find('TR').length;
            } else {
                n = $(xml).find('STREAM').text().length;
            }

            return n;
        }
    };
})(jQuery);

$(document).ready(function() {
    $('form').submit(function() {
        var b = vopdc.form.validate();
        return b;
    });

    $('div.l').each(function() {
        var bgcolor = '';
        var bgimage = '';

        var e = $(this);
        var s = $(this).find('h4.s');
        var a = $(this).find('a.q');
        var cprt = $(this).find('span.copyright');
        var dataTable = $(this).parent().find('table');
        if (a.size()) {
            var href = a.attr('href');
            var p = href.replace('*', 'TOP 10 *');

            $.ajax({
                url : '/proxy/',
                async : true,
                type : 'GET',
                //data: encodeURI(p[1] + '&TOP=1&MAXREC=1' + '&url=' + p[0]),
                //data: encodeURI('url=' + window.btoa(p)),
                data : 'url=' + p,
                //data: encodeURI('url=' + p),
                dataType : 'xml',
                beforeSend : function(xhr) {
                    e.css('backgroundColor', 'white');
                    s.css('backgroundImage', 'url("/media/images/loading.gif?g")');
                },
                complete : function(xhr, status) {
                    if (xhr.getResponseHeader('Content-Type') === 'text/xml') {
                        if (!vopdc.votable.isError(xhr.responseXML)) {
                            if (vopdc.votable.getRowCount(xhr.responseXML) > 0) {
                                bgcolor = '#a8eea4';
                                bgimage = 'url("/media/images/status-result.png?g")';
                                /*$(xhr.responseXML).find('TABLE').each(function() {
                                    //alert($(this).attr('name'));
                                    //                        var stitle = {};
                                    var aoColumns = [];
                                    if ($(this).attr('name') === "epn_core") {

                                        $(this).find('FIELD').each(function() {
                                            var aoColumnsDict = {};
                                            var columnName = $(this).attr('name');
                                            aoColumnsDict.sTitle = columnName;
                                            aoColumns.push(aoColumnsDict);
                                        });
                                        $(this).find('STREAM').each(function() {
                                            var textencoded = $(this).text();
                                            //text = atob(textencoded);
                                            alert(textencoded);
                                        });
                                    }
                                    /*                           var aoColumns = [];
                                    $(this).find('FIELD').each(function() {

                                    stitle["sTitle"] = $(this).attr('ID');
                                    aoColumns.push(stitle);
                                    });*/
                                    //                        }
                                    /*                       var datas = {};
                                     datas['aoColumns'] = stitle;
                                     */
                                    /*                                    var aoColumns = []
                                     var columnHeader1 = "A";
                                     var dict1 = {};
                                     dict1.sTitle = columnHeader1;
                                     aoColumns.push(dict1);
                                     var columnHeader2 = "B";
                                     var dict2 = {};
                                     dict2.sTitle = columnHeader2;
                                     aoColumns.push(dict2);
                                     var columnHeader3 = "C";
                                     var dict3 = {};
                                     dict3.sTitle = columnHeader3;
                                     aoColumns.push(dict3);
                                     var columnHeader4 = "D";
                                     var dict4 = {};
                                     dict4.sTitle = columnHeader4;
                                     aoColumns.push(dict4);
                                     var columnHeader5 = "E";
                                     var dict5 = {};
                                     dict5.sTitle = columnHeader5;
                                     aoColumns.push(dict5);

                                     */

/*                                    dataTable.dataTable({
                                        //"sScrollX" : "100%",
                                        //"sScrollXInner" : "110%",
                                        //"bScrollCollapse" : true,
                                       //"bAutoWidth":false,
                                       "bJQueryUI": true,
                                       //"bPaginate": false,
                                       "sScrollX": "100%",
                                        "aaData" : [
                                        // Reduced data set 
                                       ["Trident", "Internet Explorer 4.0", "Win 95+", 4, "X"], ["Trident", "Internet Explorer 5.0", "Win 95+", 5, "C"], ["Trident", "Internet Explorer 5.5", "Win 95+", 5.5, "A"], ["Trident", "Internet Explorer 6.0", "Win 98+", 6, "A"], ["Trident", "Internet Explorer 7.0", "Win XP SP2+", 7, "A"], ["Gecko", "Firefox 1.5", "Win 98+ / OSX.2+", 1.8, "A"], ["Gecko", "Firefox 2", "Win 98+ / OSX.2+", 1.8, "A"], ["Gecko", "Firefox 3", "Win 2k+ / OSX.3+", 1.9, "A"], ["Webkit", "Safari 1.2", "OSX.3", 125.5, "A"], ["Webkit", "Safari 1.3", "OSX.3", 312.8, "A"], ["Webkit", "Safari 2.0", "OSX.4+", 419.3, "A"], ["Webkit", "Safari 3.0", "OSX.4+", 522.1, "A"]],
                                        "aoColumns" : aoColumns
                                        /*                                        "aoColumns" : [{
                                       //  "sTitle" : "Engine"
                                       //  }, {
                                       //  "sTitle" : "Browser"
                                       //  }, {
                                       //  "sTitle" : "Platform"
                                       //  }, {
                                       //  "sTitle" : "Version",
                                        // "sClass" : "center"
                                       //  }, {
                                       //  "sTitle" : "Grade",
                                       ////  "sClass" : "center"
                                       //  }]
                                         
                                    });
                                });
*/                            } else {
                                bgcolor = '#eedea4';
                                bgimage = 'url("/media/images/status-no-result.png?g")';
                            }
                            cprt.text('Copyright notice: ' + vopdc.votable.getCopyright(xhr.responseXML));
                        } else {// error service QUERY_STATUS = ERROR
                            bgcolor = '#eea8a4';
                            bgimage = 'url("/media/images/status-error.png?g")';
                        }
                    } else {// error resolve hostname
                        bgcolor = '#eea8a4';
                        bgimage = 'url("/media/images/status-error.png?g")';
                    }

                    e.css('backgroundColor', bgcolor);
                    s.css('backgroundImage', bgimage);
                },
                error : function(xhr, status, exception) {
                    e.css('backgroundColor', '#eea8a4');
                    s.css('backgroundImage', 'url("/media/images/status-error.png?g")');
                },
            });
        }
    });
});
