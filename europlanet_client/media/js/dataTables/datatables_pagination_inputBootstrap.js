


$.fn.dataTableExt.oPagination.inputBootstrap = {
	    "fnInit": function (oSettings, nPaging, fnCallbackDraw) {
	 
	    	var line1 = document.createElement("div");
	    	var line2 = document.createElement("div");
	        var pagList1   = document.createElement("ul");
	        var pagList2   = document.createElement("ul");
	        var nFirst    = document.createElement('li');
	        var nPrevious = document.createElement('li');
	        var nNext     = document.createElement('li');
	        var nLast     = document.createElement('li');
	        var nInput    = document.createElement('input');
	        var nPage     = document.createElement('li');
	        var nOf       = document.createElement('li');
	        var oLang     = oSettings.oLanguage.oPaginate;
	 
	        nFirst.innerHTML    = '<a href="#">' + oLang.sFirst    + '</a>';
	        nPrevious.innerHTML = '<a href="#">' + oLang.sPrevious + '</a>';
	        nNext.innerHTML     = '<a href="#">'        + oLang.sNext     + '</a>';
	        nLast.innerHTML     = '<a href="#">'        + oLang.sLast     + '</a>';
	 
	        nFirst.className    = "paginate_button first disabled";
	        nPrevious.className = "paginate_button previous disabled";
	        nNext.className     = "paginate_button next";
	        nLast.className     = "paginate_button last";
	        nInput.className = "page_input";
	        nOf.className = "page_nof";
	        
	        nInput.type         = "text";
	        nPage.innerHTML     = "&nbsp;&nbsp;&nbsp;Page ";
	 
	        if (oSettings.sTableId !== '') {
	            nPaging.setAttribute  ( 'id', oSettings.sTableId + '_paginate' );
	            nFirst.setAttribute   ( 'id', oSettings.sTableId + '_first' );
	            nPrevious.setAttribute( 'id', oSettings.sTableId + '_previous' );
	            nNext.setAttribute    ( 'id', oSettings.sTableId + '_next' );
	            nLast.setAttribute    ( 'id', oSettings.sTableId + '_last' );
	            nOf.setAttribute      ( 'id', oSettings.sTableId + '_of' );
	            nPage.setAttribute    ( 'id', oSettings.sTableId + '_page' );
	            nInput.setAttribute   ( 'id', oSettings.sTableId + '_input' );
	        }
	 
	        pagList1.setAttribute("class", "pagination");
	        pagList1.appendChild(nFirst);
	        pagList1.appendChild(nPrevious);
	        pagList1.appendChild(nNext);
	        pagList1.appendChild(nLast);
	        
	        line1.appendChild(pagList1);
	        

	        pagList2.setAttribute("class", "pagination");
	        pagList2.appendChild(nPage);
	        pagList2.appendChild(nInput);
	        pagList2.appendChild(nOf);
	        line2.appendChild(pagList2);
	        
	        nPaging.appendChild(line2);
	        nPaging.appendChild(line1);
	        
	        
//	        $(nPaging).addClass('pagination');
//	        $(nInput).addClass('smallify');      // .smallify { width:40px; height:20px; overflow:hidden; }
	 
	        $(nFirst).click(function (e) {
	            e.preventDefault();
	            var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
	            if (iCurrentPage != 1) {
	                oSettings.oApi._fnPageChange(oSettings, "first");
	                fnCallbackDraw(oSettings);
	                $(nFirst).addClass('disabled');
	                $(nPrevious).addClass('disabled');
	                $(nNext).removeClass('disabled');
	                $(nLast).removeClass('disabled');
	            }
	        });
	 
	        $(nPrevious).click(function (e) {
	            e.preventDefault();
	            var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
	            if (iCurrentPage != 1) {
	                oSettings.oApi._fnPageChange(oSettings, "previous");
	                fnCallbackDraw(oSettings);
	                if (iCurrentPage == 2) {
	                    $(nFirst).addClass('disabled');
	                    $(nPrevious).addClass('disabled');
	                }
	                $(nNext).removeClass('disabled');
	                $(nLast).removeClass('disabled');
	            }
	        });
	 
	        $(nNext).click(function (e) {
	            e.preventDefault();
	            var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
	            if (iCurrentPage != Math.ceil((oSettings.fnRecordsDisplay() / oSettings._iDisplayLength))) {
	                oSettings.oApi._fnPageChange(oSettings, "next");
	                fnCallbackDraw(oSettings);
	                if (iCurrentPage == (Math.ceil((oSettings.fnRecordsDisplay() - 1) / oSettings._iDisplayLength) - 1)) {
	                    $(nNext).addClass('disabled');
	                    $(nLast).addClass('disabled');
	                }
	                $(nFirst).removeClass('disabled');
	                $(nPrevious).removeClass('disabled');
	            }
	        });
	 
	        $(nLast).click(function (e) {
	            e.preventDefault();
	            var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
	            if (iCurrentPage != Math.ceil((oSettings.fnRecordsDisplay() / oSettings._iDisplayLength))) {
	                oSettings.oApi._fnPageChange(oSettings, "last");
	                fnCallbackDraw(oSettings);
	                $(nFirst).removeClass('disabled');
	                $(nPrevious).removeClass('disabled');
	                $(nNext).addClass('disabled');
	                $(nLast).addClass('disabled');
	            }
	        });
	 
	// MUST INCLUDE $(nInput) KEYUP FUNCTION FROM ORIGINAL CODE.
	// REMOVED FROM THIS POST DUE TO CHARACTER LIMIT RESTRICTION.
			$(nInput).keyup(function (e) {
				// 38 = up arrow, 39 = right arrow
				if (e.which === 38 || e.which === 39) {
					this.value++;
				}
				// 37 = left arrow, 40 = down arrow
				else if ((e.which === 37 || e.which === 40) && this.value > 1) {
					this.value--;
				}

				if (this.value === '' || this.value.match(/[^0-9]/)) {
					/* Nothing entered or non-numeric character */
					this.value = this.value.replace(/[^\d]/g, ''); // don't even allow anything but digits
					return;
				}

				var iNewStart = oSettings._iDisplayLength * (this.value - 1);
				if (iNewStart < 0) {
					iNewStart = 0;
				}
				if (iNewStart >= oSettings.fnRecordsDisplay()) {
					iNewStart = (Math.ceil((oSettings.fnRecordsDisplay() - 1) / oSettings._iDisplayLength) - 1) * oSettings._iDisplayLength;
				}

				oSettings._iDisplayStart = iNewStart;
				fnCallbackDraw(oSettings);
			});
	 
	        /* Take the brutal approach to cancelling text selection */
	        $('span', nPaging).bind('mousedown', function () { return false; });
	        $('span', nPaging).bind('selectstart', function () { return false; });
	 
	        // If we can't page anyway, might as well not show it
//	        var iPages = Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength);
//	        if (iPages <= 1) {
//	            $(nPaging).hide();
//	        }
	    },
	 
	    "fnUpdate": function (oSettings, fnCallbackDraw) {
	        if (!oSettings.aanFeatures.p) {
	            return;
	        }
	        var iPages = Math.ceil((oSettings.fnRecordsDisplay()) / oSettings._iDisplayLength);
	        var iCurrentPage = Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength) + 1;
	     
	        var an = oSettings.aanFeatures.p;
	        if (iPages <= 1) // hide paging when we can't page
	        {
	            $(an).hide();
	        }
	        else {
	        	$(".page_input").val(iCurrentPage);
	        	$(".page_nof").html(" of " + iPages);
	            /* Loop over each instance of the pager */
//	            for (var i = 0, iLen = an.length ; i < iLen ; i++) {
//	                var spans = an[i].getElementsByTagName('li');     // Changed tags from spans to line items
//	                var inputs = an[i].getElementsByTagName('input');
//	                spans[3].innerHTML = " of " + iPages;
//	                inputs[0].value = iCurrentPage;
//	            }
	        }
	        var nFirst = $("#service_results_first");
	        var nPrevious = $("#service_results_previous");
	        var nNext = $("#service_results_next");
	        var nLast = $("#service_results_last");
	        if (iCurrentPage == 1){
                $(nFirst).addClass('disabled');
                $(nPrevious).addClass('disabled');
                $(nNext).removeClass('disabled');
                $(nLast).removeClass('disabled');	        	
	        }
	        else{
	        	if (iCurrentPage == iPages){
	                $(nFirst).removeClass('disabled');
	                $(nPrevious).removeClass('disabled');
	                $(nNext).addClass('disabled');
	                $(nLast).addClass('disabled');
	        	}
	        	else{
	                $(nFirst).removeClass('disabled');
	                $(nPrevious).removeClass('disabled');
	                $(nNext).removeClass('disabled');
	                $(nLast).removeClass('disabled');	        	        		
	        	}
	        	
	        }
	    }
	};
