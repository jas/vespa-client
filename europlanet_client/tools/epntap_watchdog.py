import urllib2
from lxml import etree
from app.registry import getEpnServices
import subprocess as sp
import re


TOPCAT_PATH = '/Applications/TOPCAT.app/Contents/Resources/Java/topcat-full.jar'


def check_resource_taplint(r):
  """
  Check single resource (URL) with TOPCAT's taplint. Returns dictionary with number of errors, warnings, etc.
  """
  if type(r) == str:
    url = r
  else:
    url = r.access_url
    
  try:
    topcat_output = sp.check_output(['java', '-jar', TOPCAT_PATH, '-stilts', 'taplint', url])
  except sp.CalledProcessError, e:
    print "Error when trying to use TOPCAT taplint:\n", e.output

  m = re.search('Totals: Errors: (\d+); Warnings: (\d+); Infos: (\d+); Summaries: (\d+); Failures: (\d+)', topcat_output)
  print m.group(0)
  return {'errors': m.group(1), 'warnings': m.group(2), 'infos': m.group(3), 'summaries': m.group(4), 'failures': m.group(5)}


def check_resource_voparis(r):
  """
  Check single resource (URL) with VO-Paris validator. Returns number of errors.
  """
  print 'hey!!!'
  url = r'http://voparis-validator.obspm.fr/validator.php?LANG=ADQL&REQUEST=doQuery&MAXREC=10&FORMAT=VOTable%2FTD&QUERY=select+*+from+%s.epn_core&spec=EPN-TAP2&complemparam=+&addparams=&service=http%3A%2F%2Fvoparis-validator.obspm.fr%2Fxml%2F111.xml%3F&serviceURL=%s&format=XML' % (r.schema, r.access_url)
  print url

  try:
    webFile = urllib2.urlopen(url)
  except e:
    status_code = webFile.getcode()
    print status_code
    raise 
  s = webFile.read()
  webFile.close()
  
  tree = etree.fromstring(s)
  rr = tree.xpath('/x:validator/x:error', namespaces={'x': 'http://voparis-validator.obspm.fr/'})
  return {'errors': len(rr)}


if __name__ == '__main__':
  rr = getEpnServices()
  #check_resource(rr[0])

